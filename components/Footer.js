import React from "react";
import { View, Image, Dimensions, TouchableOpacity, Text } from "react-native";
import customStyles from "../assets/styling/styles";
import { connect } from "react-redux";

const { height, width } = Dimensions.get("window");

class Footer extends React.Component {
  constructor(props) {
    super(props);
  }

  state = {
    iconSaved: true,
    iconBell: false,
    iconGraph: false,
    iconMessage: false,
    iconScan: false
  };

  static navigationOptions = {
    header: null
  };


  HandleEventSave = () => {
    this.props.actionSave();
    this.setState({ iconSaved: true, iconBell: false, iconGraph: false, iconMessage: false, iconScan: false });
  };
  HandleEventNotification = () => {
    this.props.actionNotification();
    this.setState({ iconSaved: false, iconBell: true, iconGraph: false, iconMessage: false, iconScan: false });
  };
  HandleEventProfile = () => {
    this.props.actionProfile();
    this.setState({ iconSaved: false, iconBell: false, iconGraph: true, iconMessage: false, iconScan: false });
  };
  HandleEventMessage = () => {
    this.props.actionMessage();
    this.setState({ iconSaved: false, iconBell: false, iconGraph: false, iconMessage: true, iconScan: false });
  };
  HandleEventScan = () => {
    this.props.actionScan();
    this.setState({ iconSaved: false, iconBell: false, iconGraph: false, iconMessage: false, iconScan: true });
  };

  render() {
    return (
      <View style={customStyles.containerFlex}>
        <View style={customStyles.footerContainer}>
          <View style={customStyles.footerView}>
            <View>
              <TouchableOpacity onPress={this.HandleEventSave.bind(this)}>{this.state.iconSaved ? <Image source={require("../assets/images/icons/bookmark.png")} style={customStyles.footerIcon} resizeMode="contain" /> : <Image source={require("../assets/images/icons/bookmark_gray.png")} style={customStyles.footerIcon} resizeMode="contain" />}</TouchableOpacity>
            </View>
            <View>
              <View style={customStyles.notifMessages}>
                <Text style={customStyles.notifMessagesText}>{this.props.notif}</Text>
              </View>
              <TouchableOpacity onPress={this.HandleEventNotification.bind(this)}>{this.state.iconBell ? <Image source={require("../assets/images/icons/notification.png")} style={customStyles.footerIcon} resizeMode="contain" /> : <Image source={require("../assets/images/icons/notification_gray.png")} style={customStyles.footerIcon} resizeMode="contain" />}</TouchableOpacity>
            </View>
            <View>
              <TouchableOpacity onPress={this.HandleEventProfile.bind(this)}>{this.state.iconGraph ? <Image source={require("../assets/images/icons/analytics.png")} style={customStyles.footerIcon} resizeMode="contain" /> : <Image source={require("../assets/images/icons/analytics_gray.png")} style={customStyles.footerIcon} resizeMode="contain" />}</TouchableOpacity>
            </View>
            <View>
              <View style={customStyles.notifMessages}>
                <Text style={customStyles.notifMessagesText}>{this.props.notifMessages}</Text>
              </View>
              <TouchableOpacity onPress={this.HandleEventMessage.bind(this)}>{this.state.iconMessage ? <Image source={require("../assets/images/icons/message.png")} style={customStyles.footerIcon} resizeMode="contain" /> : <Image source={require("../assets/images/icons/message_gray.png")} style={customStyles.footerIcon} resizeMode="contain" />}</TouchableOpacity>
            </View>
            <View style={{}}>
              <TouchableOpacity onPress={this.HandleEventScan.bind(this)}>{this.state.iconScan ? <Image source={require("../assets/images/icons/scan.png")} style={customStyles.footerIcon} resizeMode="contain" /> : <Image source={require("../assets/images/icons/scan_gray.png")} style={customStyles.footerIcon} resizeMode="contain" />}</TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

export default connect()(Footer);
