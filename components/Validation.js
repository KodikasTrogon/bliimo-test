export function validate(type, input) {
  switch (type){
    case 'required':
      return (input.length>0) ? true : false;
    case 'text':
      var re = /^([a-zA-Z])/;
      return (re.test(input)) ? true : false;
    case 'number':
      var re = /^(\d+\.?\d*$)/;
      return (re.test(input)) ? true : false;
    case 'name':
      var re = /^([A-Z]{1,}[a-z]{1,})+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/;
      return (re.test(input)) ? true : false;
    case 'phone':
      var re = /^(09|\+639)\d{9}$/;
      return (re.test(input)) ? true : false;
    case 'email':
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return (re.test(input)) ? true : false;
    case 'password':
      var re = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{6,50}/;
      return (re.test(input)) ? true : false;
    default:
      return 0;
  }
}