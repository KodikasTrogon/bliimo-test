import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { NavigationActions, DrawerActions } from 'react-navigation';
import { View, TextInput, Image, TouchableOpacity, Text } from 'react-native';
import { connect } from 'react-redux';
import { userGetAccount } from '../reducers/reducer';
import customStyles from '../assets/styling/styles';

class Search extends Component {
  constructor(props) {
    super(props);
  }
  state = {
    searchInput : ""
  };
  onchange = (searchInput) => {
    this.setState({searchInput});
    this.props.handleSearch(searchInput);
  };
  render() {
    return (
      <View style={customStyles.paddingTop30}>
        <View style={customStyles.searchView}>
          <Image style={[ customStyles.searchImage ]} source={require('../assets/images/search.png')} />
          <TextInput
            onChangeText={(searchInput) => {this.onchange(searchInput)}}
            value={this.state.searchInput}
            style={{ height: 40, borderWidth: 0, flex: 1, backgroundColor: 'transparent', color: '#d1d1d1' }}
            underlineColorAndroid="transparent"
          />
          {this.state.searchInput != '' && (
            <TouchableOpacity onPress={() => this.onchange('')}>
              <Image style={customStyles.searchClose} source={require('../assets/images/closeSearch.png')} />
            </TouchableOpacity>
          )}
        </View>
        {/* <View style={{ position: 'absolute', right: 5, padding: 10 }}>
          <TouchableOpacity>
            <View>
              <Image style={[ customStyles.writeImage ]} source={require('../assets/images/new_message.png')} />
            </View>
          </TouchableOpacity>
        </View> */}
      </View>
    );
  }
}
export default Search;