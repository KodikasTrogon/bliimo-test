import PropTypes from 'prop-types';
import React, {Component} from 'react';
import { NavigationActions, DrawerActions } from 'react-navigation';
import {
  View, 
} from 'react-native';
import { connect } from 'react-redux';
import { userGetAccount } from '../reducers/reducer';

class SideMenu extends Component {
  constructor(props){
    super(props);
  }

  state = {
    accountID:"",
    firstname:"",
    lastname:"",
    email:""
  }

  navigateToScreen = (route) => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
  }

  componentWillMount = async() => {
  }

  render () {
    return (
      <View style={{backgroundColor:"#299fbd"}}>
      </View>
    );
  }
}

SideMenu.propTypes = {
  navigation: PropTypes.object
};

const mapStateToProps = state => {
  
    return{};
  
};

const mapDispatchToProps = {
  userGetAccount,
};

export default connect(mapStateToProps, mapDispatchToProps)(SideMenu);