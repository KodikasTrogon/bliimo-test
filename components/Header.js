import React from 'react';
import {
  AsyncStorage,
  View,
  TouchableOpacity,
  Image,
} from 'react-native';
import { DrawerActions } from 'react-navigation';

class Head extends React.Component {
  constructor(props){
    super(props);
  }

  state = {
    accountID:"",
    headerType:"",
    width:0,
    x:-300
  }

  componentDidMount = async() => {
    let id = await AsyncStorage.getItem('accountID');
    this.setState({accountID:id});
  }

  render(){
    return(
        <View style={{height:50, padding:5, backgroundColor:"#3281b3", elevation: 2, flexDirection:"row",}}>
          <View style={{flex:2, alignContent:"center", justifyContent:"center"}}>
            <TouchableOpacity 
              onPress={()=>{ this.props.props.toggleDrawer(); }}>
              <Image style={{height:30, width:30}} source={require('../assets/images/menu.png')} />
            </TouchableOpacity>
          </View>
          <View style={{flex:1, flexDirection:"row",}}>
            {/* <TouchableNativeFeedback 
              background={TouchableNativeFeedback.Ripple("#FFFFFF")} 
              onPress={()=>{this.props.navigation.navigate("BookingList",{accountID:this.state.accountID})}}>
              <Image style={{height:30, width:30}} source={require('../assets/images/bell.png')} />
            </TouchableNativeFeedback> */}
            {/* <Image style={{height:30, width:30}} source={require('../assets/images/icon.png')} /> */}
          </View>
        </View>
    );
  }
}

export default Head;
