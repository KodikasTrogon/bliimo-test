import React, { Component } from 'react';
import { View, Image, Text, TouchableOpacity, Dimensions } from 'react-native';
import customStyles from '../assets/styling/styles';

const { height, width } = Dimensions.get('window');
class Message extends Component {
	constructor(props) {
		super(props);
	}
	state = {
    isRead : false,
		time: '',
		days: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
	};

	handleMessage = () => {
		this.setState({ isRead: true });
    this.props.openChats();
  }
	componentDidMount() {
		this._convertTime()
  }

  _convertTime = () => {
		//Split date in API
    function getParsedDate(date){
      date = String(date).split(' ');
			let hours = String(date[0]).split('.');
      return [hours[0]];
		}
		//Get Current Date
		let dateToday = new Date();
		let yearDateToday = dateToday.getFullYear().toString(), 
				monthDateToday = dateToday.getMonth() + 1,
				dayDateToday = dateToday.getDate().toString()
				dateToday = yearDateToday + '-' + monthDateToday.toString() + '-' + dayDateToday;
		//Get Date and Time in API
		let dateCreated = new Date(...getParsedDate(this.props.time));
		let hour = dateCreated.getHours(),
				yearDateCreated = dateCreated.getFullYear().toString(),
				monthDateCreated = dateCreated.getMonth() + 1,
				dayDateCreated = dateCreated.getDate().toString(),
				day = dateCreated.getDay() - 1,
				finalDateCreated = yearDateCreated + '-' + monthDateCreated.toString() + '-' + dayDateCreated,
				TimeType = '',
				minutes = '',
				finalHour = ''
		if(hour>=13){
			finalHour = hour-12
		}else{
			finalHour = hour
		}

		if(dateCreated.getMinutes().toString().length == 1){
			minutes = '0' + dateCreated.getMinutes().toString();
		}else if(dateCreated.getMinutes().toString().length == 0){
			minutes = '00';
		}else{
			minutes = dateCreated.getMinutes().toString();
		}

		//Identify it's AM or PM		
    if(hour <= 11) { TimeType = 'am' }
    else { TimeType = 'pm'; }
		let time = `${finalHour}:${ minutes + TimeType}`

		//Change after 24 hours
		if(dateToday == finalDateCreated){
			this.setState({time})
		}else{
			this.setState({time: this.state.days[day]})
		}
		setTimeout(() => {this._convertTime()}, 1000)
  }

	render() {
		return (
			<TouchableOpacity onPress={() => {this.handleMessage()}}>
				<View style={{ flex: 1, flexDirection: 'row',justifyContent: 'center', padding: 10 }}>
					<View style={{ paddingRight: 5 }}>
						<View style={{borderRadius: height*.4, overflow: 'hidden'}}>
							<Image
								source={this.props.avatar}
								style={{ height: 35, width: 35 }}
							/>
						</View>
						{!this.state.isRead && (
							<View
								style={{
									height: 10,
									width: 10,
									borderRadius: 10 / 2,
									backgroundColor: '#fff',
									position: 'absolute',
									bottom: 0,
									right: 10
								}}
							/>
						)}
					</View>
					<View style={{ flexDirection: 'column', flex: 2, justifyContent: 'center', marginLeft: 5}}>
						<View style={{ flexDirection: 'row' }}>
							<Text style={[ customStyles.threadTitle ]}>{this.props.name}</Text>
							<Text style={[ customStyles.threadTitle, { fontSize: 11, paddingLeft: 5, fontWeight: '500', paddingTop: 2 } ]}>
								@{this.props.username}
							</Text>
						</View>
						<View>
							<Text style={[ customStyles.threadContent, { fontSize: 11, fontWeight: '500' } ]}>
                {this.props.message.length > 40 ? this.props.message.substring(0, 40 - 3) + '...' : this.props.message}
							</Text>
						</View>
					</View>
					<View style={{ justifyContent: 'center' }}>
						<Text style={customStyles.threadTime}> { this.state.time} </Text>
					</View>
				</View>
			</TouchableOpacity>
		);
	}
}
export default Message;
