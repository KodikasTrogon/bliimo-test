import PropTypes from 'prop-types';
import React from 'react';
import { View, Image, Dimensions, TouchableOpacity, Text } from 'react-native';
import customStyles from '../assets/styling/styles';
import { connect } from 'react-redux';

const { height, width } = Dimensions.get('window');

class Tab extends React.Component {
	constructor(props) {
		super(props);
	}

	state = {
		iconSaved        : true,
		iconBell         : false,
		iconGraph        : false,
		iconMessage      : false,
		iconScan         : false,
		showMessageLabel : false,
		showNotifLabel   : false,
		messageCount     : 8,
		notifCount       : 5
	};

	static navigationOptions = {
		header : null
	};

	navigateToScreen = (route) => () => {
		const navigateAction = NavigationActions.navigate({
			routeName : route
		});
		this.props.navigation.dispatch(navigateAction);
	};

	componentDidMount = () => {
		this.checkMessageCount();
		this.checkNotifCount();
		// console.log('Message', this.state.showMessageLabel);
		// console.log('Notif', this.state.showNotifLabel);
	};

	checkMessageCount = () => {
		if (this.state.messageCount > 0) {
			this.setState({ showMessageLabel: true });
		} else {
			this.setState({ showMessageLabel: false });
			// console.log('WALA NA DAPAT MESSAGE');
		}
	};

	checkNotifCount = () => {
		if (this.state.notifCount > 0) {
			this.setState({ showNotifLabel: true });
		} else {
			this.setState({ showNotifLabel: false });
			// console.log('WALA NA DAPAT NOTIF');
		}
	};

	HandleEventSave = () => {
		this.props.navigation.navigate('SavedEvents');
		this.setState({ iconSaved: true, iconBell: false, iconGraph: false, iconMessage: false, iconScan: false });
	};

	HandleEventNotification = () => {
		this.props.navigation.navigate('Notifications');
		this.setState({
			iconSaved      : false,
			iconBell       : true,
			iconGraph      : false,
			iconMessage    : false,
			iconScan       : false,
			notifCount     : 0,
			showNotifLabel : false
		});
		this.HandleShowNotifLabel();
	};

	HandleEventProfile = () => {
		this.props.navigation.navigate('AccountAnalytics');
		this.setState({ iconSaved: false, iconBell: false, iconGraph: true, iconMessage: false, iconScan: false });
	};

	HandleEventMessage = () => {
		this.props.navigation.navigate('Messages');
		this.setState({
			iconSaved        : false,
			iconBell         : false,
			iconGraph        : false,
			iconMessage      : true,
			iconScan         : false,
			messageCount     : 0,
			showMessageLabel : false
		});
		this.HandleShowMessageLabel();
	};
	HandleEventScan = () => {
		this.props.navigation.navigate('RedeemPage');
		this.setState({ iconSaved: false, iconBell: false, iconGraph: false, iconMessage: false, iconScan: true });
	};

	HandleShowNotifLabel = () => {
		return this.state.showNotifLabel ? (
			<View style={customStyles.notifMessages}>
				<Text style={customStyles.notifMessagesText}> {this.state.notifCount} </Text>
			</View>
		) : null;
	};

	HandleShowMessageLabel = () => {
		return this.state.showMessageLabel ? (
			<View style={customStyles.notifMessages}>
				<Text style={customStyles.notifMessagesText}> {this.state.messageCount} </Text>
			</View>
		) : null;
	};

	render() {
		return (
			<View>
				<View style={customStyles.footerContainer}>
					<View style={customStyles.footerView}>
						<View>
							<TouchableOpacity onPress={this.HandleEventSave.bind(this)}>
								{this.state.iconSaved ? (
									<Image
										source={require('../assets/images/icons/bookmark.png')}
										style={customStyles.footerIcon}
										resizeMode="contain"
									/>
								) : (
									<Image
										source={require('../assets/images/icons/bookmark_gray.png')}
										style={customStyles.footerIcon}
										resizeMode="contain"
									/>
								)}
							</TouchableOpacity>
						</View>
						<View>
							{this.HandleShowNotifLabel()}
							<TouchableOpacity onPress={this.HandleEventNotification.bind(this)}>
								{this.state.iconBell ? (
									<Image
										source={require('../assets/images/icons/notification.png')}
										style={customStyles.footerIcon}
										resizeMode="contain"
									/>
								) : (
									<Image
										source={require('../assets/images/icons/notification_gray.png')}
										style={customStyles.footerIcon}
										resizeMode="contain"
									/>
								)}
							</TouchableOpacity>
						</View>
						<View>
							<TouchableOpacity onPress={this.HandleEventProfile.bind(this)}>
								{this.state.iconGraph ? (
									<Image
										source={require('../assets/images/icons/analytics.png')}
										style={customStyles.footerIcon}
										resizeMode="contain"
									/>
								) : (
									<Image
										source={require('../assets/images/icons/analytics_gray.png')}
										style={customStyles.footerIcon}
										resizeMode="contain"
									/>
								)}
							</TouchableOpacity>
						</View>
						<View>
							{this.HandleShowMessageLabel()}

							<TouchableOpacity onPress={this.HandleEventMessage.bind(this)}>
								{this.state.iconMessage ? (
									<Image
										source={require('../assets/images/icons/message.png')}
										style={customStyles.footerIcon}
										resizeMode="contain"
									/>
								) : (
									<Image
										source={require('../assets/images/icons/message_gray.png')}
										style={customStyles.footerIcon}
										resizeMode="contain"
									/>
								)}
							</TouchableOpacity>
						</View>
						<View style={{}}>
							<TouchableOpacity onPress={this.HandleEventScan.bind(this)}>
								{this.state.iconScan ? (
									<Image
										source={require('../assets/images/icons/scan.png')}
										style={customStyles.footerIcon}
										resizeMode="contain"
									/>
								) : (
									<Image
										source={require('../assets/images/icons/scan_gray.png')}
										style={customStyles.footerIcon}
										resizeMode="contain"
									/>
								)}
							</TouchableOpacity>
						</View>
					</View>
				</View>
			</View>
		);
	}
}

Tab.propTypes = {
	navigation : PropTypes.object
};

const mapStateToProps = (state) => {
	return {};
};

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(Tab);
