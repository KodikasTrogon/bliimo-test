import React from 'react';
import {
  View,
  Dimensions,
  ActivityIndicator,
} from 'react-native';

const { height, width } = Dimensions.get('window');

class Loading extends React.Component {
  constructor(props){
    super(props);
  }

  render(){
    return(
      <View style={{flexDirection:'row', justifyContent: 'center', alignContent: 'center', height: height/1.2, position: 'absolute', alignSelf: 'center'}}>
        <ActivityIndicator size="large" color="#666" />
      </View>
    );
  }
}

export default Loading;