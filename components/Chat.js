import PropTypes from "prop-types";
import React, { Component } from "react";
import { NavigationActions, DrawerActions } from "react-navigation";
import { View, TextInput, Image, Text, TouchableOpacity, Dimensions } from "react-native";
import * as emoticons from 'react-native-emoticons';

class Chat extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    //console.log(this.props.userID, 'recipient', this.props.recipient)
    let avatarUser  = (!this.props.userAvatar) ? require('../assets/images/profile.png') : {uri:this.props.userAvatar};  
    let recipientAvatar  = (!this.props.recipientAvatar) ? require('../assets/images/profile.png') : {uri:this.props.recipientAvatar};  
    const himMessages = (
      <View style={{ flexDirection: "row", alignItems: "center", alignSelf: "flex-start", flex: 2, paddingRight: 15, marginRight: 70 }}>
        { !this.props.messageThread ?
          <View style={{borderRadius:100, overflow: 'hidden', position: 'absolute', left: 0, marginRight: 5, bottom:0}}>
            <Image
              source={recipientAvatar}
              style={{ height: 35, width: 35}}
            />
          </View>
        :
          <View/>
        }
        <View style={{borderRadius: 10, overflow: 'hidden', backgroundColor: "#d4d2d2", paddingTop: 9, paddingBottom: 9, paddingRight: 15, paddingLeft: 15, marginLeft: 45}}>
          <Text style={{ color: "#000" }}>{emoticons.parse(this.props.message)}</Text>
        </View>
      </View>
    );
    const youMessages = (
      <View style={{ flexDirection: "row", alignItems: "center", flex: 2, alignSelf: "flex-end", paddingRight: 15, marginLeft: 70 }}>
        <View style={{borderRadius: 10, overflow: 'hidden', backgroundColor: "#606060",  padding: 8, paddingLeft: 15, paddingRight: 18, borderRadius: 10, marginRight: 5 }}>
          <Text style={{ color: "#d1d1d1" }}>{emoticons.parse(this.props.message)}</Text>
        </View>
        { this.props.messageThread ?
          <View style={{borderRadius:100, overflow: 'hidden', position: 'absolute', right: 0, bottom: 0}}>
            <Image
              source={avatarUser}
              style={{ height: 15, width: 15 }}
            />
          </View>
        :
          <View/>
        }
      </View>
    );
    return <View style={{ justifyContent: "center", padding: 3 }}>{this.props.recipient == this.props.userID ? youMessages : himMessages}</View>;
  }
}
export default Chat;