import React from 'react';
import { View, Text, StyleSheet, Dimensions, AsyncStorage, Image, TouchableOpacity, FlatList } from 'react-native';

import { connect } from 'react-redux';
import customStyles from '../assets/styling/styles';
import { Calendar, Agenda } from 'react-native-calendars';
import { getContentVouchers } from '../reducers/reducer';

const { height, width } = Dimensions.get('window');

class CalendarAgenda extends React.Component {
	static navigationOptions = {
		header: null
	};

	constructor() {
		super();
	}
	state = {
		items: {},
		currentDay: '',
		showBooking:{},
    token: '',
    markedDates: {},
    load: false
	};

	componentDidMount = async() => {
		const token = await AsyncStorage.getItem('token');
		this.setState({ token })
		this.HandleDay();
		this.HandleGetVouchers();
	};

	HandleDay = () => {
		let date = new Date();
		let year = date.getFullYear().toString();
		let month = date.getMonth() + 1;
		let day = date.getDate().toString();
		if(month.toString().length == 1){
			month = '0' + month.toString()
		}
		//Get Current Day to set in calendar
		let finalDate = year + '-' + month.toString() + '-' + day;
		this.setState({ currentDay: finalDate });
	};

	HandleAddBooking = (clickedDay) => {
		this.props.addBooking(this.state.currentDay, clickedDay);
		
	};

	HandleGetVouchers = async() => {
		let token = this.state.token, contentId = this.props.contentId;
		await this.props.getContentVouchers(token, contentId);
		let getContentVouchers = await this.props.contentVoucherState;
    let showBooking = {}, contentVouchers = [], dateArray = [], perDate = '', markedDates = {};
		let count = 1;
		if(getContentVouchers != 'failed'){
       //get list of vouchers
			getContentVouchers.vouchers.map((value, i) => {
        dateArray = [...dateArray, getParsedDate(value.reservationDate)]
        perDate = getParsedDate(value.reservationDate)
        showBooking = { ...showBooking, [perDate]:[value]}
        contentVouchers = [...contentVouchers, value];
				markedDates = { ...markedDates, [perDate]: {marked: true}}
      })
			//concat vouchers with more than 1 in 1 date
      
			for (let i = 0; i < getContentVouchers.vouchers.length-2; i++ ){
				if (getParsedDate(contentVouchers[i].reservationDate) == getParsedDate(contentVouchers[i+count].reservationDate)) {
					count = 1
					showBooking[dateArray[i]].push(contentVouchers[i])
				}else{
					count = 0
				}
			}
			this.setState({ showBooking, markedDates, load: true })
		}else{
			console.log('failed')
		}

		//Split date in API
		function getParsedDate(date) {
			date = String(date).split(' ');
			let hours = String(date[0]).split('T');
			return hours[0];
		}
	}

	render() {
		return (
			<View style={[ customStyles.containerFlex, { display: this.props.visibility ? 'flex' : 'none' } ]}>
				<Agenda
					items={this.state.items}
					loadItemsForMonth={this.loadItems.bind(this)}
					selected={this.state.currentDay}
					renderItem={this.renderItem.bind(this)}
					renderEmptyDate={this.renderEmptyDate.bind(this)}
					rowHasChanged={this.rowHasChanged.bind(this)}
					markingType={'dot'}
					markedDates={{
						[this.state.currentDay]: { selected: true, marked: true},
					}}
					// specify how agenda knob should look like
					renderKnob={() => {
						return (
							<Image
								source={require('../assets/images/menu.png')}
								style={customStyles.knobIcon}
								resizeMode="contain"
							/>
						);
					}}
          markedDates={this.state.markedDates}
					theme={{
            calendarBackground: '#191919',
            textColor: '#cccccc',

            }}
					// If provided, a standard RefreshControl will be added for "Pull to Refresh" functionality. Make sure to also set the refreshing prop correctly.
					onRefresh={() => this.HandleGetVouchers()}
					// Set this true while waiting for new data from a refresh
					refreshing={false}
					// Add a custom RefreshControl component, used to provide pull-to-refresh functionality for the ScrollView.
					refreshControl={null}
				/>
			</View>
		);
  }
  
	loadItems(day) {
		setTimeout(() => {
			if(this.state.load){
				let { items, showBooking } = this.state;
				for (let i = -15; i < 10; i++) {
					const time = day.timestamp + i * 24 * 60 * 60 * 1000;
					const strTime = this.timeToString(time);
					if (!items[strTime]) {
						items[strTime] = [];
						if (showBooking[strTime] != undefined) {
							items[strTime].push({
								date: strTime,
								height: height / 5
							});	
						}
					}
				}
				const newItems = {};
				Object.keys(items).forEach(key => { newItems[key] = items[key]; });
				this.setState({
					items
				});
			}
		}, 3000);
	}

	
	HandleClickBooking = (status) => {
		this.props.bookingAction(status);
	}

	HandleShowBooking = (value) => {
		const bookingStatus = Math.round(Math.random()); //Generate 1 - 0 for random event placeholder
		let item = value.item;
		return (
			<TouchableOpacity onPress={()=>{this.HandleClickBooking(bookingStatus)}}>
				<View style={styles.item}>
					<View style={[customStyles.calendarImageView]}>
						<Image
							source={require('../assets/images/profile/sample1.png')}
							style={customStyles.calendarImage}
						/>
					</View>
					<View>
						<Text style={customStyles.calendarTitleLabel}> {item.reservationType}</Text>
						<Text style={customStyles.calendarDetailsLabel}> {item.name}</Text>
						<Text style={customStyles.calendarDetailsLabel}> {item.activityHours} </Text>
						<Text style={customStyles.calendarDetailsLabel}> {item.reservationDate}</Text>
						<Text style={customStyles.calendarDetailsLabel}> {item.customerInfo}</Text>
						<Text style={customStyles.calendarDetailsLabel}> No. of Pax: {item.noOfPax} </Text>
					</View>
					<View style={customStyles.flexRowCenterSpaceBetween}>
						<Text style={customStyles.calendarTextStatus}> {item.redemptionStatus} </Text>
						<Text style={customStyles.calendarTextStatus}> {item.voucherCode} </Text>
					</View>
				</View>
			</TouchableOpacity>
		);
	}

	renderItem(item) {
    //console.log(item, 'item')
		return (
			<View>
        <View style={styles.notEmptyDate}>
          <TouchableOpacity onPress={()=>this.HandleAddBooking(item.date)}>
            <View style={customStyles.calendarBtnBooking}>
              <Text style={customStyles.calendarTextBooking}> Add Booking </Text>
            </View>
          </TouchableOpacity>
        </View>
        <FlatList
					data={Object.values(this.state.showBooking[item.date])}
          renderItem={this.HandleShowBooking}
					keyExtractor={(data, index) => index.toString()}
        />
			</View>
		);
	}

	//Empty Event Style
	renderEmptyDate(item,i) {
		let year = item.getFullYear().toString();
		let month = item.getMonth() + 1;
		let day = item.getDate().toString();
		if(month.toString().length == 1){
			month = '0' + month.toString()
		}
		//Get Current Day to set in calendar
		let finalDate = year + '-' + month.toString() + '-' + day;
		return (
			<View style={styles.emptyDate}>
				<TouchableOpacity onPress={()=>this.HandleAddBooking(finalDate)}>
					<View style={customStyles.calendarBtnBooking}>
						<Text style={customStyles.calendarTextBooking}> Add Booking </Text>
					</View>
				</TouchableOpacity>
			</View>
		);
	}

	rowHasChanged(r1, r2) {
		return r1.name !== r2.name;
	}

	timeToString(time) {
		const date = new Date(time);
		return date.toISOString().split('T')[0];
	}
}

const styles = StyleSheet.create({
	item: {
		backgroundColor: '#191919',
		flex: 1,
		borderRadius: 5,
		borderColor: '#cccccc',
		borderWidth: 0.5,
		padding: 8,
		marginTop: 20,
		marginBottom: 10,
		marginRight: 15,
		height: height / 5,
		flexDirection: 'column',
		justifyContent: 'space-between'
	},
	itemHighlight: {
		backgroundColor: '#cccccc',
		flex: 1,
		borderRadius: 5,
		borderColor: '#cccccc',
		borderWidth: 0.5,
		padding: 8,
		marginTop: 20,
		marginBottom: 10,
		marginRight: 10,
		height: height / 4.5,
		flexDirection: 'column',
		justifyContent: 'space-between'
	},
	emptyDate: {
		marginTop: 35,
		flex: 1,
		height: 30
  },
  notEmptyDate: {
		marginTop: 35,
		flex: 1,
		height: 50
	}
});

const mapStateToProps = (state) => {
	return {
		contentVoucherState: state.contentVoucherState,
	};
};

const mapDispatchToProps = {
	getContentVouchers
};

export default connect(mapStateToProps, mapDispatchToProps)(CalendarAgenda);