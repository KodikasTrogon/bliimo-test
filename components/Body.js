import React from 'react';
import {
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  Text
} from 'react-native';
import customStyles from '../assets/styling/styles';
import { connect } from 'react-redux';

const {height, width} = Dimensions.get('window');

class Body extends React.Component{
  constructor(props){
    super(props);
  }

  state = {
      iconSaved: true,
      iconBell: false,
      iconGraph: false,
      iconMessage: false,
      iconScan: false,
      txtHorseBackRiding: true,
      txtScubaDiving: false,
  }

  static navigationOptions = {
    header:null,
  };
  HandleHorseBackRiding = () => {
    this.props.actoinHBR();
    this.setState({txtHorseBackRiding: true, horseBackRiding: true, txtScubaDiving: false});
    console.log('click horse');
  }
  HandleScubaDiving = () => {
    this.setState({txtHorseBackRiding: false, horseBackRiding: true, txtScubaDiving: true});
    console.log('click scuba');
  }

  

  render(){
    return(
      
          <View style={{backgroundColor: '#191919'}}>
            <View style={{flexDirection: 'row', justifyContent:'space-between', marginRight: 40, marginLeft: 10,}}>
              <TouchableOpacity onPress={this.HandleHorseBackRiding.bind(this)}>
                {
                  this.state.txtHorseBackRiding?
                  <Text style={{marginLeft: 20, fontWeight: '500', color: '#cccccc', fontSize: 20}}>{"Horse Back Riding"}</Text>:
                  <Text style={{marginLeft: 20, color: '#cccccc', fontSize: 20}}>{"Horse Back Riding"}</Text>
                  
                }
              </TouchableOpacity>
              <View>
                <TouchableOpacity onPress={this.HandleScubaDiving.bind(this)}>
                  {
                    this.state.txtScubaDiving?
                    <Text style={{marginLeft: 20, fontWeight: '500', color: '#cccccc', fontSize: 20}}>{"Scuba Diving"}</Text>:
                    <Text style={{marginLeft: 20, color: '#cccccc', fontSize: 20}}>{"Scuba Diving"}</Text>
                    
                  }
                </TouchableOpacity>
              </View>
              
            </View>
            </View>
     
    );
  }
}

export default connect()(Body);

  