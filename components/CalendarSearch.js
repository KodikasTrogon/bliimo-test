import PropTypes from "prop-types";
import React, { Component } from "react";
import { NavigationActions, DrawerActions } from "react-navigation";
import { View, TextInput, Image, TouchableOpacity, Text } from "react-native";
import { connect } from "react-redux";
import { userGetAccount } from "../reducers/reducer";
import customStyles from "../assets/styling/styles";

class CalendarSearch extends Component {
	constructor(props) {
		super(props);
	}
	state = {
		text: ""
	};
	onchange = text => {
		this.setState({ text: text });
		// RUN SEARCH HERE
	};
	HandleBack = () => {
		this.props.goBack();
	}
	render() {
		return (
      <View style={{ flexDirection: 'row', alignItems: 'center', paddingTop: 30}}>
				<View style={customStyles.calendarBtnBack}>
					<TouchableOpacity
						onPress={this.HandleBack.bind(this)}>
						<View>
							<Image source={require('../assets/images/back_gray.png')} style={customStyles.backIcon} resizeMode='contain' />
						</View>
					</TouchableOpacity>
				</View>
				<View style={customStyles.calendarSearch}>
					<Image style={[customStyles.searchImage]} source={require("../assets/images/search.png")} />
					<TextInput
						onChangeText={text => this.onchange(text)}
						value={this.state.text}
						style={{ height: 40, borderWidth: 0, flex: 1, backgroundColor: "transparent", color: "#d1d1d1" }}
						underlineColorAndroid="transparent"
						placeholder={"Search"} />
					{this.state.text != "" && (
						<TouchableOpacity onPress={() => this.onchange("")}>
							<Image style={customStyles.searchClose} source={require("../assets/images/closeSearch.png")} />
						</TouchableOpacity>
					)}
				</View>
			</View>

		);
	}
}
export default CalendarSearch;
