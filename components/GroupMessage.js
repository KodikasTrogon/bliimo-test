import PropTypes from "prop-types";
import React, { Component } from "react";
import { NavigationActions, DrawerActions } from "react-navigation";
import { View, TextInput, Image, Text, TouchableOpacity, Dimensions } from "react-native";
import { connect } from "react-redux";
import { userGetAccount } from "../reducers/reducer";
import customStyles from "../assets/styling/styles";
const { height, width } = Dimensions.get("window");
class GroupMessage extends Component {
  constructor(props) {
    super(props);
  }
  state = {
    isRead: false
  };

  handleMessage = () => {
    this.setState({ isRead: true });
    this.props.navigate.navigate("Chats", { returnPage: { screen: "MainScreen", name: this.props.name } });
  };
  componentWillMount() {}
  render() {
    return (
      <TouchableOpacity onPress={this.handleMessage}>
        <View style={{ flex: 1, flexDirection: "row", justifyContent: "center", marginLeft: 10, marginRight: 10 }}>
          <View style={{ paddingRight: 5 }}>
            <Image source={require("../assets/images/profile.png")} style={{ height: height / 9, width: width / 9, borderRadius: height / 4 }} resizeMode="contain" />
            {!this.state.isRead && <View style={{ height: 10, width: 10, borderRadius: 10 / 2, backgroundColor: "#fff", position: "absolute", bottom: 17, right: 12 }} />}
          </View>
          <View style={{ flexDirection: "column", flex: 2, justifyContent: "center", marginLeft: 5 }}>
            <View style={{ flexDirection: "row" }}>
              <Text style={[customStyles.threadTitle]}>{this.props.name}</Text>
              <Text style={[customStyles.threadTitle, { fontSize: 11, marginLeft: 2, fontWeight: "500" }]}>@{this.props.username}</Text>
            </View>
            <View>
              <Text style={[customStyles.threadContent, { fontSize: 11, marginLeft: 2, fontWeight: "500" }]}>{this.props.message.length > 35 ? this.props.message.substring(0, 35 - 3) + "..." : this.props.message}</Text>
            </View>
          </View>
          <View style={{ justifyContent: "center" }}>
            <Text style={customStyles.threadTime}> {this.props.time} </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}
export default GroupMessage;
