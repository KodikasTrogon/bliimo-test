import React from 'react';
import {
  Text,
  View,
  TouchableNativeFeedback
} from 'react-native';

class ActionButton extends React.Component {
  constructor(props){
    super(props);
  }

  HandleEvent = () => {
    this.props.action();
  }

  render(){
    let color = ((this.props.color == undefined) || (this.props.color == ""))?"#009788":this.props.color;
    let textColor = ((this.props.textColor == undefined) || (this.props.textColor == ""))?"#ffffff":this.props.textColor;
    let name = ((this.props.name == undefined) || (this.props.name == ""))?"Vabby":this.props.name;

    if(!(this.props.disable == undefined) && (this.props.disable)){
      return(
        <View style={{marginBottom:10, height:50, borderRadius:5, overflow:"hidden"}}>
          <View style={{backgroundColor:color,alignItems:"center",padding:10, height:50, justifyContent:"center", alignItems:'center'}}>
              <Text style={{color:textColor}}>Loading...</Text>
          </View>
        </View>
      );
    }
    else{
      return(
        <View style={{marginBottom:10, height:50, overflow:"hidden", elevation:5}}>
          <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple("#FFFFFF")} onPress={this.HandleEvent.bind(this)}>
            <View style={{backgroundColor:color,alignItems:"center",padding:10, width: 200, height: 45, justifyContent:"center", alignItems:'center', borderRadius: 75}}>
                <Text style={{color:textColor, fontWeight: '900', fontSize: 15}}>{name}</Text>
            </View>
          </TouchableNativeFeedback>
        </View>
      );
    }
  }
}

export default ActionButton;