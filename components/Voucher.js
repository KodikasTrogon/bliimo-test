import PropTypes from "prop-types";
import React, { Component } from "react";
import { NavigationActions, DrawerActions } from "react-navigation";
import { View, TextInput, Image, Text, TouchableOpacity, Dimensions } from "react-native";
import { connect } from "react-redux";
import { userGetAccount } from "../reducers/reducer";
import customStyles from "../assets/styling/styles";
const { height, width } = Dimensions.get("window");
class Voucher extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View key={this.props.voucher.id}>
        <TouchableOpacity>
          <View style={{ flexDirection: "row", flex: 1, padding: 20, alignContent: "center", borderBottomColor: "#383838", borderBottomWidth: 1 }}>
            <Text style={[customStyles.threadTitle, { fontSize: 17, fontWeight: "400", width: "70%" }]}>{this.props.voucher.type}</Text>
            <Text style={[customStyles.threadTitle, { fontSize: 17, marginLeft: 2, textAlign: "right", width: "30%", fontWeight: "300", color: "#666666" }]}>{this.props.voucher.time}</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}
export default Voucher;
