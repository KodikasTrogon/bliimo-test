import react from 'react';
import { StyleSheet, Dimensions } from 'react-native';
import { Font } from 'expo';

export const bliimoColor = '#65d7d9';
export const { height, width } = Dimensions.get('window');

export default StyleSheet.create({
	//button
	formButton                      : {
		height    : height / 13,
		overflow  : 'hidden',
		elevation : 5
	},
	formButtonTypeInCode            : {
		height   : height / 15,
		overflow : 'hidden'
	},
	profileButton                   : {
		marginTop    : 30,
		marginBottom : 30,
		height       : 50,
		overflow     : 'hidden',
		elevation    : 5
	},
	buttonContainer                 : {
		paddingTop : 20
	},
	backgroundImage                 : {
		flex     : 1,
		width    : '100%',
		height   : '100%',
		position : 'absolute',
	},
	//input text
	textboxDesign                   : {
		paddingLeft       : 3,
		height            : 30,
		fontSize          : 15,
		color             : '#ffffff',
		borderBottomWidth : 1,
		borderBottomColor : '#606060'
	},
	textboxForm                     : {
		marginTop   : 30,
		marginLeft  : 40,
		marginRight : 40
	},
	btnBack                         : {
		marginTop  : 40,
		marginLeft : 20
	},
	txtLogin                        : {
		marginTop    : 30,
		marginLeft   : 20,
		marginRight  : 20,
		marginBottom : 10
	},
	textForgotPasswordContainer     : {
		marginLeft   : 20,
		marginRight  : 20,
		marginBottom : 25
	},
	displayText                     : {
		fontFamily : 'gotham-bold',
		color      : '#606060',
		fontSize   : 25,
		bottom     : 55
	},
	logoSize                        : {
		width     : 170,
		marginTop : 80
	},
	logoContainer                   : {
		flex       : 1,
		alignItems : 'center'
	},
	container                       : {
		flex      : 1,
		padding   : 50,
		marginTop : 40
	},
	imgSuccessContainer             : {
		alignItems : 'center',
		marginTop  : 100
	},
	imgSuccess                      : {
		width  : 150,
		height : 150
	},
	txtSuccessContainer             : {
		alignItems   : 'center',
		marginTop    : 40,
		padding      : 20,
		marginBottom : 20
	},
	txtSuccess                      : {
		color         : '#56cacd',
		fontFamily    : 'gotham-bold',
		letterSpacing : 1,
		fontSize      : 45
	},
	txtDescription                  : {
		color      : '#606060',
		fontFamily : 'gotham-medium',
		fontSize   : 20,
		textAlign  : 'center'
	},
	welcomeText                     : {
		alignItems   : 'center',
		padding      : 10,
		marginBottom : 50
	},
	formHeader                      : {
		fontSize      : 25,
		fontFamily    : 'gotham-book',
		letterSpacing : 1,
		color         : '#cccccc'
	},
	formHeaderForgotPassword        : {
		fontSize      : 25,
		fontFamily    : 'gotham-bold',
		letterSpacing : 1,
		color         : '#cccccc'
	},
	textEmailContainer              : {
		marginTop  : 40,
		marginLeft : 22
	},
	textLabel                       : {
		fontSize   : 15,
		fontFamily : 'gotham-medium',
		color      : '#cccccc'
  },
  textLabelLight                  : {
    fontSize   : 15,
    fontFamily : 'gotham-light',
    color      : '#cccccc'
  },
	formInput                       : {
		marginLeft  : 20,
		marginRight : 20
	},
	formInputEvent                  : {
		marginLeft    : 20,
		marginRight   : 20,
		paddingBottom : 20
	},
	textPasswordContainer           : {
		marginTop  : 50,
		marginLeft : 22
	},
	passwordButton                  : {
		marginTop : 40
	},
	passwordTextContainer           : {
		alignItems    : 'center',
		paddingBottom : 20
	},
	passwordText                    : {
		fontSize   : 15,
		color      : '#ffffff',
		fontFamily : 'gotham-light'
	},
	submitButtonContainer           : {
		alignSelf       : 'center',
		width           : width / 1.5,
		height          : height / 15,
		backgroundColor : '#ffffff',
		justifyContent  : 'center',
		alignItems      : 'center',
		borderRadius    : 7
	},
	submitButton                    : {
		fontSize   : 15,
		color      : '#898888',
		fontFamily : 'gotham-medium'
	},
	backIcon                        : {
		height : height / 35,
		width  : width / 35
	},
	checkIcon                       : {
		height : height / 19,
		width  : width / 19,
		tintColor: '#666666'
	},
	containerFlex                   : {
		flex            : 1,
		backgroundColor : '#191919',
		height,
	},
	containerList                   : {
		height : height / 1.3,
		flex   : 1
	},
	icon                            : {
		paddingTop : 13,
		alignItems : 'center'
	},
	txtForgotPasswordContainer      : {
		padding : 20
	},
	txtForgotPassword               : {
		color      : '#cccccc',
		fontFamily : 'gotham-light'
	},
	formInputForgotPassword         : {
		flex          : 1,
		marginLeft    : 45,
		marginRight   : 45,
		paddingBottom : 10
	},
	textEmailContainerMargin        : {
		marginTop  : 10,
		marginLeft : 42
	},
	resetButtonContainer            : {
		marginLeft      : 100,
		marginRight     : 100,
		backgroundColor : '#303030',
		justifyContent  : 'center',
		alignItems      : 'center',
		height          : height / 19,
		borderRadius    : 10
	},
	resetButton                     : {
		fontFamily : 'gotham-bold',
		fontSize   : 15,
		color      : '#cccccc',
		textAlign  : 'center'
	},
	hideShowPassword                : {
		top       : 15,
		right     : 5,
		position  : 'absolute',
		alignSelf : 'flex-end',
		color     : '#cccccc'
	},
	txtRedeemContainer              : {
		marginTop    : 130,
		marginBottom : 20,
		marginLeft   : 20,
		marginRight  : 20
	},
	txtRedeem                       : {
		textAlign     : 'center',
		fontSize      : 23,
		fontFamily    : 'gotham-medium',
		letterSpacing : 1,
		color         : '#cccccc'
	},
	textContainer                   : {
		padding : 10
	},
	//barcode
	scannerContainer                : {
		borderTopWidth : 24,
		borderTopColor : bliimoColor
	},
	barCodeScanner                  : {
		width,
		height,
		position : 'absolute'
	},
	scannerBoxContainer             : {
		position : 'relative',
		padding  : 20,
		width,
		height
	},
	scannerCloseButton              : {
		width  : 20,
		height : 20
	},
	scannerView                     : {
		width           : 300,
		height          : 300,
		backgroundColor : 'transparent',
		alignSelf       : 'center',
		borderRadius    : 5,
		borderWidth     : 2,
		borderColor     : '#191919'
	},
	scanningText                    : {
		color     : '#65d7d9',
		textAlign : 'center'
	},
	scannerBox                      : {
		position       : 'absolute',
		height,
		flexDirection  : 'column',
		alignSelf      : 'center',
		justifyContent : 'center',
		alignItems     : 'center'
	},
	// Footer
	footerContainer                 : {
		backgroundColor : '#191919',
		height          : height / 13,
		width,
		elevation: 24
	},
	footerView                      : {
		flex           : 1,
		flexDirection  : 'row',
		justifyContent : 'space-between',
		alignItems     : 'center'
	},
	footerIcon                      : {
		height : height / 12 - 30,
		width  : width / 5
	},
	//PADDING STYLES
	padding5                        : {
		padding : 5
	},
	padding10                       : {
		padding : 10
	},
	padding20                       : {
		padding : 20
	},
	padding30                       : {
		padding : 30
	},
	padding40                       : {
		padding : 40
	},
	paddingLeft5                    : {
		paddingLeft : 5
	},
	paddingRight10                  : {
		paddingRight : 10
	},
	paddingTopBottom10              : {
		paddingTop    : 10,
		paddingBottom : 10
	},
	paddingTop5                     : {
		paddingTop : 5
	},
	paddingTop10                    : {
		paddingTop : 10
	},
	paddingTop15                    : {
		paddingTop : 15
	},
	paddingTop20                    : {
		paddingTop : 20
	},
	paddingTop30                    : {
		paddingTop : 30
	},

	// MARGIN STYLES
	margin5                         : {
		margin : 5
	},
	margin10                        : {
		margin : 10
	},
	margin20                        : {
		margin : 20
	},
	margin30                        : {
		margin : 30
	},
	marginTop10                     : {
		marginTop : 10
	},
	marginTop20                     : {
		marginTop : 20
	},

	//GRID
	flexRow                         : {
		flexDirection : 'row'
	},
	flexRowCenter                   : {
		flexDirection : 'row',
		alignItems    : 'center',
		paddingTop		: 10
	},
	flexRowJustifyCenter            : {
		flexDirection  : 'row',
		justifyContent : 'center'
	},
	flexRowCenterSpaceBetween       : {
		flexDirection  : 'row',
		alignItems     : 'center',
		justifyContent : 'space-between'
	},

	//Header
	headerContainer                 : {
		backgroundColor : '#191919',
		height          : height / 7,
		width           : '100%',
		paddingLeft     : 18,
		paddingRight    : 15,
		paddingBottom   : 5,
		paddingTop			: 30,
	},
	titleContainer                  : {
		padding : 3
	},
	menuImage                       : {
		padding  : 5,
		position : 'absolute',
		right    : 20
	},
	headerArrow                     : {
		height : height * .02,
		width  : width / 8
	},
	headerMenu                      : {
		height : height / 12 - 25,
		width  : width / 5 - 10
	},
	textHeader                      : {
		flexDirection : 'row',
		alignItems    : 'center'
	},
	textHeaderStyle                 : {
		letterSpacing : -0.8,
		fontSize      : 20,
		color         : '#cccccc',
		fontFamily    : 'gotham-medium'
	},
	//Notifications
	textNotificationStyle           : {
		fontSize   : 26,
		color      : '#cccccc',
		fontFamily : 'gotham-bold',
		textAlign  : 'center'
	},
	threadTitle                     : {
		fontSize   : 15,
		color      : '#cccccc',
		fontFamily : 'gotham-medium'
	},
	threadContent                   : {
		fontSize   : 12,
		color      : '#cccccc',
		fontFamily : 'gotham-light'
	},
	threadTime                      : {
		fontSize   : 12,
		color      : '#cccccc',
		fontFamily : 'gotham-medium'
	},
	//EVENT FORM
	txtEventFormHeader              : {
		fontSize      : 17,
		fontFamily    : 'gotham-medium',
		letterSpacing : 1,
		color         : '#cccccc'
	},
	buttonPaddingTop                : {
		paddingTop : 30
	},
	txtEventContent                 : {
		flex      : 1,
		marginTop : 5
	},
	txtAlignCenter                  : {
		textAlign : 'center'
	},
	eventFormBody                   : {
		paddingBottom : 20,
		flexDirection : 'row'
	},
	txtMarginLeftPlus               : {
		marginLeft : 30
	},
	eventFormResult                 : {
		marginHorizontal : 10,
		paddingTop			 : 2
	},
	txtEventFormResult              : {
		fontSize  : 15,
    color     : '#cccccc',
    fontFamily: 'gotham-light'
	},

	//margin
	txtMarginLeft                   : {
		marginLeft : 10
	},
	txtMarginBottom                 : {
		marginBottom : 10
	},
	//Account
	textLabelContainer              : {
		paddingLeft  : 0,
		paddingRight : 0
	},
	formInputAccount                : {
		flex        : 1,
		marginLeft  : 0,
		marginRight : 0
	},
	formSectionContainer            : {
		paddingLeft  : 10,
		paddingRight : 10,
		paddingTop   : 20
	},
	textLabelContainer              : {
		padding : 0
	},
	textAccountLabel                : {
		fontFamily : 'gotham-medium',
		fontSize   : 15,
		color      : '#cccccc'
	},
	headerSetting                   : {
		height : height / 12 - 30,
		width  : width / 5 - 15
	},
	headerSettingContainer          : {
		padding : 15
	},
	headerSettingView               : {
		paddingTop : 40,
		position   : 'absolute',
		right      : 0
	},
	searchView                      : {
		flexDirection   : 'row',
		justifyContent  : 'center',
		alignItems      : 'center',
		backgroundColor : '#111',
		borderWidth     : 0.5,
		borderColor     : '#111',
		height          : 40,
		borderRadius    : 10,
		margin          : 10,
		width           : width * 0.95
	},
	searchImage                     : {
		padding    : 10,
		margin     : 5,
		marginLeft : 10,
		height     : 20,
		width      : 20,
		resizeMode : 'stretch',
		alignItems : 'center'
	},
	writeImage                      : {
		padding    : 10,
		height     : 30,
		width      : 30,
		resizeMode : 'stretch',
		alignItems : 'center'
	},
	searchClose                     : {
		padding    : 10,
		margin     : 5,
		marginLeft : 10,
		height     : 5,
		width      : 5,
		resizeMode : 'stretch',
		alignItems : 'center'
	},
	//LOG IN
	loginContainer                  : {
		marginTop    : height / 3,
		marginLeft   : 20,
		marginRight  : 20,
		marginBottom : 5
	},
	loginText                       : {
		fontSize      : 20,
		fontFamily    : 'gotham-bold',
		letterSpacing : 1,
		color         : '#ffffff'
	},
	loginLine                       : {
		borderBottomColor : '#ffffff',
		borderBottomWidth : 0.3,
		paddingBottom     : 5,
		marginBottom      : 10
	},
	textInputContainer              : {
		marginTop   : 20,
		marginLeft  : 35,
		marginRight : 35
	},
	textInputDesign                 : {
		paddingLeft     : 20,
		height          : height / 13,
		backgroundColor : '#898888',
		borderRadius    : 7,
		fontSize        : 15,
		color           : '#ffffff'
	},
	opacity                         : {
		width           : width,
		height          : height,
		backgroundColor : 'rgba(0,0,0,0.7)'
	},
	btnGotItContainer               : {
		alignSelf       : 'center',
		width           : width / 2.3,
		height          : height / 15,
		backgroundColor : '#ffffff',
		justifyContent  : 'center',
		alignItems      : 'center',
		borderRadius    : 7
	},
	notifMessages                   : {
		justifyContent  : 'center',
		alignItems      : 'center',
		borderRadius    : 100,
		backgroundColor : '#fff',
		width           : 20,
		height          : 20,
		position        : 'absolute',
		right           : 15,
		bottom          : 13,
		zIndex          : 2
	},
	notifMessagesText               : {
		color      : '#565656',
		fontSize   : 11,
		fontFamily : 'gotham-medium'
	},
	//HOME SCREEN
	homeContainer                   : {
		height          : height / 1.52,
		width           : width / 1.129,
		backgroundColor : '#fff',
		borderRadius    : 10,
		backgroundColor : 'transparent'
	},
	flatListTitle                   : {
		color      : '#fff',
		fontFamily : 'gotham-bold',
		fontSize   : 35
	},
	flatListContainer               : {
		paddingLeft    : 10,
		flex           : 1,
		justifyContent : 'flex-end',
		alignItems     : 'flex-start',
		paddingBottom  : 10
	},
	flatListLocation                : {
		color      : '#fff',
		fontSize   : 15,
		fontFamily : 'gotham-light'
	},
	imageRatingStar                 : {
		height : height / 45,
		width  : width / 25
	},
	flatListTextReview              : {
		fontFamily : 'gotham-medium',
		color      : '#fff',
		fontSize   : 15
	},
	flatListPrice                   : {
		fontFamily : 'gotham-medium',
		color      : '#fff',
		fontSize   : 15
	},
	tabBarContainer                 : {
		flexDirection  : 'row',
		justifyContent : 'space-between',
		marginRight    : 40,
		marginLeft     : 10
	},
	clickedTabBar                   : {
		flex      : 1,
		alignSelf : 'center',
		color     : '#cccccc',
		fontSize  : 20
	},
	clickedTabBarScuba              : {
		paddingLeft  : 20,
		color        : '#cccccc',
		fontSize     : 20,
		paddingRight : 15
	},
	highLightLine                   : {
		borderBottomWidth : 1,
		borderBottomColor : '#303030'
	},
	highLightContainer              : {
		flexDirection  : 'row',
		justifyContent : 'center',
		alignContent   : 'center'
	},
	highLightClicked                : {
		top            : 2,
		position       : 'relative',
		borderTopWidth : 4,
		borderTopColor : '#303030',
		flex           : 1,
		opacity        : 0
	},
	highLightContainerScuba         : {
		right    : 0,
		position : 'absolute',
		top      : 4
	},
	tabBarScrollView                : {
		paddingTop : 20
	},
	opacityContainer                : {
		backgroundColor : 'rgba(0,0,0,0.4)',
		borderRadius    : 10,
		height          : height / 1.52,
		width           : width / 1.129
	},
	//Reward and Redemption Screen
	txtRedeemHeader                 : {
		textAlign     : 'center',
		fontSize      : 25,
		fontFamily    : 'gotham-medium',
		letterSpacing : 1,
		color         : '#cccccc'
	},
	txtRedeemHeaderContainer        : {
		marginTop : 30
	},
	imageContainerProfile           : {
		alignSelf : 'center',
		padding   : 5,
		position  : 'absolute'
	},
	lineContainer                   : {
		borderBottomWidth : 1,
		borderColor       : '#303030'
	},
	imageProfile                    : {
		height       : height / 9.5,
		width        : width / 5.4,
		borderRadius : 100,
		borderWidth  : 3,
		borderColor  : '#cccccc'
	},
	pointsContainer                 : {
		flexDirection  : 'row',
		alignSelf      : 'center',
		justifyContent : 'flex-end',
		alignItems     : 'flex-end'
	},
	numberPoints                    : {
		fontSize   : 80,
		fontFamily : 'gotham-medium',
		color      : '#cccccc'
	},
	txtPoints                       : {
		marginBottom : 15,
		fontSize     : 20,
		fontFamily   : 'gotham-medium',
		color        : '#cccccc'
	},
	space                           : {
		textAlign  : 'center',
		fontSize   : 15,
		fontFamily : 'gotham-medium',
		color      : '#cccccc'
	},
	redeemInstruction               : {
		textAlign : 'center',
		fontSize  : 15,
		color     : '#cccccc'
	},
	txtHeader                       : {
		color     : 'white',
		textAlign : 'center'
	},
	txtTransactionSummaryData       : {
		color       : 'white',
		textAlign   : 'center',
		marginRight : width / 25
	},
	txtVoucherCode                  : {
		color       : 'white',
		textAlign   : 'center',
		marginRight : width / 8
	},
	txtBliimoShare                  : {
		color       : 'white',
		textAlign   : 'center',

		marginRight : width / 7
	},
	txtPointsEarn                   : {
		color       : 'white',
		textAlign   : 'center',
		marginRight : width / 12
	},
	//Account Summary
	textAccountSummaryStyle         : {
		fontSize   : 22,
		color      : '#cccccc',
		fontFamily : 'gotham-medium',
		textAlign  : 'center'
	},
	accountHighlightContainer       : {
		paddingTop : 20,
		position   : 'absolute',
		top        : 4
	},
	accountHighlightLine            : {
		borderBottomWidth : 1,
		width             : width / 1.13,
		borderBottomColor : '#303030'
	},
	accountHighlightClicked         : {
		borderTopWidth : 4,
		width          : width / 3.3,
		borderTopColor : '#303030'
	},
	accountSummaryImageContainer    : {
		paddingTop   : 20,
		paddingLeft  : 20,
		paddingRight : 20
	},
	accountSummaryImageSize         : {
		height       : height / 13,
		width        : width / 7,
		borderWidth  : 2,
		borderRadius : 150,
		borderColor  : '#00d0d0'
	},
	textAccountSummaryAll           : {
		fontSize     : 20,
		color        : '#cccccc',
		fontFamily   : 'gotham-medium',
		marginBottom : 5
	},
	accountSummaryHeaderIcon        : {
		width  : width / 25,
		height : height / 25
	},
	accountSummaryText              : {
		fontSize   : 14,
		color      : '#cccccc',
		fontWeight : '300',
		textAlign  : 'right'
	},
	accountSummaryCategoryContainer : {
		paddingLeft   : 20,
		paddingRight  : 20,
		paddingBottom : 20
	},
	categoryContainer               : {
		paddingTop     : 10,
		paddingBottom  : 10,
		flexDirection  : 'row',
		justifyContent : 'space-between'
	},
	categoryContent                 : {
		padding         : 10,
		backgroundColor : '#1d1d1d',
		width           : height / 4.5,
		height          : height / 5.5,
		borderRadius    : 10
	},
	categoryLabel                   : {
		fontSize : 10,
		color    : '#cccccc'
	},
	categoryNumberValue             : {
		paddingTop     : 15,
		paddingBottom  : 15,
		justifyContent : 'center'
	},
	categoryNumberValueStyle        : {
		fontSize   : 28,
		color      : '#cccccc',
		fontFamily : 'gotham-medium',
		textAlign  : 'center'
	},
	categoryContainerIconSize       : {
		width  : width / 28,
		height : height / 28
	},

	//Collapse
	collapseContainer               : {
		backgroundColor   : '#191919',
		width,
		//zIndex            : 10,
		paddingVertical   : 10,
		paddingHorizontal : 10,
		elevation         : 24
	},
	collapseAdventureItemText       : {
		fontSize   : 18,
		color			 : '#626262',
		fontFamily : 'gotham-medium',
		textAlign  : 'left'
	},
	collapseItemContainer           : {
		paddingBottom : 10
	},
	headerTitleContainer            : {
		zIndex       : 10,
		position     : 'absolute',
		alignContent : 'center',
		marginTop    : 60,
		elevation		 : 3,
		backgroundColor: '#191919'
	},
	headerSubtitleContainer         : {
		zIndex       : 10,
		alignContent : 'center',
		elevation    : 5,
		bottom       : 0,
		backgroundColor: '#191919'
	},
	scrollPadding                   : {
		paddingLeft  : 10,
		paddingRight : 10
	},
	sameMessage                     : {
		paddingTop    : 4,
		paddingBottom : 4
	},
	diffMessage                     : {
		paddingTop    : 7,
		paddingBottom : 7
	},
	//Redeem Success or Cancelled
	redeemScrollContainer           : {
		marginTop : 10
	},
	redeemImageBackground           : {
		flexWrap    : 'wrap',
		width       : undefined,
		height      : height / 1.4,
		marginTop   : 20,
		marginRight : 20,
		marginLeft  : 20,
		alignItems  : 'center',
		elevation   : 2
	},
	redeemImage                     : {
		width  : width * 0.9,
		height : height / 1.4
	},
	redeemImageFilter               : {
		backgroundColor         : 'rgba(0,0,0,0.4)',
		height                  : height / 1.4,
		borderTopRightRadius    : 10,
		borderTopLeftRadius     : 10,
		borderBottomLeftRadius  : 10,
		borderBottomRightRadius : 10
	},
	topImageBackgroundText          : {
		flexDirection : 'row',
		alignItems    : 'center'
	},
	topTextStyle                    : {
		color      : '#00d0d0',
		fontSize   : 30,
		fontFamily : 'gotham-bold',
		textAlign  : 'left'
	},
	topCodeStyle                    : {
		color      : '#fff',
		fontSize   : 18,
		fontFamily : 'gotham-light',
		textAlign  : 'right'
	},
	topVoucherStyle                 : {
		color      : '#fff',
		fontSize   : 18,
		fontFamily : 'gotham-book',
		textAlign  : 'right'
	},
	redeemDetailsText               : {
		color      : '#fff',
		fontSize   : 12,
		textAlign  : 'left',
		fontFamily : 'gotham-book'
	},
	redeemVoucherDetailsContainer   : {
		paddingLeft : 5,
		paddingTop  : 20
	},
	redeemVoucherDetailsText        : {
		color      : '#fff',
		fontSize   : 14,
		textAlign  : 'left',
		fontFamily : 'gotham-medium'
	},
	bottomDetailsContainer          : {
		flexDirection : 'row',
		bottom        : 0,
		position      : 'absolute',
		paddingRight  : 10,
		paddingLeft   : 10
	},
	bottomInnerView                 : {
		flex        : 1,
		paddingLeft : 5,
		paddingTop  : 10
	},
	bottomInnerViewText             : {
		color      : '#fff',
		fontSize   : 12,
		textAlign  : 'left',
		fontFamily : 'gotham-medium'
	},
	bottomReviewDetailsContainer    : {
		flexDirection : 'row',
		alignItems    : 'center',
		paddingTop    : 5,
		paddingBottom : 5
	},
	bottomStarReview                : {
		flexDirection  : 'row',
		alignItems     : 'center',
		justifyContent : 'space-between'
	},
	startImageSize                  : {
		width  : width / 30,
		height : height / 30
	},
	numberReviewContainer           : {
		alignItems : 'center'
	},
	numberReviewText                : {
		color      : '#fff',
		fontSize   : 12,
		textAlign  : 'right',
		fontFamily : 'gotham-medium'
	},
	qrImageStyle                    : {
		width  : width / 6,
		height : height / 6
	},
	merchantDetailsContainer        : {
		flex           : 1,
		flexDirection  : 'row',
		justifyContent : 'center',
		marginLeft     : 20,
		marginRight    : 20
	},
	merchantImageProfile            : {
		height       : height / 9,
		width        : width / 9,
		borderRadius : height / 4
	},
	merchantInnerContainer          : {
		flexDirection : 'column',
		flex          : 1,
		paddingTop    : 15
	},
	merchantTitleText               : {
		color      : '#00d0d0',
		fontSize   : 18,
		fontFamily : 'gotham-bold',
		textAlign  : 'left'
	},
	merchantEmailText               : {
		color      : '#fff',
		fontSize   : 10,
		fontFamily : 'gotham-light',
		textAlign  : 'left'
	},
	merchantPriceText               : {
		fontSize   : 12,
		color      : '#cccccc',
		fontFamily : 'gotham-medium',
		textAlign  : 'right'
	},
	merchantDurationContainer       : {
		flexDirection : 'row',
		alignItems    : 'center'
	},
	merchantDurationImage           : {
		flexDirection  : 'row',
		justifyContent : 'center'
	},
	merchantDurationImageSize       : {
		width  : width / 30,
		height : height / 30
	},
	merchantDurationText            : {
		fontSize   : 12,
		color      : '#cccccc',
		fontFamily : 'gotham-medium',
		textAlign  : 'right'
	},
	merchantLabel                   : {
		fontSize   : 18,
		fontFamily : 'gotham-bold',
		color      : '#fff'
	},
	merchantDesc                    : {
		fontSize   : 13,
		fontFamily : 'gotham-light',
		color      : '#fff',
		lineHeight : 21
	},
	merchantVoucherStatus           : {
		alignItems    : 'center',
		paddingTop    : 20,
		paddingBottom : 20
	},
	merchantVoucherStatusText       : {
		fontSize   : 22,
		color      : '#fff',
		fontFamily : 'gotham-bold',
		textAlign  : 'center'
	},
	redeemModalButton               : {
		marginTop      : 20,
		alignItems     : 'center',
		borderWidth    : 1,
		borderColor    : '#cccccc',
		justifyContent : 'center',
		paddingTop     : 15,
		paddingBottom  : 15,
		height         : 25,
		borderRadius   : 5,
		width          : width / 3.5
	},
	redeemModalText                 : {
		color      : '#cccccc',
		fontFamily : 'gotham-book',
		fontSize   : 12
	},
	redeemModalDesc                 : {
		color      : '#cccccc',
		fontFamily : 'gotham-medium',
		fontSize   : 17,
		textAlign  : 'center'
	},
	redeemModalDescTitle            : {
		color      : '#cccccc',
		fontFamily : 'gotham-bold',
		fontSize   : 20,
		textAlign  : 'center'
	},
	redeemModalDescContainer        : {
		paddingLeft  : 10,
		paddingRight : 10,
		alignItems   : 'center',
		marginBottom : 2
	},
	redeemModalSecondaryBox         : {
		width           : width / 2 + 120,
		height          : height / 3.5,
		padding         : 40,
		backgroundColor : 'rgba(52, 52, 52, 0.9)',
		borderRadius    : 5,
		elevation       : 5
	},
	redeemModalPrimaryBox           : {
		justifyContent  : 'center',
		flexDirection   : 'row',
		alignContent    : 'center',
		paddingTop      : 200,
		paddingBottom   : 300,
		backgroundColor : 'transparent'
	},

	scrollPickerContainer           : {
		height       : height / 12,
		marginTop    : 10,
		marginBottom : 10
	},
	checkContainer                  : {
		position  : 'absolute',
		alignSelf : 'flex-end',
		marginTop : 30,
		right     : 20
	},

	//CALENDAR COMPONENT
	knobIcon                        : {
		height    : height / 12 - 30,
		width     : width / 5,
		marginTop : -5
	},
	calendarSearch                  : {
		flexDirection   : 'row',
		justifyContent  : 'center',
		alignItems      : 'center',
		backgroundColor : '#111',
		borderWidth     : 0.5,
		borderColor     : '#111',
		height          : 40,
		borderRadius    : 8,
		margin          : 10,
		width           : width * 0.8
	},
	calendarBtnBack                 : {
		marginLeft  : 20,
		marginRight : 20
	},
	calendarBtnBooking              : {
		justifyContent  : 'center',
		borderRadius    : 8,
		backgroundColor : '#666666',
		paddingTop      : 13,
		paddingBottom   : 13,
		marginRight			: 15
	},
	calendarTextBooking             : {
		color      : '#fff',
		fontFamily : 'gotham-light',
		fontSize   : 15,
		textAlign  : 'center'
	},
	calendarImageView               : {
		paddingRight : 15,
		paddingTop   : 15,
		position     : 'absolute',
		right        : 0
	},
	calendarImage                   : {
		width        : width * 0.14,
		height       : width * 0.14,
		borderRadius : width * 0.24
	},
	calendarTitleLabel              : {
		color      : '#fff',
		fontFamily : 'gotham-medium',
		fontSize   : 13
	},
	calendarDetailsLabel            : {
		color      : '#fff',
		fontFamily : 'gotham-light',
		fontSize   : 11
	},
	calendarTextStatus              : {
		color      : '#fff',
		fontFamily : 'gotham-medium',
		fontSize   : 15
	},
	calendarTitleLabelHighlight     : {
		color      : '#161616',
		fontFamily : 'gotham-medium',
		fontSize   : 13
	},
	calendarDetailsLabelHighlight   : {
		color      : '#161616',
		fontFamily : 'gotham-light',
		fontSize   : 11
	},
	calendarTextStatusHighlight     : {
		color      : '#161616',
		fontFamily : 'gotham-medium',
		fontSize   : 15
	},
	//Account Analytics
	accountAnalyticsImageContainer  : {
		paddingLeft  : 20,
		paddingRight : 20
	},
	accountAnalyticsBlankContainer  : {
		paddingBottom : 0,
		paddingTop    : 25
	},
	accountAnalyticsArrow           : {
		height : width / 30,
		width  : width / 30
	},
	accountArrow                    : {
		marginLeft : 15
	},
	textAccountAnalyticsHeadMaster  : {
		fontSize   : 22,
		color      : '#cccccc',
		fontFamily : 'gotham-bold',
		textAlign  : 'center'
	},
	textAccountAnalyticsHeadAll     : {
		fontSize   : 18,
		color      : '#cccccc',
		fontFamily : 'gotham-bold',
		textAlign  : 'center'
	},
	textAccountAnalyticsAll         : {
		fontSize     : 18,
		color        : '#cccccc',
		fontFamily   : 'gotham-book',
		marginBottom : 5
	},
	analyticsLabel                  : {
		fontSize : 17,
		color    : '#cccccc'
	},
	analyticsContent                : {
		padding         : 10,
		backgroundColor : '#1d1d1d',
		width           : width * 0.9,
		height          : height / 5,
		borderRadius    : 10
	},
	analyticsPhp                    : {
		fontSize   : 15,
		color      : '#cccccc',
		fontFamily : 'gotham-medium',
		position   : 'absolute',
		bottom     : 40,
		left       : 80
	}
});
