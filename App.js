import React from 'react';
import { StatusBar } from 'react-native';
import { createDrawerNavigator, createStackNavigator } from 'react-navigation';
import { createBottomTabNavigator, createAppContainer } from 'react-navigation';

import { KeepAwake } from 'expo';

import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import axios from 'axios';
import multiClientMiddleware from 'redux-axios-middleware';
import reducer from './reducers/reducer';
import Login from './screens/Login';
import ForgotPassword from './screens/ForgotPassword';
import ChangePassword from './screens/ChangePassword';
import RedeemPage from './screens/RedeemPage';
import Notifications from './screens/Notifications';
import Messages from './screens/Messages';
import Scanner from './screens/Scanner';
import SavedEvents from './screens/SavedEvents';
import BookingForm from './screens/BookingForm';
import Account from './screens/Account';
import BookingFormConfirmation from './screens/BookingFormConfirmation';
import RedeemSuccess from './screens/RedeemSuccess';
import RewardScreen from './screens/RewardScreen';
import AccountSummary from './screens/AccountSummary';
import Chats from './screens/Chats';
import SearchVouchers from './screens/SearchVouchers';
import TransactionSummary from './screens/TransactionSummary';
import CalendarScreen from './screens/CalendarScreen';
import AnalyticsMasterList from './screens/AnalyticsMasterList';
import AnalyticsActivityList from './screens/AnalyticsActivityList';
import AccountMerchant from './screens/AccountMerchant';
import AccountAnalytics from './screens/AccountAnalytics';

import Tab from './components/Tab';

const client = axios.create({
	method       : 'post',
  baseURL: 'http://bliimov2-dev.ap-southeast-1.elasticbeanstalk.com', //'http://bliimov2-dev.ap-southeast-1.elasticbeanstalk.com',
	responseType : 'json'
});

const store = createStore(reducer, applyMiddleware(multiClientMiddleware(client)));

const TabNav = createBottomTabNavigator(
	{
		SavedEvents      : {
			screen : SavedEvents
		},
		Notifications    : {
			screen : Notifications
		},
		AccountAnalytics : {
			screen : AccountAnalytics
		},
		Messages         : {
			screen : Messages
		},
		RedeemPage       : {
			screen : RedeemPage
		}
	},
	{
		tabBarComponent : Tab,
		tabBarPosition  : 'bottom'
	}
);

const AppStack = createStackNavigator(
	{
		Login                 : {
			screen : Login
		},
		ForgotPassword        : {
			screen : ForgotPassword
		},
		ChangePassword        : {
			screen : ChangePassword
		},
		RedeemPage            : {
			screen : TabNav
		},
		Notifications         : {
			screen : TabNav
		},
		Scanner               : {
			screen : Scanner
		},
		SavedEvents           : {
			screen : TabNav
		},
		Messages              : {
			screen : TabNav
		},
		Account               : {
			screen : Account
		},
    BookingForm             : {
      screen: BookingForm
		},
		BookingFormConfirmation : {
      screen: BookingFormConfirmation
		},
		RedeemSuccess         : {
			screen : RedeemSuccess
		},
		RewardScreen          : {
			screen : RewardScreen
		},
		AccountSummary        : {
			screen : AccountSummary
		},
		Chats                 : {
			screen : Chats
		},
		SearchVouchers        : {
			screen : SearchVouchers
		},
		TransactionSummary    : {
			screen : TransactionSummary
		},
		CalendarScreen        : {
			screen : CalendarScreen
		},
		AnalyticsMasterList   : {
			screen : AnalyticsMasterList
		},
		AnalyticsActivityList : {
			screen : AnalyticsActivityList
		},
		AccountMerchant       : {
			screen : AccountMerchant
		},
		AccountAnalytics      : {
			screen : TabNav
		}
	},
	{
		initialRouteName  : 'Login',
		navigationOptions : {
      header      : null,
      gesturesEnabled: false,
			headerStyle : {
				backgroundColor : 'transparent'
			}
		}
	}
);

export default class App extends React.Component {
	componentDidMount() {
		StatusBar.setHidden(false);
	}
	render() {
		KeepAwake.activate(); // deactivate when running localhost emulator
		return (
			<Provider store={store}>
				<AppStack />
			</Provider>
		);
	}
}
