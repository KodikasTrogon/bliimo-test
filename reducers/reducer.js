import firebase from 'firebase';
import {
	firebase_apiKey,
	firebase_authDomain,
	firebase_databaseURL,
	firebase_storageBucket,
	firebase_projectId
} from '../keys.js';
import { AsyncStorage } from 'react-native';
import axios from 'axios';

export const USER_AUTH = 'USER_AUTH';
export const USER_AUTH_SUCCESS = 'USER_AUTH_SUCCESS';
export const USER_AUTH_FAIL = 'USER_AUTH_FAIL';

export const USER_ROLE = 'USER_ROLE';
export const USER_ROLE_SUCCESS = 'USER_ROLE_SUCCESS';
export const USER_ROLE_FAIL = 'USER_ROLE_FAIL';

export const GET_MERCHANT_DATA = 'GET_MERCHANT_DATA';
export const GET_MERCHANT_DATA_SUCCESS = 'GET_MERCHANT_DATA_SUCCESS';
export const GET_MERCHANT_DATA_FAIL = 'GET_MERCHANT_DATA_FAIL';

export const CATALOG_FILTERED_CONTENT_DATA = 'CATALOG_FILTERED_CONTENT_DATA';
export const CATALOG_FILTERED_CONTENT_DATA_SUCCESS = 'CATALOG_FILTERED_CONTENT_DATA_SUCCESS';
export const CATALOG_FILTERED_CONTENT_DATA_FAIL = 'CATALOG_FILTERED_CONTENT_DATA_FAIL';

export const GET_CATALOG_DATA = 'GET_CATALOG_DATA';
export const GET_CATALOG_DATA_SUCCESS = 'GET_CATALOG_DATA_SUCCESS';
export const GET_CATALOG_DATA_FAIL = 'GET_CATALOG_DATA_FAIL';

export const GET_MESSAGE = 'GET_MESSAGE';
export const GET_MESSAGE_SUCCESS = 'GET_MESSAGE_SUCCESS';
export const GET_MESSAGE_FAIL = 'GET_MESSAGE_FAIL';

export const GET_CHATLIST = 'GET_CHATLIST';
export const GET_CHATLIST_SUCCESS = 'GET_CHATLIST_SUCCESS';
export const GET_CHATLIST_FAIL = 'GET_CHATLIST_FAIL';

export const SEND_MESSAGE = 'SEND_MESSAGE';
export const SEND_MESSAGE_SUCCESS = 'SEND_MESSAGE_SUCCESS';
export const SEND_MESSAGE_FAIL = 'SEND_MESSAGE_FAIL';

export const CATALOG_CONTENT_DATA = 'CATALOG_CONTENT_DATA';
export const CATALOG_CONTENT_DATA_SUCCESS = 'CATALOG_CONTENT_DATA_SUCCESS';
export const CATALOG_CONTENT_DATA_FAIL = 'CATALOG_CONTENT_DATA_FAIL';

export const BOOKING_DATA = 'BOOKING_DATA';
export const BOOKING_DATA_SUCCESS = 'BOOKING_DATA_SUCCESS';
export const BOOKING_DATA_FAIL = 'BOOKING_DATA_FAIL';

export const GET_SPECIFIC_CONTENT = 'GET_SPECIFIC_CONTENT';
export const GET_SPECIFIC_CONTENT_SUCCESS = 'GET_SPECIFIC_CONTENT_SUCCESS';
export const GET_SPECIFIC_CONTENT_FAIL = 'GET_SPECIFIC_CONTENT_FAIL';

export const GET_CONTENT_VOUCHER = 'GET_CONTENT_VOUCHER';
export const GET_CONTENT_VOUCHER_SUCCESS = 'GET_CONTENT_VOUCHER_SUCCESS';
export const GET_CONTENT_VOUCHER_FAIL = 'GET_CONTENT_VOUCHER_FAIL';

export const GET_SCHEDULE = 'GET_SCHEDULE';
export const GET_SCHEDULE_SUCCESS = 'GET_SCHEDULE_SUCCESS';
export const GET_SCHEDULE_FAIL = 'GET_SCHEDULE_FAIL';

export default function reducer(
	state = {
		authState : [],
    roleState : [],
		messagesState: [],
		sendMessageState: [],
		merchantState : [],
		contentFilteredState  : [],
		catalogState  : [],
		contentState : [],
		bookingState : [],
		specificContentState: [],
		contentVoucherState: [],
		scheduleState: []
	},
	action
) {
	switch (action.type) {
		case USER_AUTH:
			return { ...state, loading: true };
		case USER_AUTH_SUCCESS:
			return { ...state, loading: false, authState: action.payload.data };
		case USER_AUTH_FAIL:
			return { ...state, loading: false, authState: action.error, error: 'Error while fetching data' };

		case USER_ROLE:
			return { ...state, loading: true };
		case USER_ROLE_SUCCESS:
			return { ...state, loading: false, roleState: action.payload.data };
		case USER_ROLE_FAIL:
			return { ...state, loading: false, roleState: 'failed', error: 'Error while fetching data' };
			
		case GET_MESSAGE:
			return { ...state, loading: true };
		case GET_MESSAGE_SUCCESS:
			return { ...state, loading: false, messagesState: action.payload.data };
		case GET_MESSAGE_FAIL:
			return { ...state, loading: false, messagesState: 'failed', error: 'Error while fetching data' };
      
    case GET_CHATLIST:
			return { ...state, loading: true };
		case GET_CHATLIST_SUCCESS:
			return { ...state, loading: false, chatsState: action.payload.data };
		case GET_CHATLIST_FAIL:
			return { ...state, loading: false, chatsState: 'failed', error: 'Error while fetching data' };

		case SEND_MESSAGE:
			return { ...state, loading: true };
		case SEND_MESSAGE_SUCCESS:
			return { ...state, loading: false, sendMessageState: action.payload.data };
		case SEND_MESSAGE_SUCCESS:
			return { ...state, loading: false, sendMessageState: 'failed', error: 'Error while fetching data' };

		case GET_MERCHANT_DATA:
			return { ...state, loading: true };
		case GET_MERCHANT_DATA_SUCCESS:
			return { ...state, loading: false, merchantState: action.payload.data };
		case GET_MERCHANT_DATA_FAIL:
			return { ...state, loading: false, merchantState: 'failed', error: 'Error while fetching data' };

		case CATALOG_FILTERED_CONTENT_DATA:
			return { ...state, loading: true };
		case CATALOG_FILTERED_CONTENT_DATA_SUCCESS:
			return { ...state, loading: false, contentFilteredState: action.payload.data };
		case CATALOG_FILTERED_CONTENT_DATA_FAIL:
			return { ...state, loading: false, contentFilteredState: 'failed', error: 'Error while fetching data' };

		case CATALOG_CONTENT_DATA:
			return { ...state, loading: true };
		case CATALOG_CONTENT_DATA_SUCCESS:
			return { ...state, loading: false, contentState: action.payload.data };
		case CATALOG_CONTENT_DATA_FAIL:
			return { ...state, loading: false, contentState: 'failed', error: 'Error while fetching data' };

		case BOOKING_DATA:
			return { ...state, loading: true };
		case BOOKING_DATA_SUCCESS:
			return { ...state, loading: false, bookingState: action.payload.data };
		case BOOKING_DATA_FAIL:
			return { ...state, loading: false, bookingState: 'failed', error: 'Error while fetching data' };

		case GET_CATALOG_DATA:
			return { ...state, loading: true };
		case GET_CATALOG_DATA_SUCCESS:
			return { ...state, loading: false, catalogState: action.payload.data };
		case GET_CATALOG_DATA_FAIL:
			return { ...state, loading: false, catalogState: 'failed', error: 'Error while fetching data' };

		case GET_SPECIFIC_CONTENT:
			return { ...state, loading: true };
		case GET_SPECIFIC_CONTENT_SUCCESS:
			return { ...state, loading: false, specificContentState: action.payload.data };
		case GET_SPECIFIC_CONTENT_FAIL:
			return { ...state, loading: false, specificContentState: 'failed', error: 'Error while fetching data' };

		case GET_SPECIFIC_CONTENT:
			return { ...state, loading: true };
		case GET_SPECIFIC_CONTENT_SUCCESS:
			return { ...state, loading: false, specificContentState: action.payload.data };
		case GET_SPECIFIC_CONTENT_FAIL:
			return { ...state, loading: false, specificContentState: 'failed', error: 'Error while fetching data' };
		
		case GET_CONTENT_VOUCHER:
			return { ...state, loading: true };
		case GET_CONTENT_VOUCHER_SUCCESS:
			return { ...state, loading: false, contentVoucherState: action.payload.data };
		case GET_CONTENT_VOUCHER_FAIL:
			return { ...state, loading: false, contentVoucherState: 'failed', error: 'Error while fetching data' };

		case GET_SCHEDULE:
			return { ...state, loading: true };
		case GET_SCHEDULE_SUCCESS:
			return { ...state, loading: false, scheduleState: action.payload.data };
		case GET_SCHEDULE_FAIL:
			return { ...state, loading: false, scheduleState: 'failed', error: 'Error while fetching data' };

		default:
			return state;
	}
}

export function authUser(data) {
	console.log(data.usernameOrEmail, data.password);
	return {
		type    : USER_AUTH,
		payload : {
			request : {
				method  : 'POST',
				url     : '/api/login',
				headers : { 'Content-Type': 'application/json' },
				data    : data
			}
		}
	};
}

export function getRole(token) {
	return {
		type    : USER_ROLE,
		payload : {
			request : {
				method  : 'GET',
				url     : '/api/me',
				headers : {
					'Content-Type' : 'application/json',
					Authorization  : `Bearer ${token}`
				}
			}
		}
	};
}

export function getMerchant(token) {
	return {
		type    : GET_MERCHANT_DATA,
		payload : {
			request : {
				method  : 'GET',
				url     : '/api/merchants',
				headers : {
					'Content-Type' : 'application/json',
					Authorization  : `Bearer ${token}`
				}
			}
		}
	};
}

export function getFilteredContent(token, catalogId) {
	return {
		type    : CATALOG_FILTERED_CONTENT_DATA,
		payload : {
			request : {
				method  : 'GET',
				url			: `api/filterContents?catalogId=${catalogId}&offset=0&limit=10`,
				headers : {
					'Content-Type' : 'application/json',
					Authorization  : `Bearer ${token}`
				}
			}
		}
	};
}

export function getContent(token) {
	return {
		type: CATALOG_CONTENT_DATA,
		payload: {
			request: {
				method: 'GET',
				url: 'api/contents',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${token}`
				}
			}
		}
	};
}

export function getCatalog(token) {
	return {
		type    : GET_CATALOG_DATA,
		payload : {
			request : {
				method  : 'GET',
				url: '/api/catalogs?offset=0&limit=8&order=DESC',
				headers : {
					'Content-Type' : 'application/json',
					Authorization  : `Bearer ${token}`
				}
			}
		}
	};
}

export function getSpecificContent(token, contentId) {
	return {
		type: GET_SPECIFIC_CONTENT,
		payload: {
			request: {
				method: 'GET',
				url: `/api/contents/${contentId}`,
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${token}`
				}
			}
		}
	};
}

export function booking(token, bookingData) {
	console.log(bookingData)
	return {
		type: BOOKING_DATA,
		payload: {
			request: {
				method: "POST",
				url: '/api/adhoc',
				headers: {
					Authorization: `Bearer ${token}`,
					'Content-Type': 'application/json'
				},
				data: bookingData
			}
		}
	};
}

export function getSchedule(token, contentId, contentOptionId, date) {
	return {
		type: GET_SCHEDULE,
		payload: {
			request: {
				method: "GET",
				url: `/api/contents/${contentId}/contentOption/${contentOptionId}?date=${date}`,
				headers: {
					Authorization: `Bearer ${token}`,
					'Content-Type': 'application/json'
				}
			}
		}
	};
}

export function getContentVouchers(token, contentId) {
	return {
		type: GET_CONTENT_VOUCHER,
		payload: {
			request: {
				method: "GET",
				url: `/api/contents/${contentId}/vouchers`,
				headers: {
					Authorization: `Bearer ${token}`,
					'Content-Type': 'application/json'
				}
			}
		}
	};
}

export function getMessages(token, chatId, load) {
	return {
		type    : GET_MESSAGE,
		payload: {
      request: {
        method:"GET",
        url: `/api/chat/${chatId}/messages?sort=dateCreated&order=desc&limit=${load}`,
        headers : {
					Authorization  : `Bearer ${token}`
				}
      }
    }
	};
}

export function getChatList(token) {
	return {
		type    : GET_CHATLIST,
		payload: {
      request: {
        method:"GET",
        url: `/api/chats?sort=lastUpdated&order=desc&chatStatus=ACTIVE`,
        headers : {
					Authorization  : `Bearer ${token}`
				}
      }
    }
	};
}

export function sendMessage(token, hash, message) {
	return {
		type    : SEND_MESSAGE,
		payload: {
      request: {
        method:"POST",
        url: `/api/chat/messages?chatHash=${hash}`,
        headers : {
					Authorization  : `Bearer ${token}`,
					'Content-Type': 'application/json'
				},
				data    : message
      }
    }
	};
}


export function firebaseIni() {
	console.log('firebase initialized');
	firebase.initializeApp({
		apiKey        : firebase_apiKey,
		authDomain    : firebase_authDomain,
		databaseURL   : firebase_databaseURL,
		storageBucket : firebase_storageBucket,
		projectId     : firebase_projectId
	});
}

export function getData(query, continues, callback) {
	if (continues) {
		firebase.database().ref(query).on('value', (e) => {
			let messages = e.val();
			callback(messages);
		});
	} else {
		firebase.database().ref(query).once('value', (e) => {
			let messages = e.val();
			callback(messages);
		});
	}
}

export function getProfile(query, email, continues, callback) {
	if (continues) {
		firebase.database().ref(query).orderByChild('email').equalTo(email).on('value', (e) => {
			let messages = e.val();
			callback(messages);
		});
	} else {
		firebase.database().ref(query).orderByChild('email').equalTo(email).on('value', (e) => {
			let messages = e.val();
			callback(messages);
		});
	}
}

export function pushData(query, data) {
	firebase.database().ref(query).push(data);
}

export function setData(query, data) {
	firebase.database().ref(query).set(data);
}

export function addAuth(email, password) {
	firebase
		.auth()
		.createUserWithEmailAndPassword(email, password)
		.then((e) => {
			console.log('success');
		})
		.catch((e) => {
			console.log(e);
		});
}

export function auth(email, password, callback) {
	firebase
		.auth()
		.signInWithEmailAndPassword(email, password)
		.then((e) => {
			callback('success');
		})
		.catch((e) => {
			callback('failed');
		});
}

export function outAuth(callback) {
	firebase.auth().signOut().then(
		function() {
			AsyncStorage.clear();
			callback('success');
			console.log('Signed Out');
		},
		function(error) {
			callback('failed');
			console.error('Sign Out Error', error);
		}
	);
}