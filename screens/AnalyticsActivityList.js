import React from 'react';
import { View, Text, Image, TouchableOpacity, ScrollView, Dimensions, FlatList } from 'react-native';

import { connect } from 'react-redux';
import { firebaseIni, pushData, setData, addAuth, auth, getData, getProfile } from '../reducers/reducer';
import customStyles from '../assets/styling/styles';

const { width, height } = Dimensions.get('window');

class AnalyticsActivityList extends React.Component {
	static navigationOptions = {
		header : null
	};

	state = {
		list     : [ 'All', 'Horse Back Riding', 'ATV', '4x4' ],
		selected : 0
	};

	constructor(props) {
		super(props);
	}

  componentDidMount = () => {
    let {indexActivityList} = this.props.navigation.state.params
    this.setState({ selected: indexActivityList || 0});
  }

	HandleLineSeparator = () => {
		return <View style={{ borderTopWidth: 0.3, borderTopColor: '#cccccc', marginLeft: 20, marginRight: 20 }} />;
	};

	_onPressActivity = (item, index) => {
    this.setState({ selected: index });
    this.props.navigation.navigate('AccountAnalytics', {clickedActivityList: item, indexActivityList: index})
	};

	_renderList = ({ item, index }) => {
		return (
			<TouchableOpacity
				onPress={() => {
					this._onPressActivity(item, index);
				}}
			>
				<View style={{ paddingVertical: 20 }}>
					<Text
						style={{
							color      : '#cccccc',
							fontSize   : 23,
							textAlign  : 'center',
							fontFamily : this.state.selected == index ? 'gotham-bold' : 'gotham-light'
						}}
					>
						{item}
					</Text>
				</View>
			</TouchableOpacity>
		);
	};

	render() {
		return (
			<View style={[ customStyles.containerFlex ]}>
				<View>
					<View style={customStyles.btnBack}>
						<TouchableOpacity
							onPress={() => {
								this.props.navigation.navigate('AccountAnalytics');
							}}
						>
							<View>
								<Image
									source={require('../assets/images/back.png')}
									style={customStyles.backIcon}
									resizeMode="contain"
								/>
							</View>
						</TouchableOpacity>
					</View>
				</View>
				<ScrollView>
					<FlatList
						extraData={this.state}
						data={this.state.list}
						renderItem={this._renderList}
						keyExtractor={(data, index) => index.toString()}
						ItemSeparatorComponent={this.HandleLineSeparator}
					/>
				</ScrollView>
			</View>
		);
	}
}

export default connect()(AnalyticsActivityList);
