import React from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Dimensions,
  Modal,
  Image,
  ScrollView,
  ImageBackground
} from 'react-native';

import { connect } from 'react-redux';
import customStyles from '../assets/styling/styles';
const {height, width} = Dimensions.get('window');

class RedeemSuccess extends React.Component{
  static navigationOptions = {
    header:null,
  };

  constructor() {
    super();
  }  
  
  
  state = {
    redeemStatus: true,
    redeemPrompt: false,
  }

  componentWillMount = () => {
    console.log(this.props.navigation.state.params, "PROPS");
    this.setState({redeemStatus: this.props.navigation.state.params});
  }

  componentDidMount = () => {
    let prompt = this.state.redeemStatus;
    if(prompt == true){
      this.setState({redeemPrompt: prompt});
    }
    else {
      this.setState({redeemPrompt: false});
    }

  }

  HandleGotIt = () => {
    this.setState({redeemPrompt: false});
  }
  
  HandleBackPress = () => {
    this.props.navigation.navigate('CalendarScreen');
  }
  render(){
    return(
      <View style={customStyles.containerFlex}>
        <ScrollView style={customStyles.redeemScrollContainer}>
          <TouchableOpacity onPress={() => {this.HandleBackPress()}}>
            <View style={customStyles.btnBack}>
                <View>
                    <Image source={require('../assets/images/back.png')} style={customStyles.backIcon} resizeMode='contain'/>
                </View>
            </View>
          </TouchableOpacity>
          <View style={customStyles.redeemImageBackground}>
            <ImageBackground source={require('../assets/images/profile/sample1.png')} style={customStyles.redeemImage}
            resizeMode='cover'
            imageStyle={{borderRadius: 10}}
            >
            <View style={customStyles.redeemImageFilter}>
                <View style={customStyles.padding10}>
                  <View style={customStyles.topImageBackgroundText}>
                    <View style={{flex: 1}}>
                      <Text style={customStyles.topTextStyle}> Pass </Text>
                    </View>
                    <View style={{flex: 1}}>
                      <Text style={customStyles.topVoucherStyle}> 82HJEN </Text>
                    </View>
                  </View>
                  
                  <View style={customStyles.paddingLeft5}>
                    <Text style={customStyles.redeemDetailsText}> Profile ID: 00AC4578D  </Text>
                    <Text style={customStyles.redeemDetailsText}> Last Name: Dimangondayao  </Text>
                    <Text style={customStyles.redeemDetailsText}> First Name: Jonah </Text>
                    <Text style={customStyles.redeemDetailsText}> Booking type: Walk-In </Text>
                    <Text style={customStyles.redeemDetailsText}> No. of pax: 5  </Text>
                  </View>

                  <View style={customStyles.redeemVoucherDetailsContainer}>
                    <Text style={customStyles.redeemVoucherDetailsText}> April 08, 2018  </Text>
                    <Text style={customStyles.redeemVoucherDetailsText}> 6:00pm  </Text>
                    <Text style={customStyles.redeemVoucherDetailsText}> Aurora, Baler, PHL </Text>
                  </View>

                </View>

                <View style={customStyles.bottomDetailsContainer}>
                  <View style={customStyles.bottomInnerView}>
                    <Text style={customStyles.bottomInnerViewText}> Horse Back Riding </Text>
                    <Text style={customStyles.bottomInnerViewText}> 2hrs, activity </Text>
                    <Text style={customStyles.bottomInnerViewText}> Aurora, Baler, PHL </Text>

                    <View style={customStyles.bottomReviewDetailsContainer}>
                      <View style={customStyles.bottomStarReview}>
                        <Image source={require('../assets/images/star-review.png')} style={customStyles.startImageSize} resizeMode='contain'/>
                        <Image source={require('../assets/images/star-review.png')} style={customStyles.startImageSize} resizeMode='contain'/>
                        <Image source={require('../assets/images/star-review.png')} style={customStyles.startImageSize} resizeMode='contain'/>
                        <Image source={require('../assets/images/star-review.png')} style={customStyles.startImageSize} resizeMode='contain'/>
                        <Image source={require('../assets/images/star-review-blank.png')} style={customStyles.startImageSize} resizeMode='contain'/>
                      </View>
                      <View style={customStyles.numberReviewContainer}>
                        <Text style={customStyles.numberReviewText}> 25,012 reviews </Text>
                      </View>
                    </View>
                    
                  </View>
                  <View>
                    <Image source={require('../assets/images/qr-sample.png')} style={customStyles.qrImageStyle} resizeMode='contain'/>
                  </View>
                </View>

              </View>

            </ImageBackground>
          </View>

          <View style={customStyles.merchantDetailsContainer} >
          <View style={customStyles.paddingRight10}>
              <Image source={require('../assets/images/Blank-Profile.png')} style={customStyles.merchantImageProfile}
               resizeMode='contain'/>
          </View>
          <View style={customStyles.merchantInnerContainer}>
            <View>
              <Text style={customStyles.merchantTitleText}>Horse Back Riding </Text>
            </View>
            <View>
              <Text style={customStyles.merchantEmailText}>Contact: help@bliimo.com </Text>
            </View>
          </View>
          <View style={{justifyContent: 'center'}}>
            <Text style={customStyles.merchantPriceText}> P100.00 </Text>
            <View style={customStyles.merchantDurationContainer}>
              <View style={customStyles.merchantDurationImage}>
                <Image source={require('../assets/images/stopwatch.png')} style={customStyles.merchantDurationImageSize} resizeMode='contain'/>
              </View>
              <View style={{alignItems: 'center'}}>
                <Text style={customStyles.merchantDurationText}> 2 Hours </Text>
              </View>
            </View>

          </View>

        </View>

          <View style={customStyles.margin20}>
            <View style={customStyles.paddingTopBottom10}>
              <Text style={customStyles.merchantLabel}>The Why</Text>
            </View>
            <View style={customStyles.paddingTopBottom10}>
              <Text style={customStyles.merchantDesc}>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                 Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type
                and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic
              </Text>
            </View>
            <View style={customStyles.paddingTopBottom10}>
              <Text style={customStyles.merchantLabel}>The Experience</Text>
            </View>
            <View style={customStyles.paddingTopBottom10}>
              <Text style={customStyles.merchantDesc}>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                 Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type
                and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic
              </Text>
            </View>
            <View style={customStyles.paddingTopBottom10}>
              <Text style={customStyles.merchantLabel}>Inclusion</Text>
            </View>
            <View style={customStyles.paddingTopBottom10}>
              <Text style={customStyles.merchantDesc}>	{'\u2022' + " "} Guided Horse Back Ride </Text>
              <Text style={customStyles.merchantDesc}>	{'\u2022' + " "} Horse and Helmet </Text>
              <Text style={customStyles.merchantDesc}>	{'\u2022' + " "} nack and Drinks </Text>
              <Text style={customStyles.merchantDesc}>	{'\u2022' + " "} Ride from Park to hotel </Text>
              
            </View>
            <View style={customStyles.paddingTopBottom10}>
              <Text style={customStyles.merchantLabel}>The Cancellation Policy</Text>
            </View>
            <View style={customStyles.paddingTopBottom10}>
              <Text style={customStyles.merchantDesc}>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                 Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type
                and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic
                typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</Text>
            </View>
          </View>
        </ScrollView>
        {
          this.state.redeemStatus? 
          <View style={customStyles.merchantVoucherStatus}>
            <Text style={customStyles.merchantVoucherStatusText}> Redeemed </Text>
          </View> : 
          <View style={customStyles.merchantVoucherStatus}>
            <Text style={customStyles.merchantVoucherStatusText}> Cancelled </Text>
          </View>
        }

        <Modal animationType="slide" transparent={true} visible={this.state.redeemPrompt} onRequestClose={() => {console.log('close')}}>
            <View style = {customStyles.redeemModalPrimaryBox}>
              <View style={customStyles.redeemModalSecondaryBox}>
                <View style={customStyles.redeemModalDescContainer}>
                  <Text style={customStyles.redeemModalDescTitle}>Oops!</Text>
                  <Text style={customStyles.redeemModalDesc}>{"This pass has already"}</Text>
                  <Text style={customStyles.redeemModalDesc}>{"been redeemed."}</Text>
                  
                    <TouchableOpacity
                      onPress={() => {this.HandleGotIt()}}>
                      <View style={customStyles.redeemModalButton}>
                          <Text style={customStyles.redeemModalText}>Got It</Text>
                      </View>
                    </TouchableOpacity>
                </View>
             </View>
          </View>
        </Modal>
        
          

      </View> 
    )
  }
}

export default RedeemSuccess;