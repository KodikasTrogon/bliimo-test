import React from "react";
import { View, Text, TextInput, TouchableOpacity, Dimensions, FlatList, Image, ScrollView } from "react-native";

import { connect } from "react-redux";
import customStyles from "../assets/styling/styles";
import SearchComponents from "../components/Search";
import Voucher from "../components/Voucher";
const { height, width } = Dimensions.get("window");

class SearchVouchers extends React.Component {
  static navigationOptions = {
    header: null
  };

  constructor() {
    super();
  }
  state = {
    Events: [],
    Result: []
  };
  handleEvents = () => {
    data = [
      {
        id: 1,
        type: "Redeemed",
        time: "Jan 2018"
      },
      {
        id: 2,
        type: "Redeemed",
        time: "Jan 2018"
      },
      {
        id: 3,
        type: "Redeemed",
        time: "Jan 2018"
      },
      {
        id: 4,
        type: "Unredeemed",
        time: "Jan 2018"
      },
      {
        id: 5,
        type: "Unredeemed",
        time: "Jan 2018"
      },
      {
        id: 6,
        type: "Unredeemed",
        time: "Jan 2018"
      },
      {
        id: 7,
        type: "Canceled",
        time: "Jan 2018"
      },
      {
        id: 8,
        type: "Canceled",
        time: "Jan 2018"
      },
      {
        id: 9,
        type: "Canceled",
        time: "Jan 2018"
      }
    ];
    this.setState({ Events: data, Result: data });
  };

  handleSearch = text => {
    this.handleEvents();
    searchData = [];
    for (var i = 0; i < this.state.Events.length; i++) {
      if (this.state.Events[i].type.substr(0, text.length).toLowerCase() == text.toLowerCase()) {
        searchData.push(this.state.Events[i]);
      }
    }
    this.setState({ result: searchData });
    if (text.trim().length == 0) {
      this.setState({ Events: [], Result: [] });
    }
    console.log(this.state.Events);
  };
  render() {
    return (
      <View style={[customStyles.containerFlex, { padding: 10, display: "flex" }]}>
        <SearchComponents handleSearch={this.handleSearch.bind(this)} />
        <ScrollView>
          <FlatList data={this.state.result} renderItem={({ item }) => <Voucher voucher={item} />} keyExtractor={(data, index) => index.toString()} />
        </ScrollView>
        <View />
      </View>
    );
  }
}

export default SearchVouchers;
