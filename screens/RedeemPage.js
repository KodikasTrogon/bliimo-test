import React from 'react';
import { View, Text, TextInput, TouchableOpacity, Dimensions, BackHandler } from 'react-native';

import { connect } from 'react-redux';
import { firebaseIni, pushData, setData, addAuth, auth, getData, getProfile } from '../reducers/reducer';
import customStyles from '../assets/styling/styles';
import { validate } from '../components/Validation';
const { height, width } = Dimensions.get('window');

class RedeemPage extends React.Component {
	static navigationOptions = {
		header : null
	};

	constructor() {
		super();
	}

	state = {
		email                 : '',
		password              : '',
		redeemContainerButton : true,
		typeQR                : false
	};

	HandleTypeQR = () => {
		this.setState({ redeemContainerButton: false, typeQR: true });
	};

	HandleEventScanner = () => {
		this.props.navigation.navigate('Scanner');
	};

	componentDidMount() {
		BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
	}

	componentWillUnmount() {
		BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
	}

	handleBackPress = () => {
		this.setState({ redeemContainerButton: true, typeQR: false });
		return true;
	};

	render() {
		return (
			<View style={customStyles.containerFlex}>
				<View style={customStyles.txtRedeemContainer}>
					<Text style={customStyles.txtRedeem}>{"Redeem the user's pass"}</Text>
				</View>
				{this.state.redeemContainerButton && (
					<View style={customStyles.buttonContainer}>
						<View style={customStyles.formButtonTypeInCode}>
							<TouchableOpacity onPress={this.HandleTypeQR.bind(this)}>
								<View style={customStyles.resetButtonContainer}>
									<Text style={customStyles.resetButton}>Type in code</Text>
								</View>
							</TouchableOpacity>
						</View>
						<View style={customStyles.formButton}>
							<TouchableOpacity onPress={this.HandleEventScanner.bind(this)}>
								<View style={customStyles.resetButtonContainer}>
									<Text style={customStyles.resetButton}>Scan QR code</Text>
								</View>
							</TouchableOpacity>
						</View>
					</View>
				)}
				{this.state.typeQR && (
					<View style={{ height: 150 }}>
						<View style={customStyles.textEmailContainerMargin}>
							<Text style={customStyles.textLabel}>Confirmation code:</Text>
						</View>
						<View style={customStyles.formInputForgotPassword}>
							<TextInput
								style={customStyles.textboxDesign}
								placeholder="Type confirmation code here"
								placeholderTextColor="#606060"
								returnKeyType={'done'}
								onChangeText={(text) => this.setState({ email: text })}
								autoCorrect={false}
								selectionColor={'#cccccc'}
								underlineColorAndroid={'transparent'}
							/>
						</View>
						<View style={customStyles.formButton}>
							<TouchableOpacity onPress={this.HandleTypeQR.bind(this)}>
								<View style={customStyles.resetButtonContainer}>
									<Text style={customStyles.resetButton}>Confirm</Text>
								</View>
							</TouchableOpacity>
						</View>
					</View>
				)}
			</View>
		);
	}
}

export default RedeemPage;
