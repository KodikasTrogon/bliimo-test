import React from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Dimensions,
  FlatList,
  Image,
  ScrollView,
  Modal,
  AsyncStorage,
  BackHandler,
  KeyboardAvoidingView
} from 'react-native';

import { connect } from 'react-redux';
import { getRole } from '../reducers/reducer';
import customStyles from '../assets/styling/styles';
import ScrollPicker from '../components/ScrollPicker';
import Loading from './../components/Loading';

const { height, width } = Dimensions.get('window');

class Account extends React.Component {
  static navigationOptions = {
    header: null
  };

  constructor() {
    super();
  }

  state = {
    enabled: true,
    bankList: [
      'BDO Corporate Internet Banking',
      'BDO Over-The-Counter',
      'BDO Over-The-Counter desposit with refer...'
    ],
    successEditModal: false,
    successLogoutModal: false,
    accesToken: '',
    merchantData: '',
    partnerData: '',
    partnerLocation: '',
    partnerBankType: '',
    avatar: 'https://i.imgur.com/erpGfdE.png',
    masterName: '',
    masterLoginEmail: '',
    masterBusinessEmail: '',
    masterPhoneNumber: '',
    masterBusinessAddress: '',
    masterAccountNumber: '',
    load: false
  };

  componentWillMount = () => {
    this.HandleFetchMerchant();
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
  };

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }

  handleBackPress = () => {
    this.props.navigation.navigate('SavedEvents');
    return true;
  };
  // _retrieveData = async () => {
  // 	try {
  // 		const token = await AsyncStorage.getItem('token');
  // 		if (token !== null) {
  // 			// We have data!!
  // 			this.setState({ accesToken: token });
  // 			console.log(token, 'GET ACCESS TOKEN');
  // 		}
  // 	} catch (error) {
  // 		// Error retrieving data
  // 	}
  // };

  HandleFetchMerchant = async () => {
    const token = await AsyncStorage.getItem('token');
    await this.props.getRole(token);
    let getMasterPartnerData = await this.props.roleState;
    if (getMasterPartnerData != 'failed') {
      this.setState({
        masterName: getMasterPartnerData.firstName + ' ' + getMasterPartnerData.lastName,
        masterLoginEmail: getMasterPartnerData.email,
        masterBusinessEmail: getMasterPartnerData.masterPartner.email,
        masterPhoneNumber: getMasterPartnerData.masterPartner.phone,
        masterBusinessAddress: getMasterPartnerData.masterPartner.location.address,
        masterAccountNumber: getMasterPartnerData.masterPartner.payoutAccount,
        load: true
      })
    } else {
      alert('Error fetching data.')
      this.setState({ load: true })
    }
    //Check Avatar
    // console.log(this.state.avatar);
    if (getMasterPartnerData.masterPartner.logo != null) {
      this.setState({ avatar: getMasterPartnerData.masterPartner.logo });
    }
    // console.log('SHOW', this.state.merchantData.partner.name);
    // console.log('Merchant DATA', this.state.partnerBankType);
  };

  HandleUpdate = () => {
    this.setState({ successEditModal: true });

    setTimeout(() => {
      this.setState({ successEditModal: false });
    }, 3000);
  };

  HandleLogout = () => {
    this.setState({ successLogoutModal: true });
  };

  HandleClose = () => {
    this.setState({ successLogoutModal: false });
  };

  HandleSuccessLogout = () => {
    AsyncStorage.clear();
    this.setState({ successLogoutModal: false });
    this.props.navigation.navigate('Login');
  };

  HandleRenderItembankList = (data, index) => {
    console.log(data);
    this.bankList.scrollToIndex(index);
    let selectedValue2 = this.bankList.getSelected();
    return selectedValue2;
  };

  render() {
    if (this.state.load) {
      return (
        <View style={customStyles.containerFlex}>
          <KeyboardAvoidingView behavior="padding" enabled>
            <ScrollView scrollEnabled={this.state.enabled}>
              <View style={customStyles.headerSettingContainer}>
                <View style={customStyles.headerSettingView}>
                  <TouchableOpacity onPress={this.HandleLogout}>
                    <Image
                      source={require('../assets/images/cog.png')}
                      style={customStyles.headerSetting}
                      resizeMode="contain"
                    />
                  </TouchableOpacity>
                </View>
              </View>

              <View style={{ borderBottomColor: '#474747', borderBottomWidth: 1, marginTop: 20, top: 85 }} />

              <View style={{ paddingTop: 20, justifyContent: 'center', alignItems: 'center' }}>
                <View
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderWidth: 4,
                    borderRadius: 150,
                    borderColor: '#fff',
                    overflow: 'hidden',
                    height: 110,
                    width: 110,
                    backgroundColor: '#fff'
                  }}>
                  <Image
                    source={{
                      uri: this.state.avatar
                    }}
                    style={{ height: 110, width: 110 }}
                  />
                </View>
                <View style={{ padding: 20 }}>
                  <Text style={customStyles.textNotificationStyle}>{'MASTER'}</Text>
                </View>
              </View>

              {/* Profile Informations */}
              <View style={customStyles.formSectionContainer}>
                <View style={customStyles.textLabelContainer}>
                  <Text style={customStyles.textAccountLabel}>Name:</Text>
                </View>
                <View style={customStyles.formInputAccount}>
                  <TextInput
                    style={customStyles.textboxDesign}
                    placeholder="Your business name"
                    placeholderTextColor="#606060"
                    returnKeyLabel={'done'}
                    autoCorrect={false}
                    selectionColor={'#fff'}
                    underlineColorAndroid={'transparent'}
                    placeholderTextColor="#fff"
                    onChangeText={(masterName) => this.setState({ masterName })}
                    value={this.state.masterName}
                  />
                </View>
              </View>
              <View style={customStyles.formSectionContainer}>
                <View style={customStyles.textLabelContainer}>
                  <Text style={customStyles.textAccountLabel}>Login Email:</Text>
                </View>
                <View style={customStyles.formInputAccount}>
                  <TextInput
                    style={customStyles.textboxDesign}
                    placeholder="Your email address"
                    placeholderTextColor="#606060"
                    returnKeyLabel={'done'}
                    autoCorrect={false}
                    selectionColor={'#fff'}
                    underlineColorAndroid={'transparent'}
                    placeholderTextColor="#fff"
                    onChangeText={(masterLoginEmail) => this.setState({ masterLoginEmail })}
                    value={this.state.masterLoginEmail}
                  />
                </View>
              </View>
              <View style={customStyles.formSectionContainer}>
                <View style={customStyles.textLabelContainer}>
                  <Text style={customStyles.textAccountLabel}>Business Email:</Text>
                </View>
                <View style={customStyles.formInputAccount}>
                  <TextInput
                    style={customStyles.textboxDesign}
                    placeholder="Your business email address"
                    placeholderTextColor="#606060"
                    returnKeyLabel={'done'}
                    autoCorrect={false}
                    selectionColor={'#fff'}
                    underlineColorAndroid={'transparent'}
                    placeholderTextColor="#fff"
                    onChangeText={(masterBusinessEmail) => this.setState({ masterBusinessEmail })}
                    value={this.state.masterBusinessEmail}
                  />
                </View>
              </View>
              <View style={customStyles.formSectionContainer}>
                <View style={customStyles.textLabelContainer}>
                  <Text style={customStyles.textAccountLabel}>Number:</Text>
                </View>
                <View style={customStyles.formInputAccount}>
                  <TextInput
                    style={customStyles.textboxDesign}
                    placeholder="Your phone number"
                    placeholderTextColor="#606060"
                    returnKeyLabel={'done'}
                    autoCorrect={false}
                    selectionColor={'#fff'}
                    underlineColorAndroid={'transparent'}
                    placeholderTextColor="#fff"
                    onChangeText={(masterPhoneNumber) => this.setState({ masterPhoneNumber })}
                    value={this.state.masterPhoneNumber}
                  />
                </View>
              </View>
              <View style={customStyles.formSectionContainer}>
                <View style={customStyles.textLabelContainer}>
                  <Text style={customStyles.textAccountLabel}>Business Address:</Text>
                </View>
                <View style={customStyles.formInputAccount}>
                  <TextInput
                    style={customStyles.textboxDesign}
                    placeholder="Your business address"
                    placeholderTextColor="#606060"
                    returnKeyLabel={'done'}
                    autoCorrect={false}
                    selectionColor={'#fff'}
                    underlineColorAndroid={'transparent'}
                    placeholderTextColor="#fff"
                    onChangeText={(masterBusinessAddress) => this.setState({ masterBusinessAddress })}
                    value={this.state.masterBusinessAddress}
                  />
                </View>
              </View>
              <View style={customStyles.formSectionContainer}>
                <View style={customStyles.textLabelContainer}>
                  <Text style={customStyles.textAccountLabel}>Account Number:</Text>
                </View>
                <View style={customStyles.formInputAccount}>
                  <TextInput
                    style={customStyles.textboxDesign}
                    placeholder="Your account number"
                    placeholderTextColor="#606060"
                    returnKeyLabel={'done'}
                    autoCorrect={false}
                    selectionColor={'#fff'}
                    underlineColorAndroid={'transparent'}
                    placeholderTextColor="#fff"
                    onChangeText={(masterAccountNumber) => this.setState({ masterAccountNumber })}
                    value={this.state.masterAccountNumber}
                  />
                </View>
              </View>
              <View style={[customStyles.scrollPickerContainer, {paddingLeft: 20}]}>
                <ScrollPicker
                  touchStart={(ev) => {
                    this.setState({ enabled: ev });
                  }}
                  ref={(bankList) => {
                    this.bankList = bankList;
                  }}
                  style={{ width: width - 40 }}
                  dataSource={this.state.bankList}
                  selectedIndex={1}
                  wrapperHeight={height / 11}
                  _renderItem={(data, index) => {
                    return (
                      <View>
                        <Text>{data}</Text>
                      </View>
                    );
                  }}
                  onValueChange={(data, selectedIndex) => {
                    this.HandleRenderItembankList(data, selectedIndex);
                  }}
                />
              </View>
              <View style={customStyles.profileButton}>
                <TouchableOpacity onPress={this.HandleUpdate}>
                  <View style={customStyles.resetButtonContainer}>
                    <Text style={customStyles.resetButton}>Submit</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </ScrollView>
          </KeyboardAvoidingView>


          {/* Modals */}

          <Modal
            animationType="fade"
            transparent={true}
            visible={this.state.successEditModal}
            onRequestClose={() => {
              console.log('close');
            }}
          >
            <View
              style={{
                justifyContent: 'center',
                flexDirection: 'row',
                alignContent: 'center',
                paddingTop: 200,
                paddingBottom: 300,
                backgroundColor: 'transparent'
              }}
            >
              <View
                style={{
                  width: width / 2 + 120,
                  height: height / 2 - 120,
                  backgroundColor: 'rgba(52, 52, 52, 0.9)',
                  borderRadius: 5,
                  elevation: 5
                }}
              >
                <View style={{ paddingTop: 40, paddingBottom: 10, alignSelf: 'center' }}>
                  <Image
                    style={{ width: 50, height: 50 }}
                    source={require('../assets/images/check_icon.png')}
                    resizeMode="contain"
                  />
                </View>
                <View style={{ paddingLeft: 10, paddingRight: 10, alignItems: 'center', marginBottom: 20 }}>
                  <Text style={{ color: '#cccccc', fontWeight: '500', fontSize: 20, textAlign: 'center' }}>Awesome!</Text>
                  <Text style={{ color: '#cccccc', fontWeight: '400', fontSize: 17, textAlign: 'center' }}>
                    {'Changes to your profile have'}
                  </Text>
                  <Text style={{ color: '#cccccc', fontWeight: '400', fontSize: 17, textAlign: 'center' }}>
                    {'been submitted for approval'}
                  </Text>
                </View>
              </View>
            </View>
          </Modal>

          <Modal
            animationType="fade"
            transparent={true}
            visible={this.state.successLogoutModal}
            onRequestClose={() => {
              console.log('close');
            }}
          >
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                flexDirection: 'column',
                alignItems: 'center',
                backgroundColor: 'transparent'
              }}
            >
              <View
                style={{
                  width: width / 2 + 120,
                  height: undefined,
                  backgroundColor: 'rgba(52, 52, 52, 1)',
                  borderRadius: 5,
                  elevation: 5
                }}
              >
                <View style={{ padding: 20 }}>
                  <Text style={{ color: '#cccccc', fontWeight: '500', fontSize: 20, textAlign: 'center' }}>Logout</Text>
                  <Text style={{ color: '#cccccc', fontWeight: '400', fontSize: 17, textAlign: 'center' }}>
                    {'Are you sure you want to logout?'}
                  </Text>
                </View>
                <View style={{ marginTop: 10, borderWidth: 0.5, borderColor: '#cccccc' }} />

                <View
                  style={{ paddingLeft: 10, paddingRight: 10, flexDirection: 'row', justifyContent: 'space-between' }}
                >
                  <View style={{ flex: 1, borderRightWidth: 0.5, borderColor: '#cccccc' }}>
                    <TouchableOpacity onPress={this.HandleClose}>
                      <View style={{ paddingTop: 10, paddingBottom: 10 }}>
                        <Text style={{ color: '#cccccc', fontWeight: '400', fontSize: 20, textAlign: 'center' }}>No</Text>
                      </View>
                    </TouchableOpacity>
                  </View>

                  <View style={{ flex: 1, borderLeftWidth: 0.5, borderColor: '#cccccc' }}>
                    <TouchableOpacity
                      onPress={() => {
                        this.HandleSuccessLogout();
                      }}
                    >
                      <View style={{ paddingTop: 10, paddingBottom: 10 }}>
                        <Text style={{ color: '#cccccc', fontWeight: '400', fontSize: 20, textAlign: 'center' }}>
                          Yes
                        </Text>
                      </View>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </View>
          </Modal>
        </View>
      );
    } else {
      return (
        <View style={customStyles.containerFlex}>
          <Loading />
        </View>
      )
    }
  }
}

const mapStateToProps = (state) => {
  return {
    roleState: state.roleState
  };
};

const mapDispatchToProps = {
  getRole
};

export default connect(mapStateToProps, mapDispatchToProps)(Account);