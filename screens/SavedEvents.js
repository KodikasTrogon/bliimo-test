import React from 'react';
import {
	Text,
	View,
	Image,
	Dimensions,
	ScrollView,
	TouchableOpacity,
	FlatList,
	ImageBackground,
	StyleSheet,
	AsyncStorage,
	BackHandler
} from 'react-native';

import customStyles from '../assets/styling/styles';
import { connect } from 'react-redux';
import { getRole, getMerchant, getFilteredContent, getCatalog, getContent } from '../reducers/reducer';
import Collapsible from 'react-native-collapsible';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import Loading from './../components/Loading';

const { height, width } = Dimensions.get('window');

class SavedEvents extends React.Component {
	constructor(props) {
		super(props);
	}

	state = {
		accordionArrowEvent     : true,
		accordionArrowAdventure : true,
		collapsed               : true,
		headerCollapsed         : true,
		catalogCollections      : [ 'Sample', 'Sample2', 'Sample3', 'Sample4', 'Sample5' ],
		active                  : true,
		activeSlide             : 0,
		showAccount             : false,
		showHome                : true,
		showMerchant            : false,
		showMaster              : false,
		collections             : [],

		fontLoaded              : false,
		headerBtn               : true,
		currentAccountRole      : false,
		roleValue               : '',
		displayName             : '',
    isClickedMerchant       : undefined,
    isClickedCatalog        : undefined,
		cardContents            : [],
		totalScrollCount        : 0,
		catalogData             : [],
		token										: '',
		contentsResult					: '',
		selectedAdventure				: 'Adventures',
		selectedMerchant				: '',
		showMerchantList				: false
	};

	static navigationOptions = {
		header : null
	};

	componentDidMount = async() => {
		const token = await AsyncStorage.getItem('token');
		this.setState({ token })
		console.log(this.props.navigation.state.params.currentUser);
		let current = this.props.navigation.state.params.currentUser;
		this.HandleCheckRole(current);
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
	};

	componentWillUnmount() {
		BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
	}

	handleBackPress = () => {
		return true;
	};

	HandleCheckRole = (role) => {
		if (role == 'ROLE_MERCHANT_MASTER') {
			this.setState({ currentAccountRole: true, roleValue: role, headerBtn: false, selectedMerchant: 'Master Partner' });
			this.HandleFetchMasterData();
		} else {
			this.setState({ currentAccountRole: false, roleValue: role, headerBtn: true, selectedMerchant: 'Merchant' });
			this.HandleFetchMerchantData();
		}
	};

	//Get Data if user is Master Partner
	HandleFetchMasterData = async () => {
		let token = this.state.token;
		await this.props.getRole(this.state.token); //Get data api/me
		let getMerchantData = await this.props.roleState;
		this.setState({
			selectedMerchant	 : getMerchantData.masterPartner.name
		});

		//console.log('NAME', getMerchantData)
		await this.props.getMerchant(token); //Get data api/merchants
		let getMerchants = await this.props.merchantState;
		let collections = [];
		
		if(getMerchants.merchants != ''){
			if (getMerchants) {
				getMerchants.merchants.map((val) => {
					collections = [...collections, val.username];
				});
			}
			this.setState({ collections, showMerchantList: true });
		}else{
			this.setState({ showMerchantList: false })
		}
		await this.props.getContent(token); //Get data api/filterContents?catalogId=1
		let getContentCatalog = await this.props.contentState;
		//console.log('catalog', getContentCatalog)
		let cardContents = [];
		//Get filtered catalog
		if (getContentCatalog.data.length > 0) {
			getContentCatalog.data.map((val) => {
				//console.log(val)
				cardContents = [
					...cardContents,
					{
						cardId: val.id,
						cardTitle: val.title,
						cardAddress: val.provinceName + ', ' + val.localityName + ', ' + val.countryName,
						cardRatingStar: val.ratingTotal,
						cardRatingCount: val.ratingCount,
						cardPhoto: val.photos[0],
						cardSubCategoryName: val.subcategoryName,
						cardPrice: val.displayPrice
					}
				];
			});
			this.setState({ cardContents, totalScrollCount: getContentCatalog.countTotal });
		} else {
			this.setState({ cardContents, contentsResult: 'empty' })
		}

		//Get data api/catalogs
		await this.props.getCatalog(token);
		let getCatalogData = await this.props.catalogState;
		let catalogData = [];
		if (getCatalogData) {
			getCatalogData.data.map((val) => {
				catalogData = [...catalogData, val.catalog.name];
			});
		}
		this.setState({ catalogData });
	};

	//Filter catalogs by adventures
	HandleFilterCatalogContent = async(catalogId) => {
		let token = this.state.token;
		await this.props.getFilteredContent(token, (catalogId + 1)); //Get data api/filterContents?catalogId=1
		let getCatalogContentFiltered = await this.props.contentFilteredState;
		let cardContents = [];
		//Get filtered catalog
		if( getCatalogContentFiltered.data.length > 0 ){
			getCatalogContentFiltered.data.map((val) => {
				cardContents = [
					...cardContents,
					{
						cardId: val.id,
						cardTitle: val.title,
						cardAddress: val.provinceName + ', ' + val.localityName + ', ' + val.countryName,
						cardRatingStar: val.ratingTotal,
						cardRatingCount: val.ratingCount,
						cardPhoto: val.photos[0],
						cardSubCategoryName: val.subcategoryName,
						cardPrice: val.displayPrice
					}
				];
			});
			this.setState({ cardContents, totalScrollCount: getCatalogContentFiltered.countTotal });
		}else{
			this.setState({ cardContents, contentsResult: 'empty' })
		}
	}

	//Get Data if user is Merchant
	HandleFetchMerchantData = async () => {
		const token = await AsyncStorage.getItem('token');
		await this.props.getRole(token);
		let getMerchantData = await this.props.roleState;
		this.setState({
			selectedMerchant	 : getMerchantData.partner.name
		});

		await this.props.getContent(token); //Get data api/filterContents?catalogId=1
		let getContentCatalog = await this.props.contentState;
		let cardContents = [];
		//Get filtered catalog
		if(getContentCatalog.data.length > 0){
			getContentCatalog.data.map((val) => {
				//console.log(val)
				cardContents = [
					...cardContents,
					{
						cardId: val.id,
						cardTitle: val.title,
						cardAddress: val.provinceName + ', ' + val.localityName + ', ' + val.countryName,
						cardRatingStar: val.ratingTotal,
						cardRatingCount: val.ratingCount,
						cardPhoto: val.photos[0],
						cardSubCategoryName: val.subcategoryName,
						cardPrice: val.displayPrice
					}
				];
			});
			this.setState({ cardContents, totalScrollCount: getContentCatalog.countTotal });
		}else{
			this.setState({ cardContents, contentsResult: 'empty' })
		}
	
		//Get data api/catalogs
		await this.props.getCatalog(token);
		let getCatalogData = await this.props.catalogState;
		let catalogData = [];
		if (getCatalogData) {
			getCatalogData.data.map((val) => {
				catalogData = [...catalogData, val.catalog.name];
			});
		}
		this.setState({ catalogData });
	};

	HandleAccordionAdventure = () => {
		if (this.state.accordionArrowAdventure) {
			this.setState({ accordionArrowAdventure: false, accordionArrowEvent: true });
			this.setState({ collapsed: false });
		} else {
			this.setState({ accordionArrowAdventure: true });
			this.setState({ collapsed: true });
		}
	};
	HandleAccordionEvent = () => {
		if (this.state.accordionArrowEvent) {
			this.setState({ accordionArrowEvent: false, accordionArrowAdventure: true });
			this.setState({ headerCollapsed: false });
			this.setState({ collapsed: true });
		} else {
			this.setState({ accordionArrowEvent: true });
			this.setState({ headerCollapsed: true });
		}
  };
  
	HandlePage = (contentId) => {
		console.log(contentId)
		this.props.navigation.navigate('CalendarScreen', {contentId});
	};

	HandleMenu = () => {
		let user = this.state.roleValue;
		console.log('USER', user);
		if (user == 'ROLE_MERCHANT_USER') {
			this.props.navigation.navigate('AccountMerchant');
		} else if (user == 'ROLE_MERCHANT_MASTER') {
			this.props.navigation.navigate('Account');
		} else {
			this.props.navigation.navigate('ChangePassword');
		}
	};

	get pagination() {
		return (
			<ScrollView 
				horizontal
				scrollEnabled={false}
				style={{alignSelf: 'center', position: 'absolute', bottom: 0, width: width * .25 }}
				ref={(c) => { this.scrollView = c; }}
			>
				<View style={{ justifyContent: 'center', alignItems: 'center' }}>
					<Pagination
						// containerStyle ={{marginHorizontal: -20, marginRight: -50}}
						dotContainerStyle={{ height: 10, marginHorizontal: 3, justifyContent: 'center', alignItems: 'center'}}
						dotsLength={this.state.cardContents.length}
						activeDotIndex={this.state.activeSlide}
						dotStyle={{
							width           : 7,
							height          : 7,
							borderRadius    : 5,
							backgroundColor : 'rgba(255, 255, 255, 0.92)'
						}}
						inactiveDotStyle={{
							// Define styles for inactive dots here
						}}
						inactiveDotOpacity={0.4}
						inactiveDotScale={0.6}
					/>
				</View>
			</ScrollView>
		);
	}

	HandleStarLoop = (starCount) => {
		let blankStarsCount = 5 - starCount;
		let stars = [];
		for (let x = 0; x < starCount; x++) {
			stars.push(
				<View key={`star_${x}`} style={{ paddingRight: 3 }}>
					<Image source={require('../assets/images/star-review.png')} style={customStyles.imageRatingStar} />
				</View>
			);
		}
		for (let y = 0; y < blankStarsCount; y++) {
			stars.push(
				<View key={`blankStar_${y}`} style={{ paddingRight: 3 }}>
					<Image source={require('../assets/images/star-review-blank.png')} style={customStyles.imageRatingStar} />
				</View>
			);
		}
		return stars;
	};

	HandleCardContent = ({ item, index }, parallaxProps) => {
		// console.log(item);
		return (
			<View style={{paddingTop:30}}>
				<TouchableOpacity
					activeOpacity={0.5}
					onPress={() => {
						this.HandlePage(item.cardId);
					}}
				>
					<View style={customStyles.homeContainer}>
						<ImageBackground
							borderRadius={10}
							style={customStyles.backgroundImage}
							source={{
								uri : item.cardPhoto
							}}
							resizeMode="cover"
						>
							<View style={customStyles.opacityContainer}>
								<View style={customStyles.flatListContainer}>
									<View style={{ paddingBottom: 3 }}>
										<View style={{ paddingHorizontal: 5 }}>
											<Text style={customStyles.flatListTitle}>{item.cardTitle}</Text>
										</View>
										<View style={{ padding: 3 }}>
											<Text style={customStyles.flatListLocation}>{item.cardAddress}</Text>
										</View>
									</View>
									<View style={{ flexDirection: 'row', padding: 3 }}>
										{/* Loop star ratings */}
										{this.HandleStarLoop(item.cardRatingStar)}
										<View style={{ paddingLeft: 5 }}>
											<Text style={customStyles.flatListTextReview}>
												{item.cardRatingCount}
												{'Reviews'}
											</Text>
										</View>
									</View>
								</View>
							</View>
						</ImageBackground>
					</View>
				</TouchableOpacity>

				<View style={{ paddingTop: 8, paddingLeft: 10 }}>
					<Text style={customStyles.flatListPrice}>
						{item.cardSubCategoryName}
						{'\n'}
						{item.cardPrice}
						{' per person'}
					</Text>
				</View>
			</View>
		);
	};

	HandleClickedMerchant = (dataIndex) => {
		this.setState({ isClickedMerchant: dataIndex });
		this.HandleAccordionEvent();
  };
  HandleClickedAdventure = (dataIndex) => {
    this.setState({ isClickedCatalog: dataIndex });
    this.HandleAccordionAdventure();
	};


	HandleShowMerchants = (data) => {
		this.state.isClickedMerchant == data.index ? this.setState({ selectedMerchant: data.item }) : null;
		return (
			<View style={customStyles.collapseItemContainer}>
				<TouchableOpacity
					onPress={() => {
						this.HandleClickedMerchant(data.index);
					}}
				>
					<View
						style={{
							paddingVertical: 5,
						}}
					>
						<Text style={customStyles.collapseAdventureItemText}>{data.item}</Text>
					</View>
				</TouchableOpacity>
			</View>
		);
	};

	HandleShowCatalogs = (data) => {
   	this.state.isClickedCatalog == data.index? this.setState({ selectedAdventure: data.item }) : null;
		return (
			<View style={{  }}>
				<View style={customStyles.collapseItemContainer}>
					<TouchableOpacity
						onPress={() => {
							this.HandleAccordionAdventure();
							this.HandleClickedAdventure(data.index);
							this.HandleFilterCatalogContent(data.index);
						}}
					>
						<View
							style={{
								paddingVertical : 5,
							}}
						>
							<Text style={customStyles.collapseAdventureItemText}>{data.item}</Text>
						</View>
					</TouchableOpacity>
				</View>
			</View>
		);
	};

	_checkCardContents = () => {
		if (this.state.contentsResult == 'empty') {
			return (
				<Text style={{ color: '#666', fontSize: 20, alignSelf: 'center', paddingTop: height * .4, position: 'absolute' }}>No Catalog Contents</Text>
			)
		} else {
			return (
				(Object.entries(this.state.cardContents).length <= 0) && (<Loading />)
			)
		}
	}

	render() {
		return (
			<View style={customStyles.containerFlex}>
				{/* HEADER */}
				<View style={{ flex: 1, display: this.state.showHome ? 'flex' : 'none' }}>
					<View style={customStyles.headerContainer}>
						<View style={{ flexDirection: 'row', width }}>
							<View style={customStyles.titleContainer}>
								<TouchableOpacity
									style={customStyles.textHeader}
									onPress={() => {
										this.HandleAccordionEvent();
									}}
									disabled={this.state.headerBtn}
								>
									<Text style={customStyles.textHeaderStyle}>{this.state.selectedMerchant}</Text>
									{this.state.accordionArrowEvent ? (
										<Image
											source={require('../assets/images/arrow_right.png')}
											style={[ customStyles.headerArrow, { display: this.state.currentAccountRole ? 'flex' : 'none' } ]}
											resizeMode="contain"
										/>
									) : (
										<Image
											source={require('../assets/images/arrow_down.png')}
											style={customStyles.headerArrow}
											resizeMode="contain"
										/>
									)}
								</TouchableOpacity>
							</View>
							<View style={customStyles.menuImage}>
								<TouchableOpacity onPress={this.HandleMenu}>
									<Image
										source={require('../assets/images/menu.png')}
										style={customStyles.headerMenu}
										resizeMode="contain"
									/>
								</TouchableOpacity>
							</View>
						</View>

						<View style={customStyles.titleContainer}>
							<TouchableOpacity
								style={customStyles.textHeader}
								onPress={() => {
									this.HandleAccordionAdventure();
								}}
							>
								<Text style={customStyles.textHeaderStyle}>{this.state.selectedAdventure}</Text>
								{this.state.accordionArrowAdventure ? (
									<Image
										source={require('../assets/images/arrow_right.png')}
										style={customStyles.headerArrow}
										resizeMode="contain"
									/>
								) : (
									<Image
										source={require('../assets/images/arrow_down.png')}
										style={customStyles.headerArrow}
										resizeMode="contain"
									/>
								)}
							</TouchableOpacity>
						</View>
					</View>

					{/* HEADER COLLAPSE ITEMS  */}
					{this.state.showMerchantList ? (
						<View style={customStyles.headerTitleContainer}>
							<Collapsible collapsed={this.state.headerCollapsed} align="center">
								<View style={customStyles.collapseContainer}>
									<ScrollView style={customStyles.scrollPadding}>
										<FlatList
											data={this.state.collections}
											renderItem={this.HandleShowMerchants}
											extraData={this.state.isClickedMerchant}
											keyExtractor={(data, index) => index.toString()}
										/>
									</ScrollView>
								</View>
							</Collapsible>
						</View>
						):(
							<View style={customStyles.headerTitleContainer}>
								<Collapsible collapsed={this.state.headerCollapsed} align="center">
									<View style={customStyles.collapseContainer}>
										<Text style={{ color: '#cccccc', textAlign: 'center'}}>No merchant</Text>
									</View>
								</Collapsible>
							</View>
						)
					}
					{/* COLLAPSE ITEMS */}
					<View style={customStyles.headerSubtitleContainer}>
						<Collapsible collapsed={this.state.collapsed} align="center">
							<View style={customStyles.collapseContainer}>
								<ScrollView style={customStyles.scrollPadding}>
									<FlatList
										data={this.state.catalogData}
                    renderItem={this.HandleShowCatalogs}
                    extraData={this.state.isClickedCatalog}
										keyExtractor={(data, index) => index.toString()}
									/>
								</ScrollView>
							</View>
						</Collapsible>
					</View>
					{/* Carousel */}
					<View
						style={{
							position        : 'absolute',
							zIndex          : 1,
							marginTop       : height / 8.9,
							width           : width
						}}
					>
						{this._checkCardContents()}
						<Carousel
							containerCustomStyle={{ height: height / 1.18, paddingLeft: 7.5 }}
							inactiveSlideScale={1}
							inactiveSlideOpacity={1}
							data={this.state.cardContents}
							renderItem={this.HandleCardContent}
							onSnapToItem={(index) => {
								this.setState({ activeSlide: index }),
								this.scrollView.scrollTo({ x: index * 10 , y: 0, animated: true })
							}}
							sliderWidth={width}
							itemWidth={width / 1.079}
							useScrollView={true}
							swipeThreshold={1}
							shouldOptimizeUpdates={true}
							lockScrollWhileSnapping={true}
						/>
						{this.pagination}
					</View>
				</View>
			</View>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		roleState     : state.roleState,
		merchantState : state.merchantState,
		contentFilteredState : state.contentFilteredState,
		catalogState  : state.catalogState,
		contentState 	: state.contentState
	};
};

const mapDispatchToProps = {
	getRole,
	getMerchant,
	getFilteredContent,
	getCatalog,
	getContent
};

export default connect(mapStateToProps, mapDispatchToProps)(SavedEvents);
