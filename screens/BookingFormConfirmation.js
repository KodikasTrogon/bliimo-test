import React from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  Modal,
  Dimensions,
  AsyncStorage
} from 'react-native';

import { connect } from 'react-redux';
import { booking } from '../reducers/reducer';
import customStyles from '../assets/styling/styles';

const {width, height} = Dimensions.get('window');

class BookingFormConfirmation extends React.Component{
  static navigationOptions = {
    header:null,
  };
  
  state = {
    successModal: false,
    cardContents: [],
    token: ''
  }

  componentDidMount = async () => {
    let { cardTitle, cardDate, cardLocation, cardPrice, cardPriceId, contentOptionId, contentId, customer, remarks, bookingType, cardMaxPax, selectedSchedule, date } = this.props.navigation.state.params
    const token = await AsyncStorage.getItem('token');
    this.setState({ 
      cardContents: {
        cardTitle,
        cardDate,
        cardLocation,
        cardPrice,
        cardPriceId,
        contentOptionId,
        contentId,
        customer,
        remarks,
        bookingType,
        cardMaxPax,
        selectedSchedule,
        date
      },
      token
    })
  }


  HandleConfirmEvent = async() => {
    let item = this.state.cardContents, bookingType = '', token = this.state.token;
    if(item.bookingType == 'Walk-in'){
      bookingType = 'WALK-IN'
    }else{
      bookingType = 'RESERVATION'
    }
    let bookingData = {
      customerInfo: item.customer,
      remarks: item.remarks,
      name: item.cardTitle,
      reservationType: bookingType,
      quantity: item.cardMaxPax,
      contentId: item.contentId,
      contentOptionId: item.contentOptionId,
      paxPriceId: item.cardPriceId,
      reservationDateTime: item.date + ' ' + getParsedTime(item.selectedSchedule)+":00"
    };
    await this.props.booking(token, bookingData);
    let bookingResponse = await this.props.bookingState;
   
    if(bookingResponse != 'failed'){
      this.setState({ successModal: true, textModalHeader: "Awesome!", textModalBooking: "You've successfully booked\nyour activity!", modalImage: require('../assets/images/check_icon.png') });
      setTimeout(() => {
        this.setState({ successModal: false });
        this.props.navigation.navigate('CalendarScreen')
      }, 3000);
    }else{
      this.setState({ successModal: true, textModalHeader: "Failed booking!", textModalBooking: "Something went wrong!", modalImage: require('../assets/images/warning-sign.png') });
      setTimeout(() => {
        this.setState({ successModal: false });
      }, 3000);
    }

    //Split time
    function getParsedTime(time) {
      time = String(time).split(' ');
      let d = String(time[0]).split('A' || 'P');
      console.log(d)
      return d;
    }
  }

  render(){
    let item = this.state.cardContents;
    return(
      <View style={customStyles.containerFlex}>
        <ScrollView> 
          <View>
            <View style={customStyles.btnBack}>
              <TouchableOpacity
                onPress={() => {this.props.navigation.navigate('BookingForm')}}>
                <View>
                  <Image source={require('../assets/images/back_gray.png')} style={customStyles.backIcon} resizeMode='contain'/>
                </View>
              </TouchableOpacity>
            </View>
            <View style={[customStyles.txtLogin, { paddingRight: 130, paddingBottom: 30 }]}>
              <View>
                <Text style={customStyles.txtEventFormHeader}>{item.cardTitle}</Text>
              </View>
              <View style={customStyles.txtEventContent}>
                <Text style={customStyles.textLabelLight}>{item.cardDate}</Text>
                <Text style={customStyles.textLabelLight}>{item.cardLocation}</Text>
              </View>
              <View style={{ position: 'absolute', alignSelf: 'flex-end' }}>
                <Text style={customStyles.txtEventFormHeader}>{item.cardPrice}</Text>
              </View>
            </View>
            <View style={{ paddingLeft: 20, paddingRight: 20 }}>
              <View style={customStyles.eventFormBody}>
                <Text style={customStyles.txtEventFormHeader}>Schedule:</Text>
                <View style={customStyles.eventFormResult}>
                  <Text style={customStyles.txtEventFormResult}>{item.selectedSchedule}</Text>
                </View>
              </View>
              <View style={customStyles.eventFormBody}>
                <Text style={customStyles.txtEventFormHeader}>No. of Pax:</Text>
                <View style={customStyles.eventFormResult}>
                  <Text style={customStyles.txtEventFormResult}>{item.cardMaxPax}</Text>
                </View>
              </View>
              <View style={customStyles.eventFormBody}>
                <Text style={customStyles.txtEventFormHeader}>Booking Type:</Text>
                <View style={customStyles.eventFormResult}>
                  <Text style={customStyles.txtEventFormResult}>{item.bookingType}</Text>
                </View>
              </View>
              <View style={customStyles.eventFormBody}>
                <Text style={customStyles.txtEventFormHeader}>Customer:</Text>
                <View style={customStyles.eventFormResult}>
                  <Text style={customStyles.txtEventFormResult}>{item.customer}</Text>
                </View>
              </View>
              <View style={customStyles.eventFormBody}>
                <Text style={customStyles.txtEventFormHeader}>Remarks:</Text>
                <View style={customStyles.eventFormResult}>
                  <Text style={customStyles.txtEventFormResult}>{item.remarks}</Text>
                </View>
              </View>
            </View>
           
            <View style={[customStyles.buttonPaddingTop]}>
              <TouchableOpacity
                onPress={this.HandleConfirmEvent}>
                <View style={customStyles.resetButtonContainer}>
                    <Text style={customStyles.resetButton}>Confirm</Text>
                </View>
              </TouchableOpacity>
            </View> 
          {/* Modal for success or failed */}
          <Modal animationType="slide" transparent={true} visible={this.state.successModal} onRequestClose={() => {console.log('close')}}>
            <View style = {{justifyContent: 'center', flexDirection: 'row', alignContent: 'center', paddingTop:200, paddingBottom: 300, backgroundColor:"transparent"}}>
              <View style={{width: (width/2)+120, height: (height/2)-120, backgroundColor: 'rgba(52, 52, 52, 0.9)', borderRadius: 5, elevation: 5 }}>
                <View style={{paddingTop: 40, paddingBottom:10, alignSelf: 'center'}}>
                  <Image style={{ width: 50, height: 50}} source={this.state.modalImage} resizeMode= 'contain'/>
                </View>
                <View style={{paddingLeft: 10, paddingRight: 10, alignItems: 'center', marginBottom: 20}}>
                  <Text style={{color: "#cccccc", fontWeight: "500", fontSize: 20, textAlign: 'center'}}>{this.state.textModalHeader}</Text>
                  <Text style={{color: "#cccccc", fontWeight: "400", fontSize: 17, textAlign: 'center'}}>{this.state.textModalBooking}</Text>
                  {/* <Text style={{color: "#cccccc", fontWeight: "400", fontSize: 17, textAlign: 'center'}}>{"your activity!"}</Text> */}
                </View>
             </View>
            </View> 
          </Modal>
          </View>
        </ScrollView> 
      </View> 
    )
  }
}

const mapStateToProps = (state) => {
  return {
    bookingState: state.bookingState,
  };
};

const mapDispatchToProps = {
  booking
};

export default connect(mapStateToProps, mapDispatchToProps)(BookingFormConfirmation);