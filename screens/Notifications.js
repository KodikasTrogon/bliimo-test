import React from 'react';
import { View, Text, TextInput, TouchableOpacity, Dimensions, FlatList, Image, ScrollView } from 'react-native';

import { connect } from 'react-redux';
import customStyles from '../assets/styling/styles';
const { height, width } = Dimensions.get('window');

class Notifications extends React.Component {
	static navigationOptions = {
		header : null
	};

	constructor() {
		super();
	}

	state = {
		notificationThread : [
			'Sample',
			'Sample2',
			'Sample3',
			'Sample4',
			'Sample5',
			'Sample6',
			'Sample7',
			'Sample8',
			'Sample9',
			'Sample10'
		]
	};

	HandleNotifications = () => {
		return (
			<TouchableOpacity>
				<View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', marginLeft: 10, marginRight: 10 }}>
					<View style={{ paddingRight: 10 }}>
						<Image
							source={require('../assets/images/profile.png')}
							style={{ height: height / 9, width: width / 9, borderRadius: height / 4 }}
							resizeMode="contain"
						/>
						<View
							style={{
								height          : 10,
								width           : 10,
								borderRadius    : 10 / 2,
								backgroundColor : '#fff',
								position        : 'absolute',
								bottom          : 17,
								right           : 12
							}}
						/>
					</View>
					<View style={{ flexDirection: 'column', flex: 1, paddingTop: 15 }}>
						<View>
							<Text style={customStyles.threadTitle}>Bliimo Technologies</Text>
						</View>
						<View>
							<Text style={customStyles.threadContent}>Merchant Scubadoo price has changed!</Text>
						</View>
					</View>
					<View style={{ justifyContent: 'center' }}>
						<Text style={customStyles.threadTime}> 5:32 pm </Text>
					</View>
				</View>
			</TouchableOpacity>
		);
	};

	render() {
		return (
			<View style={[customStyles.containerFlex, {paddingTop: 20}]}>
				<View style={{ padding: 20 }}>
					<Text style={customStyles.textNotificationStyle}>Notifications</Text>
				</View>
				<ScrollView style={{ paddingLeft: 10, paddingRight: 10 }}>
					<FlatList
						data={this.state.notificationThread}
						renderItem={this.HandleNotifications}
						keyExtractor={(data, index) => index.toString()}
					/>
				</ScrollView>
			</View>
		);
	}
}

export default Notifications;
