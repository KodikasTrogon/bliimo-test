import React from 'react';
import {
  View,
  Text,
  AppRegistry,
  Alert,
  Button,
  StyleSheet,
  TextInput,
  Image,
  ScrollView,
  TouchableOpacity,
} from 'react-native';

import { connect } from 'react-redux';

import { firebaseIni, pushData, setData, addAuth, auth, getData, getProfile } from '../reducers/reducer';
import customStyles from '../assets/styling/styles';
import { validate } from '../components/Validation';

class ForgotPassword extends React.Component{
  static navigationOptions = {
    header:null,
  };
  
  state = {
    email:"",
    password:"",

  }
  render(){
    return(
      <View style={customStyles.containerFlex}>
        <ScrollView> 
          <View style={customStyles.btnBack}>
            <TouchableOpacity
              onPress={() => {this.props.navigation.navigate('Login')}}>
              <View>
                  <Image source={require('../assets/images/back.png')} style={customStyles.backIcon} resizeMode='contain'/>
              </View>
            </TouchableOpacity>
          </View>
          <View style={customStyles.txtLogin}>
            <Text style={customStyles.formHeaderForgotPassword}>Forgot password? </Text>
          </View>
          <View style={customStyles.textForgotPasswordContainer}>
            <Text style={customStyles.txtForgotPassword}>Please provide your email address below and we will send you instructions on
            how to reset your password.</Text>
          </View>
          <View style={customStyles.textEmailContainerMargin}>
            <Text style={customStyles.textLabel}>Email:</Text>
          </View>
          <View style={customStyles.formInputForgotPassword}>
            <TextInput
              style={customStyles.textboxDesign}
              placeholder="Your email address"
              placeholderTextColor= '#606060'
              returnKeyType = {"done"}
              onChangeText={(text) => this.setState({email:text})}
              autoCorrect={false}
              selectionColor= {'#cccccc'}
              underlineColorAndroid = {'transparent'}
            />
          </View>
          <View style={customStyles.formButton}>
            <TouchableOpacity
              onPress={() => {this.props.navigation.navigate('Login')}}>
              <View style={customStyles.resetButtonContainer}>
                  <Text style={customStyles.resetButton}>Reset</Text>
              </View>
            </TouchableOpacity>
          </View> 
        </ScrollView>
      </View> 
    )
  }
}

export default connect()(ForgotPassword);