import React from 'react';
import { View, Text, TextInput, Image, Dimensions, ScrollView, TouchableOpacity, Modal } from 'react-native';

import { connect } from 'react-redux';
import { firebaseIni, pushData, setData, addAuth, auth, getData, getProfile } from '../reducers/reducer';
import customStyles from '../assets/styling/styles';
import { validate } from '../components/Validation';

const { width, height } = Dimensions.get('window');

class ChangePassword extends React.Component {
	static navigationOptions = {
		header : null
	};

	constructor(props) {
		super(props);
	}

	state = {
		secureCurrentPassword : true,
		passwordCurrentTitle  : true,
		secureNewPassword     : true,
		passwordNewTitle      : true,
		secureConfirmPassword : true,
		passwordConfirmTitle  : true,
		email                 : '',
		password              : '',
		successModal          : false
	};

	HandleConfirmPassword = () => {
		this.setState({ successModal: true });
		setTimeout(() => {
			this.setState({ successModal: false });
		}, 2000);
	};

	//CURRENT PASSWORD
	changeCurrentPasswordType = () => {
		if (this.state.secureCurrentPassword) {
			this.setState({ secureCurrentPassword: !this.state.secureCurrentPassword, passwordCurrentTitle: false });
		} else {
			this.setState({ secureCurrentPassword: !this.state.secureCurrentPassword, passwordCurrentTitle: true });
		}
	};
	//NEW PASSWORD
	changeNewPasswordType = () => {
		if (this.state.secureNewPassword) {
			this.setState({ secureNewPassword: !this.state.secureNewPassword, passwordNewTitle: false });
		} else {
			this.setState({ secureNewPassword: !this.state.secureNewPassword, passwordNewTitle: true });
		}
	};
	//CONFIRM PASSWORD
	changeConfirmPasswordType = () => {
		if (this.state.secureConfirmPassword) {
			this.setState({ secureConfirmPassword: !this.state.secureConfirmPassword, passwordConfirmTitle: false });
		} else {
			this.setState({ secureConfirmPassword: !this.state.secureConfirmPassword, passwordConfirmTitle: true });
		}
	};
	HandleBackPress = () => {
		this.props.navigation.navigate('SavedEvents');
	};

	render() {
		return (
			<View style={customStyles.containerFlex}>
				<ScrollView>
					<View style={customStyles.btnBack}>
						<TouchableOpacity
							onPress={() => {
								this.HandleBackPress();
							}}
						>
							<View>
								<Image
									source={require('../assets/images/back.png')}
									style={customStyles.backIcon}
									resizeMode="contain"
								/>
							</View>
						</TouchableOpacity>
					</View>
					<View style={customStyles.txtLogin}>
						<Text style={customStyles.formHeader}>Change password</Text>
					</View>
					<View style={customStyles.textEmailContainerMargin}>
						<Text style={customStyles.textLabel}>Current password:</Text>
					</View>
					<View style={customStyles.formInputForgotPassword}>
						<TextInput
							style={customStyles.textboxDesign}
							secureTextEntry={this.state.secureCurrentPassword}
							placeholderTextColor="#606060"
							returnKeyType={'done'}
							onChangeText={(text) => this.setState({ email: text })}
							autoCorrect={false}
							selectionColor={'#cccccc'}
              underlineColorAndroid={'transparent'}
						/>
						<Text
							style={customStyles.hideShowPassword}
							name={[ this.state.icEye ]}
							onPress={this.changeCurrentPasswordType}
						>
							{this.state.passwordCurrentTitle ? 'Show' : 'Hide'}
						</Text>
					</View>
					<View style={customStyles.textEmailContainerMargin}>
						<Text style={customStyles.textLabel}>New password:</Text>
					</View>
					<View style={customStyles.formInputForgotPassword}>
						<TextInput
							style={customStyles.textboxDesign}
							secureTextEntry={this.state.secureNewPassword}
							placeholderTextColor="#606060"
							returnKeyType={'done'}
							onChangeText={(text) => this.setState({ email: text })}
							autoCorrect={false}
							selectionColor={'#cccccc'}
              underlineColorAndroid={'transparent'}
						/>
						<Text
							style={customStyles.hideShowPassword}
							name={[ this.state.icEye ]}
							onPress={this.changeNewPasswordType}
						>
							{this.state.passwordNewTitle ? 'Show' : 'Hide'}
						</Text>
					</View>
					<View style={customStyles.textEmailContainerMargin}>
						<Text style={customStyles.textLabel}>Confirm password:</Text>
					</View>
					<View style={customStyles.formInputForgotPassword}>
						<TextInput
							style={customStyles.textboxDesign}
							placeholderTextColor="#606060"
							secureTextEntry={this.state.secureConfirmPassword}
							returnKeyType={'done'}
							onChangeText={(text) => this.setState({ email: text })}
							autoCorrect={false}
							selectionColor={'#cccccc'}
              underlineColorAndroid={'transparent'}
						/>
						<Text
							style={customStyles.hideShowPassword}
							name={[ this.state.icEye ]}
							onPress={this.changeConfirmPasswordType}
						>
							{this.state.passwordConfirmTitle ? 'Show' : 'Hide'}
						</Text>
					</View>
					<View style={{ paddingBottom: 20 }} />
					<View style={customStyles.formButton}>
						<TouchableOpacity onPress={this.HandleConfirmPassword}>
							<View style={customStyles.resetButtonContainer}>
								<Text style={customStyles.resetButton}>Confirm</Text>
							</View>
						</TouchableOpacity>
					</View>
				</ScrollView>

				<Modal
					animationType="fade"
					transparent={true}
					visible={this.state.successModal}
					onRequestClose={() => {
						console.log('close');
					}}
				>
					<View
						style={{
							justifyContent  : 'center',
							flexDirection   : 'row',
							alignContent    : 'center',
							paddingTop      : 200,
							paddingBottom   : 300,
							backgroundColor : 'transparent'
						}}
					>
						<View
							style={{
								width           : width / 2 + 120,
								height          : height / 2 - 120,
								backgroundColor : 'rgba(52, 52, 52, 0.9)',
								borderRadius    : 5,
								elevation       : 5
							}}
						>
							<View style={{ paddingTop: 40, paddingBottom: 10, alignSelf: 'center' }}>
								<Image
									style={{ width: 50, height: 50 }}
									source={require('../assets/images/check_icon.png')}
									resizeMode="contain"
								/>
							</View>
							<View style={{ paddingLeft: 10, paddingRight: 10, alignItems: 'center', marginBottom: 20 }}>
								<Text style={{ color: '#cccccc', fontWeight: '500', fontSize: 20, textAlign: 'center' }}>Awesome!</Text>
								<Text style={{ color: '#cccccc', fontWeight: '400', fontSize: 17, textAlign: 'center' }}>
									{"You've successfully changed"}
								</Text>
								<Text style={{ color: '#cccccc', fontWeight: '400', fontSize: 17, textAlign: 'center' }}>
									{'your password!'}
								</Text>
							</View>
						</View>
					</View>
				</Modal>
			</View>
		);
	}
}

export default connect()(ChangePassword);
