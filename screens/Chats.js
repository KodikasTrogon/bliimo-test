import React from "react";
import { View, Text, TextInput, TouchableOpacity, Dimensions, FlatList, Image, ScrollView, Keyboard, AsyncStorage, RefreshControl } from "react-native";

import { connect } from "react-redux";
import customStyles from "../assets/styling/styles";
import Chat from "../components/Chat";
import { getMessages, getRole, sendMessage } from '../reducers/reducer';
import Loading from './../components/Loading';
import * as emoticons from 'react-native-emoticons';

const { height, width } = Dimensions.get("window");
const token = ''

class Chats extends React.Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
  }
  state = {
    messages: [],

    textHeight: 0,
    chatId: 0,
    limit: 20,
    opacity: 0.5,

    name: "",
    userAvatar: "",
    hash: "",
    messageText: "",
    accountID: "",
    recipientAvatar: "",
    
    refreshing: false,
    scrollToEnd: true,
    sendButton: true,
    timer: false
  };

  componentDidMount = async() => {
    token = await AsyncStorage.getItem('token');
    let {chatId, name, avatar, hash} = this.props.navigation.state.params
    this.setState({chatId: chatId, name: name, recipientAvatar: avatar, hash: hash})
    this.HandleFetchMessages(chatId);
    this.HandleGetUser();
  }

  componentWillMount = () => {
    this.keyboardDidShowListener = Keyboard.addListener("keyboardDidShow", this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener("keyboardDidHide", this._keyboardDidHide);
  } 

  _keyboardDidShow = (e) => {
    this.setState({ textHeight: e.endCoordinates.height });
  };
  
  _keyboardDidHide = () => {
    this.setState({ textHeight: 0 });
  };

  HandleSendMessage = async() => {
    if (this.state.messageText != "") {
      const messageText = emoticons.stringify(this.state.messageText);
      let message = {
        message : messageText,
      };
      console.log(message);
      await this.props.sendMessage(token, this.state.hash, message);
      this.setState({ messageText: "", sendButton: true, opacity: 0.5});
    }else{
      console.log("Empty field")
    }
  };

  HandleGetUser = async() => {
    await this.props.getRole(token);
    let getUser = this.props.roleState || 0
    this.setState({accountID: getUser.id, userAvatar: getUser.avatar})
  }

  HandleFetchMessages = async (chatId) => {
		await this.props.getMessages(token, chatId, this.state.limit);
    let getMessages = await this.props.messagesState || 0, messages = [];
    if(this.state.timer){
      this.clearTimer();
    }else{
      getMessages.messages.map((value) => {
        messages = [...messages, value]
      })
      this.setState({ messages: messages.reverse() });
      this.timer = setTimeout(() => {
        this.HandleFetchMessages(chatId);
      }, 500);
      console.log('start timer message')
    }
  }

  clearTimer = () => {
    console.log("stop timer message")
    clearTimeout(this.timer)
  }

  _onLoadMoreMessage = async() => {
    this.setState({
      limit: this.state.limit + 10,
      refreshing: true,
      scrollToEnd: false
    }, () => {
        this.HandleFetchMessages(this.state.chatId)
        this.setState({refreshing: false});
    });
  }

  HandleGoToMessages = () => {
    this.props.navigation.navigate('Messages', {timer: false});
    this.setState({timer: true})
  };

  onChangeTextMessage(messageText){
   if(messageText != ""){
    this.setState({ messageText, sendButton: false, opacity: 1 })
   }else{
     this.setState({ messageText, sendButton: true, opacity: 0.5 })
   }
  }

  render() {
    return (
      <View style={[customStyles.containerFlex, {display: 'flex', height}]}>
        <View style={{marginTop: 30, padding: 10}}>
          <TouchableOpacity
            onPress={() => {
              this.HandleGoToMessages();
            }}
          >
            <View>
              <Image
                source={require('../assets/images/back.png')}
                style={customStyles.backIcon}
                resizeMode="contain"
              />
            </View>
          </TouchableOpacity>
        </View>   
        <View style={{ borderStyle: "solid", borderStartWidth: width, borderColor: "#2b2b2b", height: 0.5, top: 12 }} />
        <Text style={{ color: "#d1d1d1", fontSize: 15, alignSelf: "center", backgroundColor: "#191919", paddingLeft: 10, paddingRight: 10, marginBottom: 20 }}>{this.state.name}</Text>
        <View>
          { (Object.entries(this.state.messages).length<=0) && (<Loading/>)}
        </View>
        <View >
          <ScrollView 
            style={{padding: 5, height: this.state.textHeight == 0 ? height-175 : height/3.3}}
            ref={(c) => { this.scrollView = c; }}
            onContentSizeChange={ () => {
              if(this.state.scrollToEnd){
                this.scrollView.scrollToEnd({animated: false});
              }
            }}
            onLayout={() => this.scrollView.scrollToEnd({ animated: false })}
            refreshControl={
              <RefreshControl
                onRefresh={this._onLoadMoreMessage}
                refreshing={this.state.refreshing}
              />
            }
          >
          <FlatList
            style={{paddingTop:5, paddingBottom:5}}
            data={this.state.messages} extraData={this.state}
            renderItem={({ item, index }) => 
            (
              <Chat
                message={item.mess}
                messageThread={index < this.state.messages.length-1 ? (this.state.messages[index+1].sender.id != this.state.accountID ? true : false) : this.state.messages[index].sender.id == this.state.accountID ? true : false }
                recipient={item.sender.id}
                recipientAvatar={this.state.recipientAvatar}
                userAvatar={this.state.userAvatar}
                userID = {this.state.accountID}
              />
            )}
            extraData={this.state}
            keyExtractor={(data, index) => index.toString()}
          />
          </ScrollView>
        </View>
        <View style={{flexDirection:'row', justifyContent: 'center', alignContent: 'center', position: 'absolute', bottom: this.state.textHeight,  padding: 12.5, width}}>
          <TextInput
            style={{flex: 1, width: "78%", marginRight: 12, color: "#fff", alignSelf: "flex-start", paddingLeft: 20, paddingRight: 20, height: 40, borderColor: "#2b2b2b", borderRadius: 15, borderWidth: 2, overflow: 'hidden' }}
            onChangeText={(messageText) => this.onChangeTextMessage(messageText)}
            value={this.state.messageText}
            placeholder="What do you want to say?"
            placeholderTextColor="#666"
          />
          <TouchableOpacity onPress={this.HandleSendMessage.bind(this)} disabled={this.state.sendButton}>
            <Image
              source={require("../assets/images/send.png")}
              style={[customStyles.backIcon, { width: width / 10, height: height / 20, alignSelf: "flex-end", marginTop: 2, opacity: this.state.opacity }]}
              resizeMode="contain"
            />
          </TouchableOpacity>
        </View> 
      </View>
    );
  }
}

const mapStateToProps = (state) => {
	return {
    messagesState : state.messagesState,
    roleState: state.roleState,
    sendMessageState: state.sendMessageState
	};
};

const mapDispatchToProps = {
  getMessages,
  getRole,
  sendMessage
};

export default connect(mapStateToProps, mapDispatchToProps)(Chats);