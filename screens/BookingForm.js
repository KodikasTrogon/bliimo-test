import React from 'react';
import {
  View,
  Text,
  AppRegistry,
  Alert,
  Button,
  StyleSheet,
  TextInput,
  Image,
  TouchableOpacity,
  ScrollView,
  Dimensions,
  AsyncStorage,
  KeyboardAvoidingView
} from 'react-native';

import { connect } from 'react-redux';
import { getSpecificContent, getSchedule } from '../reducers/reducer';
import customStyles from '../assets/styling/styles';
import ScrollPicker from '../components/ScrollPicker';
import Loading from './../components/Loading';


const { width, height } = Dimensions.get('window');

class BookingForm extends React.Component {
  static navigationOptions = {
    header: null,
  };

  state = {
    selectedSchedule: '',
    enabled: true,
    bookingType: '',
    customer: '',
    remarks: '',
    token: '',
    contentId: '',
    cardContents: [],
    cardTitle: '',
    cardLocation: '',
    cardDate: '',
    cardPrice: 0,
    cardMaxPax: 1,
    maxPax: [],
    month: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
    load: false,
    error: '',
    schedule: [],
    date: '',
    time: ['12:00 AM', '1:00 AM', '2:00 AM', '3:00 AM', '4:00 AM', '5:00 AM', '6:00 AM', '7:00 AM', '8:00 AM', '9:00 AM', '10:00 AM', '11:00 AM',
           '12:00 PM', '1:00 PM', '2:00 PM', '3:00 PM', '4:00 PM', '5:00 PM', '6:00 PM', '7:00 PM', '8:00 PM', '9:00 PM', '10:00 PM', '11:00 PM']
  }

  constructor(props) {
    super(props);
  };
  
  componentDidMount = async () => {
    let { contentId, currentDay, clickedDay } = this.props.navigation.state.params
    const token = await AsyncStorage.getItem('token');
    this.setState({ token, contentId })
    this.HandleGetSpecificContent();
    this._convertDate(clickedDay, currentDay);
   
  }

  //Convert date
  _convertDate = (clickedDay, currentDay) =>{
    console.log('clicked day', getParsedDate(clickedDay))
    let clickedDay2 = getParsedDate(clickedDay), currentDay2 = getParsedDate(currentDay)
    this.setState({ cardDate: clickedDay2, date: clickedDay2 })
    this.HandleBookingType(clickedDay2, currentDay2);
    //Split date in API
    function getParsedDate(date) {
      date = String(date).split(' ');
      let d = String(date[0]).split('-'), day = '';
      if (d[2].length == 1) {
        day = '0' + d[2];
      }else{
        day = d[2];
      }
      let final = d[0] + '-' + d[1] + '-' + day
      return final;
    }

  }

  //Render selected schedule
  HandleRenderItemSelectSchedule(data, index) {
    this.selectedSchedule.scrollToIndex(index);
    this.setState({ selectedSchedule: data })
    return data
  }

  //Render selected pax
  HandleRenderCardMaxPax(data, index) {
    this.maxPax.scrollToIndex(index);
    this.setState({ cardMaxPax: data })
    return data
  }
  
  //Identifies walk-in or reservation
  HandleBookingType = (currentDay, clickedDay) => {
    if(currentDay != clickedDay){
      this.setState({ bookingType: 'Reservation' })
    }else{
      this.setState({ bookingType: 'Walk-in' })
    }
  }

  //Function to get list of available schedules
  HandleGetSchedule = async() => {
    let token = this.state.token,
        contentId = this.state.contentId;
        contentOptionId =  this.state.cardContents.contentOptionId;
        date = this.state.cardDate + " 00:00:00";
    await this.props.getSchedule(token, contentId, contentOptionId, date);
    let getSchedule = await this.props.scheduleState;
    let timeSlot = [], time = this.state.time, schedule = []

    if( getSchedule.timeSlot.length > 0 ){
      getSchedule.timeSlot.map((value)=>{
        timeSlot = [ ...timeSlot, value];
      })

      let x = []
      for(let i = 0; i < timeSlot.length; i++){
        x.push(timeSlot[i] % 24)
        if(x[i] >= 12){
          schedule.push(time[x[i]])
        }else{
          schedule.push(time[x[i]])
        }
      }
      this.setState({ schedule, load: true, selectedSchedule: schedule[0] })
    }
  }

  //Navigate to booking comfirmation screen
  HandleBookingConfirmation = () => {
    let item = this.state.cardContents;
    console.log(item.cardPriceId)
    if (this.state.customer != "") {
      this.props.navigation.navigate('BookingFormConfirmation', {
        cardTitle: item.cardTitle,
        cardDate: item.cardDate,
        cardLocation: item.cardLocation,
        cardPrice: item.cardPrice,
        cardPriceId: item.cardPriceId,
        contentOptionId: item.contentOptionId,
        contentId: this.state.contentId,
        customer: this.state.customer,
        remarks: this.state.remarks,
        bookingType: this.state.bookingType,
        cardMaxPax: this.state.cardMaxPax,
        selectedSchedule: this.state.selectedSchedule,
        date: this.state.date
      })      
    } else {
      this.setState({ error: '*required' })
    }
  };

  //Get specific content data
  HandleGetSpecificContent = async() => {
    let token = this.state.token, contentId = this.state.contentId;
    await this.props.getSpecificContent(token, contentId);
    let getContent = await this.props.specificContentState;
    let cardDate = new Date(this.state.cardDate), month = this.state.month
    cardDate = month[cardDate.getMonth()] + ' ' + cardDate.getDate() + ', ' + cardDate.getFullYear();
    let maxPax = [];

    for (let x = 1; x <= getContent.contentOptions[0].maxPax; x++){
      maxPax.push( 
        pax = x
      );
      this.setState({ maxPax })
    }

    this.setState({
      cardContents: {
        cardTitle: "" || getContent.subcategory.name,
        cardDate,
        cardLocation: getContent.province.name + ', ' + getContent.locality.name + ', ' + getContent.country.countryName,
        cardPrice: 'PHP' + getContent.contentOptions[0].paxPrice[0].pricePerPax,
        cardPriceId: getContent.contentOptions[0].paxPrice[0].id,
        contentOptionId: "" || getContent.contentOptions[0].id 
      }
    });
    this.HandleGetSchedule();
  }

  render() {
    if(this.state.load){
      let item = this.state.cardContents;
      return (
        <View style={[customStyles.containerFlex]}>
          <KeyboardAvoidingView behavior="position" keyboardVerticalOffset={10} enabled>
          <ScrollView scrollEnabled={this.state.enabled}>
            <View>
              <View style={customStyles.btnBack}>
                <TouchableOpacity
                  onPress={() => { this.props.navigation.navigate('CalendarScreen') }}>
                  <View>
                    <Image source={require('../assets/images/back_gray.png')} style={customStyles.backIcon} resizeMode='contain' />
                  </View>
                </TouchableOpacity>
              </View>
              <View style={{ position: 'absolute', alignSelf: 'flex-end', marginTop: 33, right: 20 }}>
                <TouchableOpacity
                  onPress={() => { this.HandleBookingConfirmation()}}>
                  <View>
                    <Image source={require('../assets/images/check_icon.png')} style={customStyles.checkIcon} resizeMode='contain' />
                  </View>
                </TouchableOpacity>
              </View>
            </View>
            <View style={[customStyles.txtLogin, { paddingRight: 130 }]}>
              <View>
                <Text style={customStyles.txtEventFormHeader}>{item.cardTitle}</Text>
              </View>
              <View style={customStyles.txtEventContent}>
                <Text style={customStyles.textLabelLight}>{item.cardDate}</Text>
                <Text style={customStyles.textLabelLight}>{item.cardLocation}</Text>
              </View>
              <View style={{ position: 'absolute', alignSelf: 'flex-end' }}>
                <Text style={customStyles.txtEventFormHeader}>{item.cardPrice}</Text>
              </View>
            </View>
            <View style={customStyles.txtLogin}>
              <View>
                <Text style={[customStyles.txtEventFormHeader, customStyles.txtAlignCenter]}>Select Schedule:</Text>
              </View>
              <View style={customStyles.scrollPickerContainer}>
                <ScrollPicker
                  touchStart={(ev) => {
                    this.setState({ enabled: ev });
                  }}
                  style={{width: width-40}}
                  ref={(selectedSchedule) => { this.selectedSchedule = selectedSchedule }}
                  dataSource={this.state.schedule}
                  selectedIndex={0}
                  wrapperHeight={height / 11}
                  highlightColor={'#606060'}
                  _renderItem={(data, index) => {
                    return (
                      <View>
                        <Text>{data}</Text>
                      </View>
                    )
                  }}
                  onValueChange={(data, selectedIndex) => {
                    this.HandleRenderItemSelectSchedule(data, selectedIndex)
                  }}
                />
              </View>
              <View>
                <Text style={[customStyles.txtEventFormHeader, customStyles.txtAlignCenter]}>No of Pax:</Text>
              </View>
              <View style={customStyles.scrollPickerContainer}>
                <ScrollPicker
                  touchStart={(ev) => {
                    this.setState({ enabled: ev });
                  }}
                  style={{width: width-40}}
                  ref={(maxPax) => { this.maxPax = maxPax }}
                  dataSource={this.state.maxPax}
                  selectedIndex={0}
                  wrapperHeight={height / 11}
                  highlightColor={'#606060'}
                  _renderItem={(data, index) => {
                    return (
                      <View>
                        <Text>{data}</Text>
                      </View>
                    )
                  }}
                  onValueChange={(data, selectedIndex) => {
                    this.HandleRenderCardMaxPax(data, selectedIndex)
                  }}
                />
              </View>
              <View>
                <Text style={[customStyles.txtEventFormHeader, customStyles.txtAlignCenter]}>Booking Type:</Text>
              </View>
              <View style={customStyles.scrollPickerContainer}>
                <Text style={{color:'#cccccc', textAlign: 'center'}}>{this.state.bookingType}</Text>
              </View>
            </View>
            <View style={[customStyles.formInput, {flexDirection: 'row'}]}>
              <Text style={customStyles.textLabel}>Customer:  </Text>
              <Text style={{ color: 'red', fontFamily:'gotham-light' }}>{this.state.error}</Text>
            </View>
            <View style={[customStyles.formInputEvent, customStyles.txtMarginBottom]}>
              <TextInput
                style={customStyles.textboxDesign}
                placeholder="Email address, full name or contact number"
                placeholderTextColor='#606060'
                returnKeyType={"done"}
                onChangeText={(customer) => {
                  if( customer != ''){
                    this.setState({ customer, error: '' })
                  }else{
                    this.setState({ customer, error: '*required' })
                  }
                }}
                value={this.state.customer}
                autoCorrect={false}
                selectionColor={'#606060'}
                underlineColorAndroid={'transparent'}
              />
            </View>
            <View style={customStyles.formInput}>
              <Text style={customStyles.textLabel}>Remarks:</Text>
            </View>
            <View style={customStyles.formInputEvent}>
              <TextInput
                style={customStyles.textboxDesign}
                placeholder="Tell us something about this booking"
                placeholderTextColor='#606060'
                returnKeyType={"done"}
                onChangeText={(remarks) => this.setState({ remarks })}
                value={this.state.remarks}
                autoCorrect={false}
                selectionColor={'#606060'}
                underlineColorAndroid={'transparent'}
              />
            </View>
          </ScrollView>
          </KeyboardAvoidingView>
        </View>
      )
    }else{
      return(
        <View style={[customStyles.containerFlex]}>
          <Loading />
        </View>
      )
    }
  }

}

const mapStateToProps = (state) => {
  return {
    specificContentState: state.specificContentState,
    scheduleState: state.scheduleState
  };
};

const mapDispatchToProps = {
  getSpecificContent,
  getSchedule
};

export default connect(mapStateToProps, mapDispatchToProps)(BookingForm);