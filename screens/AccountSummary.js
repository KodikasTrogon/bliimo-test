import React from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Dimensions,
  FlatList,
  Image,
  ScrollView, 
  Modal
} from 'react-native';

import { connect } from 'react-redux';
import customStyles from '../assets/styling/styles';
import ScrollPicker from '../components/ScrollPicker';
import Account from '../screens/Account';


const {height, width} = Dimensions.get('window');

class AccountSummary extends React.Component{
  static navigationOptions = {
    header:null,
  };

  constructor() {
    super();
  }  
  
  
  state = {
   showAccountProfile: false,
   showAccountSummary: false,


  }

  HandleAccountProfile = () => {
    this.props.goToAccountProfile();
    console.log("PASOK");
  }
  
  render(){
    return(
      
      <View style={[customStyles.containerFlex, { display: this.props.visibility ? "flex" : "none" }]}>
        <Account 
        visibility={this.state.showAccountProfile} 
        gotoAccountProfile={() => {
          this.setState({showAccountProfile: true})
        }}
        />
        <View style={customStyles.accountSummaryImageContainer}>
          <View style={customStyles.flexRowCenter}>
            <TouchableOpacity onPress={this.HandleAccountProfile.bind(this)}>
              <View style={customStyles.paddingRight10}>
                  <Image source={require('../assets/images/profile/merchant_01.png')} style={customStyles.accountSummaryImageSize}
                  />
              </View>
            </TouchableOpacity>
            
            <View style={customStyles.padding10}>
                <Text style={customStyles.textAccountSummaryStyle}>Master</Text>
            </View>
          </View>

          <View style={customStyles.marginTop20}>
            <View>
              <Text style={customStyles.textAccountSummaryAll}> All </Text>
              <View style={customStyles.flexRowCenter}>
                <View style={customStyles.flexRowCenter}>
                  <View style={customStyles.flexRowJustifyCenter}>
                    <Image source={require('../assets/images/account-summary/clock.png')} style={customStyles.accountSummaryHeaderIcon} resizeMode='contain'/>
                  </View>
                  <View>
                    <Text style={customStyles.accountSummaryText}> 2 Hours </Text>
                  </View>
                </View>
                <View style={{flex: 1}}>
                  <Text style={customStyles.accountSummaryText}> P799.00 </Text>
                </View>
              </View>

              <View style={customStyles.flexRowCenter}>
                <View style={customStyles.flexRowCenter}>
                  <View style={customStyles.flexRowJustifyCenter}>
                    <Image source={require('../assets/images/account-summary/group.png')} style={customStyles.accountSummaryHeaderIcon} resizeMode='contain'/>
                  </View>
                  <View>
                    <Text style={customStyles.accountSummaryText}> 5 Pax </Text>
                  </View>
                </View>
                <View style={{flex: 1}}>
                  <Text style={customStyles.accountSummaryText}> Manila, PH </Text>
                </View>
              </View>

            </View>
            <View style={customStyles.paddingTop20}>
              <View>
                <Text style={customStyles.accountHighlightLine}></Text>
              </View>
              <View style={customStyles.accountHighlightContainer}>
                <Text style={customStyles.accountHighlightClicked}></Text>
              </View>
            </View>
          </View>

        </View>
        
        {/* Select Catergory */}
        <ScrollView>
          <View style={customStyles.accountSummaryCategoryContainer}>
            <View style={customStyles.categoryContainer}>
              <TouchableOpacity>
                <View style={customStyles.categoryContent}>
                  <Text style={customStyles.categoryLabel}>Online Bookings</Text>
                  <View style={customStyles.categoryNumberValue}>
                    <Text style={customStyles.categoryNumberValueStyle}>679</Text>
                  </View>
                  <View style={customStyles.flexRowCenterSpaceBetween}>
                    <View>
                      <Text style={customStyles.categoryLabel}>March - 420</Text>  
                    </View>
                    <View>
                      <Image source={require('../assets/images/account-summary/bookmark.png')} style={customStyles.categoryContainerIconSize} resizeMode='contain'/> 
                    </View>
                  </View>
                </View>
              </TouchableOpacity>
              
              <TouchableOpacity>
                <View style={customStyles.categoryContent}>
                  <Text style={customStyles.categoryLabel}>Walk-In Bookings</Text>
                  <View style={customStyles.categoryNumberValue}>
                    <Text style={customStyles.categoryNumberValueStyle}>679</Text>
                  </View>
                  <View style={customStyles.flexRowCenterSpaceBetween}>
                    <View>
                      <Text style={customStyles.categoryLabel}>March - 290</Text>  
                    </View>
                    <View>
                      <Image source={require('../assets/images/account-summary/footprints.png')} style={customStyles.categoryContainerIconSize} resizeMode='contain'/> 
                    </View>
                  </View>
                </View>
              </TouchableOpacity>
            </View>

            <View style={customStyles.categoryContainer}>
              <TouchableOpacity>
                <View style={customStyles.categoryContent}>
                  <Text style={customStyles.categoryLabel}>Saved</Text>
                  <View style={customStyles.categoryNumberValue}>
                    <Text style={customStyles.categoryNumberValueStyle}>866</Text>
                  </View>
                  <View style={customStyles.flexRowCenterSpaceBetween}>
                    <View>
                      <Text style={customStyles.categoryLabel}>Realtime</Text>  
                    </View>
                    <View>
                      <Image source={require('../assets/images/account-summary/heart.png')} style={customStyles.categoryContainerIconSize} resizeMode='contain'/> 
                    </View>
                  </View>
                </View>
              </TouchableOpacity>
              
              <TouchableOpacity>
                <View style={customStyles.categoryContent}>
                  <Text style={customStyles.categoryLabel}>Shares</Text>
                  <View style={customStyles.categoryNumberValue}>
                    <Text style={customStyles.categoryNumberValueStyle}>203</Text>
                  </View>
                  <View style={customStyles.flexRowCenterSpaceBetween}>
                    <View>
                      <Text style={customStyles.categoryLabel}>Realtime</Text>  
                    </View>
                    <View>
                      <Image source={require('../assets/images/account-summary/share.png')} style={customStyles.categoryContainerIconSize} resizeMode='contain'/> 
                    </View>
                  </View>
                </View>
              </TouchableOpacity>
            </View>

            <View style={customStyles.categoryContainer}>
              <TouchableOpacity>
                <View style={customStyles.categoryContent}>
                  <Text style={customStyles.categoryLabel}>Reviews</Text>
                  <View style={customStyles.categoryNumberValue}>
                    <Text style={customStyles.categoryNumberValueStyle}>264</Text>
                  </View>
                  <View style={customStyles.flexRowCenterSpaceBetween}>
                    <View>
                      <Text style={customStyles.categoryLabel}>Realtime</Text>  
                    </View>
                    <View>
                      <Image source={require('../assets/images/account-summary/star.png')} style={customStyles.categoryContainerIconSize} resizeMode='contain'/> 
                    </View>
                  </View>
                </View>
              </TouchableOpacity>
              
              <TouchableOpacity>
                <View style={customStyles.categoryContent}>
                  <Text style={customStyles.categoryLabel}>Views</Text>
                  <View style={customStyles.categoryNumberValue}>
                    <Text style={customStyles.categoryNumberValueStyle}>2640</Text>
                  </View>
                  <View style={customStyles.flexRowCenterSpaceBetween}>
                    <View>
                      <Text style={customStyles.categoryLabel}>Realtime</Text>  
                    </View>
                    <View>
                      <Image source={require('../assets/images/account-summary/eyes.png')} style={customStyles.categoryContainerIconSize} resizeMode='contain'/> 
                    </View>
                  </View>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>

      </View> 
    )
  }
}

export default AccountSummary;