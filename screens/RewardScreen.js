import React from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Dimensions,
  Image,
  ScrollView
} from 'react-native';

import { connect } from 'react-redux';
import { firebaseIni, pushData, setData, addAuth, auth, getData, getProfile } from '../reducers/reducer';
import customStyles from '../assets/styling/styles';
import { validate } from '../components/Validation';
const {height, width} = Dimensions.get('window');

class RewardScreen extends React.Component{
  static navigationOptions = {
    header:null,
  };

  constructor() {
    super();
  }  
  
  state = {
    email:"",
    password:"",
    redeemContainerButton: true,
    typeQR: false
  }

  HandleTypeQR = () => {
    this.setState({redeemContainerButton: false, typeQR: true});
  }

  HandleEventScanner = () => {
    this.props.actionScanner();
  }

  render(){
    return(
      <View style={[customStyles.containerFlex]}>
        <ScrollView>
          <View style={customStyles.btnBack}>
            <TouchableOpacity
              onPress={() => {this.props.navigation.navigate('Login')}}>
              <View>
                  <Image source={require('../assets/images/back.png')} style={customStyles.backIcon} resizeMode='contain'/>
              </View>
            </TouchableOpacity>
          </View> 
          <View style={{paddingTop: 30}}>
            <Text style={customStyles.lineContainer}></Text>
            <View style={customStyles.imageContainerProfile}>
              <Image source={require('../assets/images/sample1.png')} style={customStyles.imageProfile} resizeMode='cover'/>
            </View>
          </View>
          
          <View style={{paddingTop: 40}}>
            <Text style={customStyles.txtRedeem}>{"Horse Riders PH"}</Text>
          </View>
          <View style={customStyles.txtRedeemHeaderContainer}>
            <Text style={customStyles.txtRedeemHeader}>{"Rewards and Redemption"}</Text>
          </View>
         
          <View style={customStyles.pointsContainer}>
            <Text style={customStyles.numberPoints}>{"200"}</Text>
            <Text style={customStyles.txtPoints}>{"points"}</Text>
          </View>
      
          <View style ={customStyles.buttonContainer}>
            <View style={customStyles.formButton}>
              <TouchableOpacity
                onPress={() => {this.props.navigation.navigate('TransactionSummary')}} 
                >
                <View style={customStyles.resetButtonContainer}>
                    <Text style={customStyles.resetButton}>Transaction summary</Text>
                </View>
              </TouchableOpacity>
            </View> 
            <View style={customStyles.formButton}>
              <TouchableOpacity
                onPress={() => {this.props.navigation.navigate('ConvertToCash')}}
                >
                <View style={customStyles.resetButtonContainer}>
                    <Text style={customStyles.resetButton}>Convert to cash</Text>
                </View>
              </TouchableOpacity>
            </View> 
            <View style={customStyles.formButton}>
              <TouchableOpacity
                onPress={() => {this.props.navigation.navigate('BliimoMarketing')}}
                >
                <View style={customStyles.resetButtonContainer}>
                  <Text style={customStyles.resetButton}>Bliimo marketing</Text>
                </View>
              </TouchableOpacity>
            </View> 
            <View>
              <Text style={customStyles.space}>{"1 point = P1.00"}</Text>
            </View>
            <View style={{paddingTop:10}}>
              <Text style={customStyles.redeemInstruction}>{"Amount redeemed will be credited"}</Text>
              <Text style={customStyles.redeemInstruction}>{"on the next remittance schedule."}</Text>
            </View>
          </View>
        </ScrollView>    
      </View> 
    )
  }
}

export default RewardScreen;