import React from 'react';
import { View, Text, TextInput, TouchableOpacity, Dimensions, FlatList, Image, ScrollView, Modal } from 'react-native';

import { connect } from 'react-redux';
import customStyles from '../assets/styling/styles';
import ScrollPicker from '../components/ScrollPicker';
import CalendarAgenda from '../components/CalendarAgenda';
import { Calendar, Agenda } from 'react-native-calendars';

const { height, width } = Dimensions.get('window');

class AccountAnalytics extends React.Component {
	static navigationOptions = {
		header : null
	};

	constructor(props) {
		super(props);
	}

	state = {
		showAccountProfile : false,
		showAccountSummary : false,
		clickDay           : true,
		clickWeek          : false,
		clickMonth         : false,
		clickYear          : false,
		showAgenda         : true,
		items              : undefined,
		defaultMaster      : false,
		accountProfile     : 'Master',
    merchantProfile    : 'All',
    seconds            : 0,
    indexMasterList    : 0,
		indexActivityList  : 0,
	};

	HandleDayTab = () => {
		this.setState({ clickDay: true, clickWeek: false, clickMonth: false, clickYear: false });
	};
	HandleWeekTab = () => {
		this.setState({ clickDay: false, clickWeek: true, clickMonth: false, clickYear: false });
	};
	HandleMonthTab = () => {
		this.setState({ clickDay: false, clickWeek: false, clickMonth: true, clickYear: false });
	};
	HandleYearTab = () => {
		this.setState({ clickDay: false, clickWeek: false, clickMonth: false, clickYear: true });
	};

	componentDidMount = () => {
    this.HandleCheckMaster();
    this.interval = setInterval(() => this.HanleClickedMasterList(), 3000);
		this.interval = setInterval(() => this.HandleClickedActivityList(), 3000);
  };

	HandleCheckMaster = () => {
		if (this.state.accountProfile != 'Master') {
			this.setState({ defaultMaster: true });
		} else {
			this.setState({ defaultMaster: false });
		}
	};

	HandleMasterPress = () => {
		this.props.navigation.navigate('AnalyticsMasterList', {indexMasterList: this.state.indexMasterList});
	};

	HandleAllPress = () => {
		this.props.navigation.navigate('AnalyticsActivityList', {indexActivityList: this.state.indexActivityList});
  };
  
  HanleClickedMasterList = () => {
    let {clickedMasterList, indexMasterList} = this.props.navigation.state.params
		this.setState({accountProfile: clickedMasterList || 'Master', indexMasterList: indexMasterList});
  };

  HandleClickedActivityList = () => {
    let {clickedActivityList, indexActivityList} = this.props.navigation.state.params
    this.setState({merchantProfile: clickedActivityList || 'All', indexActivityList: indexActivityList});
  };

	render() {
		return (
			<View style={customStyles.containerFlex}>
				<View style={{ borderBottomColor: '#474747', borderBottomWidth: 0.5, marginTop: 30, top: 60 }} />
				<View style={{ paddingTop: 20, height: height * .25 }}>
					<View style={{ alignItems: 'center' }}>
						<Image
							source={require('../assets/images/profile/merchant_01.png')}
							style={{
								height       : width / 5,
								width        : width / 5,
								borderWidth  : 2.5,
								borderRadius : width / 9,
								borderColor  : '#fff'
							}}
						/>
					</View>
					<View style={{ padding: 5, alignItems: 'center' }}>
						<TouchableOpacity
							onPress={() => {
								this.HandleMasterPress();
							}}
						>
							<View style={customStyles.flexRowCenter}>
								<View>
									<Text style={customStyles.textAccountAnalyticsHeadMaster}>{this.state.accountProfile}</Text>
								</View>
								<View style={customStyles.accountArrow}>
									<Image
										source={require('../assets/images/arrow_right.png')}
										style={customStyles.accountAnalyticsArrow}
										resizeMode="contain"
									/>
								</View>
							</View>
						</TouchableOpacity>
						<TouchableOpacity
							onPress={() => {
								this.HandleAllPress();
							}}
						>
							<View style={[ customStyles.flexRowCenter ]}>
								<View>
									<Text style={customStyles.textAccountAnalyticsHeadAll}>{this.state.merchantProfile}</Text>
								</View>
								<View style={customStyles.accountArrow}>
									<Image
										source={require('../assets/images/arrow_right.png')}
										style={customStyles.accountAnalyticsArrow}
										resizeMode="contain"
									/>
								</View>
							</View>
						</TouchableOpacity>
					</View>
				</View>
				{this.state.defaultMaster ? (
					<View style={customStyles.accountAnalyticsImageContainer}>
						<View style={customStyles.marginTop10}>
							<View>
								<Text style={customStyles.textAccountAnalyticsAll}>{this.state.accountProfile}</Text>
								<View style={customStyles.flexRowCenter}>
									<View style={customStyles.flexRowCenter}>
										<View style={customStyles.flexRowJustifyCenter}>
											<Image
												source={require('../assets/images/account-summary/clock.png')}
												style={customStyles.accountSummaryHeaderIcon}
												resizeMode="contain"
											/>
										</View>
										<View>
											<Text style={customStyles.accountSummaryText}> 2 Hours </Text>
										</View>
									</View>
									<View style={{ flex: 1 }}>
										<Text style={customStyles.accountSummaryText}> P799.00 </Text>
									</View>
								</View>

								<View style={customStyles.flexRowCenter}>
									<View style={customStyles.flexRowCenter}>
										<View style={customStyles.flexRowJustifyCenter}>
											<Image
												source={require('../assets/images/account-summary/group.png')}
												style={customStyles.accountSummaryHeaderIcon}
												resizeMode="contain"
											/>
										</View>
										<View>
											<Text style={customStyles.accountSummaryText}> 5 Pax </Text>
										</View>
									</View>
									<View style={{ flex: 1 }}>
										<Text style={customStyles.accountSummaryText}> Manila, PHL </Text>
									</View>
								</View>
							</View>
							<View style={customStyles.paddingTop20}>
								<View>
									<Text style={customStyles.accountHighlightLine} />
								</View>
								<View style={customStyles.accountHighlightContainer}>
									<Text style={customStyles.accountHighlightClicked} />
								</View>
							</View>
						</View>
					</View>
				) : (
					<View style={customStyles.accountAnalyticsBlankContainer} />
				)}

				{/* Select Catergory */}
				<ScrollView>
					<View style={customStyles.accountSummaryCategoryContainer}>
						<View style={customStyles.marginTop10}>
							<View
								style={{
									flexDirection   : 'row',
									alignItems      : 'center',
									justifyContent  : 'space-between',
									backgroundColor : '#191919'
								}}
							>
								<View>
									<TouchableOpacity onPress={this.HandleDayTab}>
										{this.state.clickDay ? (
											<View
												style={{
													paddingTop      : 5,
													paddingBottom   : 5,
													width           : width / 4.8,
													backgroundColor : '#606060',
													borderRadius    : 3,
													justifyContent  : 'center'
												}}
											>
												<Text style={{ textAlign: 'center', fontSize: 12, color: '#cccccc' }}>Day</Text>
											</View>
										) : (
											<View style={{ paddingTop: 5, paddingBottom: 5, width: width / 4.8, justifyContent: 'center' }}>
												<Text style={{ textAlign: 'center', fontSize: 12, color: '#606060' }}>Day</Text>
											</View>
										)}
									</TouchableOpacity>
								</View>
								<View>
									<TouchableOpacity onPress={this.HandleWeekTab}>
										{this.state.clickWeek ? (
											<View
												style={{
													paddingTop      : 5,
													paddingBottom   : 5,
													width           : width / 4.8,
													backgroundColor : '#606060',
													borderRadius    : 3,
													justifyContent  : 'center'
												}}
											>
												<Text style={{ textAlign: 'center', fontSize: 12, color: '#cccccc' }}>Week</Text>
											</View>
										) : (
											<View style={{ paddingTop: 5, paddingBottom: 5, width: width / 4.8, justifyContent: 'center' }}>
												<Text style={{ textAlign: 'center', fontSize: 12, color: '#606060' }}>Week</Text>
											</View>
										)}
									</TouchableOpacity>
								</View>
								<View>
									<TouchableOpacity onPress={this.HandleMonthTab}>
										{this.state.clickMonth ? (
											<View
												style={{
													paddingTop      : 5,
													paddingBottom   : 5,
													width           : width / 4.8,
													backgroundColor : '#606060',
													borderRadius    : 3,
													justifyContent  : 'center'
												}}
											>
												<Text style={{ textAlign: 'center', fontSize: 12, color: '#cccccc' }}>Month</Text>
											</View>
										) : (
											<View style={{ paddingTop: 5, paddingBottom: 5, width: width / 4.8, justifyContent: 'center' }}>
												<Text style={{ textAlign: 'center', fontSize: 12, color: '#606060' }}>Month</Text>
											</View>
										)}
									</TouchableOpacity>
								</View>
								<View>
									<TouchableOpacity onPress={this.HandleYearTab}>
										{this.state.clickYear ? (
											<View
												style={{
													paddingTop      : 5,
													paddingBottom   : 5,
													width           : width / 4.8,
													backgroundColor : '#606060',
													borderRadius    : 3,
													justifyContent  : 'center'
												}}
											>
												<Text style={{ textAlign: 'center', fontSize: 12, color: '#cccccc' }}>Year</Text>
											</View>
										) : (
											<View style={{ paddingTop: 5, paddingBottom: 5, width: width / 4.8, justifyContent: 'center' }}>
												<Text style={{ textAlign: 'center', fontSize: 12, color: '#606060' }}>Year</Text>
											</View>
										)}
									</TouchableOpacity>
								</View>
							</View>
						</View>
						{/* Calendar Agenda */}
						<Agenda
							style={{ marginBottom: 20 }}
							items={this.state.items}
							//loadItemsForMonth={this.loadItems.bind(this)}
							selected={this.state.currentDay}
							//renderItem={this.renderItem.bind(this)}
							//renderEmptyDate={this.renderEmptyDate.bind(this)}
							//rowHasChanged={this.rowHasChanged.bind(this)}
							markingType={'dot'}
							markedDates={{
								[this.state.currentDay]: { selected: true, marked: true },
								'2019-02-08'            : { marked: true }
							}}
							// specify how agenda knob should look like
							renderKnob={() => {
								return (
									<Image
										source={require('../assets/images/menu.png')}
										style={customStyles.knobIcon}
										resizeMode="contain"
									/>
								);
							}}
							theme={{
								calendarBackground : '#191919',
								textColor          : '#cccccc'
							}}
							// Hide knob button. Default = false
							hideKnob={false}
							//renderDay={(day, item) => (<Text>{day ? day.day: 'item'}</Text>)}
							// If provided, a standard RefreshControl will be added for "Pull to Refresh" functionality. Make sure to also set the refreshing prop correctly.
							//onRefresh={() => console.log('refreshing...')}
							// Set this true while waiting for new data from a refresh
							//refreshing={false}
							// Add a custom RefreshControl component, used to provide pull-to-refresh functionality for the ScrollView.
							//refreshControl={null}
						/>

						<View style={customStyles.categoryContainer}>
							<TouchableOpacity>
								<View style={customStyles.analyticsContent}>
									<Text style={customStyles.analyticsLabel}>Earnings</Text>
									<View style={customStyles.categoryNumberValue}>
										<Text style={customStyles.analyticsPhp}> PHP </Text>
										<Text style={customStyles.categoryNumberValueStyle}>28,407</Text>
									</View>
									<View style={customStyles.flexRowCenterSpaceBetween}>
										<View>
											<Text style={customStyles.categoryLabel}>March - 12,946 </Text>
										</View>
										<View>
											<Image
												source={require('../assets/images/account-summary/graph.png')}
												style={customStyles.categoryContainerIconSize}
												resizeMode="contain"
											/>
										</View>
									</View>
								</View>
							</TouchableOpacity>
						</View>
						<View style={customStyles.categoryContainer}>
							<TouchableOpacity>
								<View style={customStyles.categoryContent}>
									<Text style={customStyles.categoryLabel}>Online Bookings</Text>
									<View style={customStyles.categoryNumberValue}>
										<Text style={customStyles.categoryNumberValueStyle}>679</Text>
									</View>
									<View style={customStyles.flexRowCenterSpaceBetween}>
										<View>
											<Text style={customStyles.categoryLabel}>March - 420</Text>
										</View>
										<View>
											<Image
												source={require('../assets/images/account-summary/bookmark.png')}
												style={customStyles.categoryContainerIconSize}
												resizeMode="contain"
											/>
										</View>
									</View>
								</View>
							</TouchableOpacity>

							<TouchableOpacity>
								<View style={customStyles.categoryContent}>
									<Text style={customStyles.categoryLabel}>Walk-In Bookings</Text>
									<View style={customStyles.categoryNumberValue}>
										<Text style={customStyles.categoryNumberValueStyle}>679</Text>
									</View>
									<View style={customStyles.flexRowCenterSpaceBetween}>
										<View>
											<Text style={customStyles.categoryLabel}>March - 290</Text>
										</View>
										<View>
											<Image
												source={require('../assets/images/account-summary/footprints.png')}
												style={customStyles.categoryContainerIconSize}
												resizeMode="contain"
											/>
										</View>
									</View>
								</View>
							</TouchableOpacity>
						</View>

						<View style={customStyles.categoryContainer}>
							<TouchableOpacity>
								<View style={customStyles.categoryContent}>
									<Text style={customStyles.categoryLabel}>Saved</Text>
									<View style={customStyles.categoryNumberValue}>
										<Text style={customStyles.categoryNumberValueStyle}>866</Text>
									</View>
									<View style={customStyles.flexRowCenterSpaceBetween}>
										<View>
											<Text style={customStyles.categoryLabel}>Realtime</Text>
										</View>
										<View>
											<Image
												source={require('../assets/images/account-summary/heart.png')}
												style={customStyles.categoryContainerIconSize}
												resizeMode="contain"
											/>
										</View>
									</View>
								</View>
							</TouchableOpacity>

							<TouchableOpacity>
								<View style={customStyles.categoryContent}>
									<Text style={customStyles.categoryLabel}>Shares</Text>
									<View style={customStyles.categoryNumberValue}>
										<Text style={customStyles.categoryNumberValueStyle}>203</Text>
									</View>
									<View style={customStyles.flexRowCenterSpaceBetween}>
										<View>
											<Text style={customStyles.categoryLabel}>Realtime</Text>
										</View>
										<View>
											<Image
												source={require('../assets/images/account-summary/share.png')}
												style={customStyles.categoryContainerIconSize}
												resizeMode="contain"
											/>
										</View>
									</View>
								</View>
							</TouchableOpacity>
						</View>

						<View style={customStyles.categoryContainer}>
							<TouchableOpacity>
								<View style={customStyles.categoryContent}>
									<Text style={customStyles.categoryLabel}>Reviews</Text>
									<View style={customStyles.categoryNumberValue}>
										<Text style={customStyles.categoryNumberValueStyle}>264</Text>
									</View>
									<View style={customStyles.flexRowCenterSpaceBetween}>
										<View>
											<Text style={customStyles.categoryLabel}>Realtime</Text>
										</View>
										<View>
											<Image
												source={require('../assets/images/account-summary/star.png')}
												style={customStyles.categoryContainerIconSize}
												resizeMode="contain"
											/>
										</View>
									</View>
								</View>
							</TouchableOpacity>

							<TouchableOpacity>
								<View style={customStyles.categoryContent}>
									<Text style={customStyles.categoryLabel}>Views</Text>
									<View style={customStyles.categoryNumberValue}>
										<Text style={customStyles.categoryNumberValueStyle}>2640</Text>
									</View>
									<View style={customStyles.flexRowCenterSpaceBetween}>
										<View>
											<Text style={customStyles.categoryLabel}>Realtime</Text>
										</View>
										<View>
											<Image
												source={require('../assets/images/account-summary/eyes.png')}
												style={customStyles.categoryContainerIconSize}
												resizeMode="contain"
											/>
										</View>
									</View>
								</View>
							</TouchableOpacity>
						</View>
					</View>
				</ScrollView>
			</View>
		);
	}
}

export default AccountAnalytics;