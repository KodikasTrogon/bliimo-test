import React from 'react';
import { View, Text, Image, TouchableOpacity, ScrollView, Dimensions, FlatList } from 'react-native';

import { connect } from 'react-redux';
import { firebaseIni, pushData, setData, addAuth, auth, getData, getProfile } from '../reducers/reducer';
import customStyles from '../assets/styling/styles';

const { width, height } = Dimensions.get('window');

class AnalyticsMasterList extends React.Component {
	static navigationOptions = {
		header : null
	};

	state = {
		list     : [ 'Master', 'Horse Riders PH', 'Xtreme Adventures' ],
		selected : 0
	};

	constructor(props) {
		super(props);
  }
  
  componentDidMount = () => {
    let {indexMasterList} = this.props.navigation.state.params
    this.setState({ selected: indexMasterList || 0});
  }

	HandleLineSeparator = () => {
		return <View style={{ borderTopWidth: 0.3, borderTopColor: '#cccccc', marginLeft: 20, marginRight: 20 }} />;
	};

	_onPressMaster = (item, index) => {
    this.setState({ selected: index });
    this.props.navigation.navigate('AccountAnalytics', {clickedMasterList: item, indexMasterList: index})
	};

	_renderList = ({ item, index }) => {
		return (
			<TouchableOpacity
				onPress={() => {
					this._onPressMaster(item, index);
				}}
			>
				<View style={{ paddingVertical: 20 }}>
					<Text
						style={{
							color      : '#cccccc',
							fontSize   : 23,
							textAlign  : 'center',
							fontFamily : this.state.selected == index ? 'gotham-bold' : 'gotham-light'
						}}
					>
						{item}
					</Text>
				</View>
			</TouchableOpacity>
		);
	};

	HandleGoToMaster = () => {
		this.props.navigation.navigate('AccountAnalytics');
	};

	render() {
		return (
			<View style={[ customStyles.containerFlex ]}>
				<View>
					<View style={customStyles.btnBack}>
						<TouchableOpacity
							onPress={() => {
								this.HandleGoToMaster();
							}}
						>
							<View>
								<Image
									source={require('../assets/images/back.png')}
									style={customStyles.backIcon}
									resizeMode="contain"
								/>
							</View>
						</TouchableOpacity>
					</View>
				</View>
				<ScrollView>
					<FlatList
						extraData={this.state}
						data={this.state.list}
						renderItem={this._renderList}
						keyExtractor={(data, index) => index.toString()}
						ItemSeparatorComponent={this.HandleLineSeparator}
					/>
				</ScrollView>
			</View>
		);
	}
}

export default connect()(AnalyticsMasterList);
