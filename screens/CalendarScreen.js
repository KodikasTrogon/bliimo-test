import React from 'react';
import { View, Text, TextInput, TouchableOpacity, Dimensions, FlatList, Image, ScrollView } from 'react-native';

import { connect } from 'react-redux';
import customStyles from '../assets/styling/styles';
import CalendarAgenda from '../components/CalendarAgenda';
import CalendarSearch from '../components/CalendarSearch';

const { height, width } = Dimensions.get('window');

class CalendarScreen extends React.Component {
	static navigationOptions = {
		header : null
	};

	constructor() {
		super();
	}
	state = {
		showAgenda : true,
    statusText : '',
    contentId  : ''
  };
  
  componentDidMount = () => {
    let { contentId } = this.props.navigation.state.params
		this.setState({ contentId })
		console.log(contentId)
  }

	render() {
		return (
			<View style={customStyles.containerFlex}>
				<CalendarSearch
					goBack={() => {
						this.props.navigation.navigate('SavedEvents');
					}}
				/>
				<CalendarAgenda
					addBooking={(currentDay, clickedDay) => {
						this.props.navigation.navigate('BookingForm', { currentDay: currentDay, clickedDay: clickedDay, contentId: this.state.contentId });
					}}
					bookingAction={(status) => {
						if (status === 1) {
							this.setState({ statusText: true });
						} else {
							this.setState({ statusText: false });
						}
						this.props.navigation.navigate('RedeemSuccess', this.state.statusText);
					}}
          visibility={this.state.showAgenda}
          contentId={this.state.contentId}
				/>
			</View>
		);
	}
}

export default CalendarScreen;
