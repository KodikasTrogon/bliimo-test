import React from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity, Alert, Linking, ScrollView } from 'react-native';

import { connect } from 'react-redux';
import { sendQR } from '../reducers/reducer';
import { Constants, BarCodeScanner, Permissions } from 'expo';
import customStyles, { bliimoColor } from '../assets/styling/styles';

class Scanner extends React.Component {
	static navigationOptions = {
		header : null
	};

	state = {
		hasCameraPermission : null,
		pageOrigin          : ''
	};

	HandleCameraPermission = async () => {
		const { status } = await Permissions.askAsync(Permissions.CAMERA);
		this.setState({
			hasCameraPermission : status === 'granted'
		});
	};

	HandleQRCodeRead = (qr) => {
		this.props.navigation.navigate('RedeemSuccess');
		console.log('scanner', qr.data);
	};

	componentWillMount = async () => {
		this.HandleCameraPermission();
	};

	render() {
		if (this.state.hasCameraPermission) {
			return (
				<View style={[ customStyles.scannerContainer ]}>
					<BarCodeScanner
						onBarCodeRead={(result) => {
							this.HandleQRCodeRead(result);
						}}
						style={customStyles.barCodeScanner}
					/>
					<View style={customStyles.scannerBoxContainer}>
						<View style={{ alignSelf: 'flex-end' }}>
							<TouchableOpacity
								style={{ padding: 20 }}
								onPress={() => {
									this.props.navigation.navigate(this.props.navigation.state.params.returnPage.screen);
								}}
							>
								<Image
									source={require('../assets/images/close.png')}
									style={customStyles.scannerCloseButton}
									tintColor="#fff"
									resizeMode="contain"
								/>
							</TouchableOpacity>
						</View>
						<View style={customStyles.scannerBox}>
							<View style={customStyles.scannerView} />
						</View>
					</View>
				</View>
			);
		} else {
			return (
				<View style={customStyles.containerFlex}>
					<ScrollView>
						<View>
							<View style={customStyles.logoContainer}>
								<Image
									source={require('../assets/images/gray.png')}
									style={customStyles.logoSize}
									resizeMode="contain"
								/>
							</View>
							<View style={customStyles.textContainer}>
								<Text style={customStyles.resetButton}>Camera permision is disabled</Text>
							</View>
							<View customStyles={customStyles.formButton}>
								<TouchableOpacity
									onPress={() => {
										Linking.openURL('app-settings:');
										this.HandleCameraPermission();
									}}
									style={customStyles.resetButtonContainer}
								>
									<Text style={customStyles.resetButton}>Enable Camera</Text>
								</TouchableOpacity>
							</View>
						</View>
					</ScrollView>
				</View>
			);
		}
	}
}

const styles = StyleSheet.create({
	container : {
		flex           : 1,
		alignItems     : 'center',
		justifyContent : 'center'
	}
});

const mapStateToProps = (state) => {
	return {};
};

const mapDispatchToProps = {
	sendQR
};

export default connect(mapStateToProps, mapDispatchToProps)(Scanner);
