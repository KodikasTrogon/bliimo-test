import React from 'react';
import {
	View,
	Text,
	TextInput,
	TouchableOpacity,
	Dimensions,
	FlatList,
	Image,
	ScrollView,
	Modal,
  AsyncStorage,
  BackHandler
} from 'react-native';

import { connect } from 'react-redux';
import { getRole } from '../reducers/reducer';
import customStyles from '../assets/styling/styles';
import Loading from './../components/Loading';

const { height, width } = Dimensions.get('window');

class Account extends React.Component {
	static navigationOptions = {
		header : null
	};

	constructor() {
		super();
	}

	state = {
		enabled            : true,
		successEditModal   : false,
		successLogoutModal : false,
		accesToken         : '',
		merchantData       : '',
		partnerData        : '',
		partnerLocation    : '',
		partnerBankType    : '',
		avatar             : '',
		firstName: '',
		lastName: '',
		loginEmail: '',
		number: '',
		businessAddress: '',
		businessName: '',
		businessLogo: '',
		load: false
	};

	componentWillMount = () => {
    this.HandleFetchMerchant();
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
  };

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }
  
  handleBackPress = () => {
    this.props.navigation.navigate('SavedEvents')
    return true;
  }

	HandleFetchMerchant = async () => {
		const token = await AsyncStorage.getItem('token');
		await this.props.getRole(token);
		let getMerchantData = await this.props.roleState;
		if( getMerchantData != 'failed'){
			this.setState({
				firstName: getMerchantData.firstName,
				lastName: getMerchantData.lastName,
				loginEmail: getMerchantData.email,
				number: getMerchantData.partner.phone,
				businessAddress : getMerchantData.partner.location.address,
				businessName: getMerchantData.partner.name,
				load: true
			});
		}else{
			alert('Error fetching data.')
			this.setState({ load: true })
		}
		//Check Avatar
		console.log(getMerchantData.partner.logo);
		if (getMerchantData.partner.logo != null) {
			this.setState({ businessLogo: getMerchantData.partner.logo});
		}else{
			this.setState({ businessLogo: 'https://i.imgur.com/vqHFZuT.png' })
		}
		console.log
		// console.log('SHOW', this.state.merchantData.partner.name);
		// console.log('Merchant DATA', this.state.partnerBankType);
	};

	HandleUpdate = () => {
		this.setState({ successEditModal: true });

		setTimeout(() => {
			this.setState({ successEditModal: false });
		}, 3000);
	};

	HandleLogout = () => {
		this.setState({ successLogoutModal: true });
	};

	HandleClose = () => {
		this.setState({ successLogoutModal: false });
	};

	HandleSuccessLogout = () => {
		AsyncStorage.clear();
		this.setState({ successLogoutModal: false });
		this.props.navigation.navigate('Login');
	};

	render() {
		if (this.state.load) {
			return (
				<View style={customStyles.containerFlex}>
					<ScrollView scrollEnabled={this.state.enabled}>
						<View style={customStyles.headerSettingContainer}>
							<View style={customStyles.headerSettingView}>
								<TouchableOpacity onPress={this.HandleLogout}>
									<Image
										source={require('../assets/images/cog.png')}
										style={customStyles.headerSetting}
										resizeMode="contain"
									/>
								</TouchableOpacity>
							</View>
						</View>

						<View style={{ borderBottomColor: '#474747', borderBottomWidth: 1, marginTop: 20, top: 85 }} />

						<View style={{ paddingTop: 20, justifyContent: 'center',	alignItems: 'center' }}>
							<View 
								style={{
									justifyContent: 'center',
									alignItems: 'center',
									borderWidth: 4,
									borderRadius: 150,
									borderColor: '#fff',
									overflow: 'hidden',
									height: 110,
									width: 110,
									backgroundColor: '#fff'
								}}>
									<Image
										source={{
											uri : this.state.avatar
										}}
										style={{ height: 110, width: 110 }}
									/>
							</View>
							<View style={{ padding: 20 }}>
								<Text style={customStyles.textNotificationStyle}>{this.state.businessName}</Text>
								<Text style={customStyles.textNotificationStyle}>{}</Text>
							</View>
						</View>

						{/* Profile Informations */}
						<View style={customStyles.formSectionContainer}>
							<View style={customStyles.textLabelContainer}>
								<Text style={customStyles.textAccountLabel}>First Name:</Text>
							</View>
							<View style={customStyles.formInputAccount}>
								<TextInput
									style={customStyles.textboxDesign}
									placeholder="Your business name"
									placeholderTextColor="#606060"
									returnKeyLabel={'done'}
									autoCorrect={false}
									selectionColor={'#fff'}
									underlineColorAndroid={'transparent'}
									placeholderTextColor="#fff"
									onChangeText={(text) => this.setState({ firstName: text })}
									value={this.state.firstName}
								/>
							</View>
						</View>
						<View style={customStyles.formSectionContainer}>
							<View style={customStyles.textLabelContainer}>
								<Text style={customStyles.textAccountLabel}>Last Name:</Text>
							</View>
							<View style={customStyles.formInputAccount}>
								<TextInput
									style={customStyles.textboxDesign}
									placeholder="Your business name"
									placeholderTextColor="#606060"
									returnKeyLabel={'done'}
									autoCorrect={false}
									selectionColor={'#fff'}
									underlineColorAndroid={'transparent'}
									placeholderTextColor="#fff"
									onChangeText={(text) => this.setState({ lastName: text })}
									value={this.state.lastName}
								/>
							</View>
						</View>
						<View style={customStyles.formSectionContainer}>
							<View style={customStyles.textLabelContainer}>
								<Text style={customStyles.textAccountLabel}>Login Email:</Text>
							</View>
							<View style={customStyles.formInputAccount}>
								<TextInput
									style={customStyles.textboxDesign}
									placeholder="Your email address"
									placeholderTextColor="#606060"
									returnKeyLabel={'done'}
									autoCorrect={false}
									selectionColor={'#fff'}
									underlineColorAndroid={'transparent'}
									placeholderTextColor="#fff"
									onChangeText={(text) => this.setState({  loginEmail: text })}
									value={this.state.loginEmail}
								/>
							</View>
						</View>

						<View style={customStyles.formSectionContainer}>
							<View style={customStyles.textLabelContainer}>
								<Text style={customStyles.textAccountLabel}>Number:</Text>
							</View>
							<View style={customStyles.formInputAccount}>
								<TextInput
									style={customStyles.textboxDesign}
									placeholder="Your phone number"
									placeholderTextColor="#606060"
									returnKeyLabel={'done'}
									autoCorrect={false}
									selectionColor={'#fff'}
									underlineColorAndroid={'transparent'}
									placeholderTextColor="#fff"
									onChangeText={(text) => this.setState({ number: text})}
									value={this.state.number}
								/>
							</View>
						</View>
						<View style={customStyles.formSectionContainer}>
							<View style={customStyles.textLabelContainer}>
								<Text style={customStyles.textAccountLabel}>Business Address:</Text>
							</View>
							<View style={customStyles.formInputAccount}>
								<TextInput
									style={customStyles.textboxDesign}
									placeholder="Your business address"
									placeholderTextColor="#606060"
									returnKeyLabel={'done'}
									autoCorrect={false}
									selectionColor={'#fff'}
									underlineColorAndroid={'transparent'}
									placeholderTextColor="#fff"
									onChangeText={(text) => this.setState({ businessAddress: text })}
									value={this.state.businessAddress}
								/>
							</View>
						</View>

						<View style={customStyles.profileButton}>
							<TouchableOpacity onPress={this.HandleUpdate}>
								<View style={customStyles.resetButtonContainer}>
									<Text style={customStyles.resetButton}>Submit</Text>
								</View>
							</TouchableOpacity>
						</View>
					</ScrollView>
					{/* Modals */}

					<Modal
						animationType="fade"
						transparent={true}
						visible={this.state.successEditModal}
						onRequestClose={() => {
							console.log('close');
						}}
					>
						<View
							style={{
								justifyContent  : 'center',
								flexDirection   : 'row',
								alignContent    : 'center',
								paddingTop      : 200,
								paddingBottom   : 300,
								backgroundColor : 'transparent'
							}}
						>
							<View
								style={{
									width           : width / 2 + 120,
									height          : height / 2 - 120,
									backgroundColor : 'rgba(52, 52, 52, 0.9)',
									borderRadius    : 5,
									elevation       : 5
								}}
							>
								<View style={{ paddingTop: 40, paddingBottom: 10, alignSelf: 'center' }}>
									<Image
										style={{ width: 50, height: 50 }}
										source={require('../assets/images/check_icon.png')}
										resizeMode="contain"
									/>
								</View>
								<View style={{ paddingLeft: 10, paddingRight: 10, alignItems: 'center', marginBottom: 20 }}>
									<Text style={{ color: '#cccccc', fontWeight: '500', fontSize: 20, textAlign: 'center' }}>Awesome!</Text>
									<Text style={{ color: '#cccccc', fontWeight: '400', fontSize: 17, textAlign: 'center' }}>
										{'Changes to your profile have'}
									</Text>
									<Text style={{ color: '#cccccc', fontWeight: '400', fontSize: 17, textAlign: 'center' }}>
										{'been submitted for approval'}
									</Text>
								</View>
							</View>
						</View>
					</Modal>

					<Modal
						animationType="fade"
						transparent={true}
						visible={this.state.successLogoutModal}
						onRequestClose={() => {
							console.log('close');
						}}
					>
						<View
							style={{
								flex            : 1,
								justifyContent  : 'center',
								flexDirection   : 'column',
								alignItems      : 'center',
								backgroundColor : 'transparent'
							}}
						>
							<View
								style={{
									width           : width / 2 + 120,
									height          : undefined,
									backgroundColor : 'rgba(52, 52, 52, 1)',
									borderRadius    : 5,
									elevation       : 5
								}}
							>
								<View style={{ padding: 20 }}>
									<Text style={{ color: '#cccccc', fontWeight: '500', fontSize: 20, textAlign: 'center' }}>Logout</Text>
									<Text style={{ color: '#cccccc', fontWeight: '400', fontSize: 17, textAlign: 'center' }}>
										{'Are you sure you want to logout?'}
									</Text>
								</View>
								<View style={{ marginTop: 10, borderWidth: 0.5, borderColor: '#cccccc' }} />

								<View
									style={{ paddingLeft: 10, paddingRight: 10, flexDirection: 'row', justifyContent: 'space-between' }}
								>
									<View style={{ flex: 1, borderRightWidth: 0.5, borderColor: '#cccccc' }}>
										<TouchableOpacity onPress={this.HandleClose}>
											<View style={{ paddingTop: 10, paddingBottom: 10 }}>
												<Text style={{ color: '#cccccc', fontWeight: '400', fontSize: 20, textAlign: 'center' }}>No</Text>
											</View>
										</TouchableOpacity>
									</View>

									<View style={{ flex: 1, borderLeftWidth: 0.5, borderColor: '#cccccc' }}>
										<TouchableOpacity
											onPress={() => {
												this.HandleSuccessLogout();
											}}
										>
											<View style={{ paddingTop: 10, paddingBottom: 10 }}>
												<Text style={{ color: '#cccccc', fontWeight: '400', fontSize: 20, textAlign: 'center' }}>
													Yes
												</Text>
											</View>
										</TouchableOpacity>
									</View>
								</View>
							</View>
						</View>
					</Modal>
				</View>
			);
		}else{
			return (
				<View style={customStyles.containerFlex}>
					<Loading />
				</View>
			)
		}
	}
}

const mapStateToProps = (state) => {
	return {
		roleState : state.roleState
	};
};

const mapDispatchToProps = {
	getRole
};

export default connect(mapStateToProps, mapDispatchToProps)(Account);
