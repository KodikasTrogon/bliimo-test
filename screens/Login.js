import React from 'react';
import {
	View,
	Text,
	StyleSheet,
	TextInput,
	Image,
	ScrollView,
	TouchableOpacity,
	ImageBackground,
	Modal,
	KeyboardAvoidingView,
	Dimensions,
	AsyncStorage
} from 'react-native';

import { connect } from 'react-redux';
import { Font } from 'expo';

import customStyles from '../assets/styling/styles';
import { validate } from '../components/Validation';
import { authUser, getRole } from '../reducers/reducer';
global.Symbol = require('core-js/es6/symbol');
require('core-js/fn/symbol/iterator');
require('core-js/fn/map');
require('core-js/fn/set');
require('core-js/fn/array/find');

const { height, width } = Dimensions.get('window');

class Login extends React.Component {
	static navigationOptions = {
		header : null
	};

	constructor() {
		super();
	}

	state = {
		errorLoginModal : false,
		email						: 'cantina_luna_master_merchant',
		password				: 'password123',
		// email           : 'joelMerchant@gmail.co1111m',
		// password        : 'password123',
		fontLoaded      : false,
    currentRole     : '',
    errorMessage    : ''
	};

	//Custom Font
	async componentDidMount() {
		await Font.loadAsync({
			'gotham-light'  : require('../assets/fonts/Gotham-Light.ttf'),
			'gotham-medium' : require('../assets/fonts/Gotham-Medium.ttf'),
			'gotham-bold'   : require('../assets/fonts/Gotham-Bold.otf'),
			'gotham-book'   : require('../assets/fonts/GothamBook.ttf')
		});
		this.setState({ fontLoaded: true });
	}

	componentWillMount = async () => {
		this._retrieveData();
	};

	_storeData = async (token, usernameOrEmail, password) => {
		// console.log('SET ITEM', token);
		try {
			await AsyncStorage.setItem('token', token);
			await AsyncStorage.setItem('usernameOrEmail', usernameOrEmail);
			await AsyncStorage.setItem('password', password);
		} catch (error) {
			// Error saving data
			console.log('Error saving data');
		}
	};

	_retrieveData = async () => {
		try {
			const token = await AsyncStorage.getItem('token');
			const usernameOrEmail = await AsyncStorage.getItem('usernameOrEmail');
			const password = await AsyncStorage.getItem('password');

			if ((usernameOrEmail && password) || token !== null) {
				// We have data!!
				this.HandleLogin(usernameOrEmail, password); //Auto Login Function
			}
		} catch (error) {
			// Error retrieving data
		}
	};

	HandleModal = () => {
		this.setState({ errorLoginModal: true });
		setTimeout(() => {
			this.setState({ errorLoginModal: false });
		}, 3000);
	};

	HandleLogin = async (email, password) => {
		let validateEmail = true;
		let validatePassword = true;

		if (validateEmail && validatePassword) {
			let data = {
				usernameOrEmail : email,
				password        : password
			};
			//Get Token
			await this.props.authUser(data);
      let response = await this.props.authState;
      console.log('res', response)
      if ( response.data != 'Network Error'){
        if (response != 'Error: Request failed with status code 500') {
          await this.props.getRole(response.accessToken); //Get Role
          let responseRole = await this.props.roleState;
          let role = '';
          //Get current role
          responseRole.roles.map((val) => {
            if (val.name == 'ROLE_MERCHANT_MASTER') {
              role = val.name;
            } else if (role != 'ROLE_MERCHANT_MASTER') {
              role = val.name;
            }
          });
          this.setState({ currentRole: role });
          this._storeData(response.accessToken, email, password); //Save Access Token
          this.props.navigation.navigate('SavedEvents', { currentUser: this.state.currentRole });
        } else {
          this.setState({ errorLoginModal: true, errorMessage: 'You have entered the\nwrong username or password' });
          console.log('FAILED IN');
        }
      } else {
        this.setState({errorLoginModal: true, errorMessage: 'No Internet Connection'})
        console.log('no internet');
			}
		} else {
			this.setState({ errorLoginModal: true });
		}
	};

	render() {
		return (
			<View style={customStyles.containerFlex}>
				{/* SET CUSTOM FONT CONDITION */}
				{this.state.fontLoaded ? (
					<ImageBackground style={[ customStyles.backgroundImage ]} source={require('../assets/images/landing.png')}>
						<View style={customStyles.opacity}>
							<ScrollView>
								<KeyboardAvoidingView behavior="position" keyboardVerticalOffset={10} enabled>
									<View style={customStyles.loginContainer}>
										<Text style={customStyles.loginText}>Log in to Bliimo</Text>
										<View style={customStyles.loginLine} />
									</View>
									<View style={customStyles.textInputContainer}>
										<TextInput
											style={customStyles.textInputDesign}
											placeholder="Email"
											returnKeyType={'next'}
											onSubmitEditing={() => {
												this.password.focus();
											}}
											autoCorrect={false}
											selectionColor={'#fff'}
											underlineColorAndroid={'transparent'}
											placeholderTextColor="#fff"
											onChangeText={(text) => this.setState({ email: text })}
											value={this.state.email}
											placeholderStyle={{ fontFamiliy: 'gotham-light' }}
										/>
									</View>
									<View style={customStyles.textInputContainer}>
										<TextInput
											style={customStyles.textInputDesign}
											secureTextEntry={true}
											placeholder="Password"
											returnKeyLabel={'done'}
											ref={(input) => {
												this.password = input;
											}}
											autoCorrect={false}
											selectionColor={'#fff'}
											underlineColorAndroid={'transparent'}
											placeholderTextColor="#fff"
											onChangeText={(text) => this.setState({ password: text })}
											value={this.state.password}
										/>
									</View>
									<View style={customStyles.passwordButton}>
										<TouchableOpacity
											onPress={() => {
												this.props.navigation.navigate('ForgotPassword');
											}}
										>
											<View style={customStyles.passwordTextContainer}>
												<Text style={customStyles.passwordText}>Forgot Password?</Text>
											</View>
										</TouchableOpacity>
									</View>
									<View style={customStyles.formButton}>
										<TouchableOpacity
											onPress={() => {
												this.HandleLogin(this.state.email, this.state.password);
											}}
										>
											<View style={customStyles.submitButtonContainer}>
												<Text style={customStyles.submitButton}>Log in</Text>
											</View>
										</TouchableOpacity>
									</View>
								</KeyboardAvoidingView>
							</ScrollView>
						</View>
					</ImageBackground>
				) : null}

				<Modal
					animationType="fade"
					transparent={true}
					visible={this.state.errorLoginModal}
					onRequestClose={() => {
						console.log('close');
					}}
				>
					<View
						style={{
							justifyContent  : 'center',
							flexDirection   : 'row',
							alignContent    : 'center',
							width           : width,
							height          : height,
							backgroundColor : 'rgba(52, 52, 52, 0.9)'
						}}
					>
						<View style={{ alignSelf: 'center', backgroundColor: 'transparent' }}>
							<View style={{ alignSelf: 'center' }}>
								<Image
									style={{ width: 50, height: 50 }}
									source={require('../assets/images/warning-sign.png')}
									resizeMode="contain"
								/>
							</View>
							<View style={{ paddingLeft: 10, paddingRight: 10, alignItems: 'center', marginBottom: 20 }}>
								<Text style={{ color: '#cccccc', fontWeight: '500', fontSize: 20, textAlign: 'center' }}>Ooops!</Text>
								<Text style={{ color: '#cccccc', fontWeight: '400', fontSize: 17, textAlign: 'center' }}>
									{this.state.errorMessage}
								</Text>
								{/* <Text style={{ color: '#cccccc', fontWeight: '400', fontSize: 17, textAlign: 'center' }}>
									{'wrong username or password'}
								</Text> */}
							</View>
							<View>
								<View style={customStyles.formButton}>
									<TouchableOpacity
										onPress={() => {
											this.setState({ errorLoginModal: false });
										}}
									>
										<View style={customStyles.btnGotItContainer}>
											<Text style={customStyles.submitButton}>Got it</Text>
										</View>
									</TouchableOpacity>
								</View>
								<View style={customStyles.marginTop10}>
									<TouchableOpacity
										onPress={() => {
											this.props.navigation.navigate('ForgotPassword');
											this.setState({ errorLoginModal: false });
										}}
									>
										<View style={customStyles.passwordTextContainer}>
											<Text style={customStyles.passwordText}>Forgot Password?</Text>
										</View>
									</TouchableOpacity>
								</View>
							</View>
						</View>
					</View>
				</Modal>
			</View>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		authState : state.authState,
		roleState : state.roleState
	};
};

const mapDispatchToProps = {
	authUser,
	getRole
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
