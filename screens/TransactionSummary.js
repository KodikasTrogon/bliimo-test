import React from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Dimensions,
  Image,
  ScrollView,
  FlatList
} from 'react-native';

import { connect } from 'react-redux';
import { firebaseIni, pushData, setData, addAuth, auth, getData, getProfile } from '../reducers/reducer';
import customStyles from '../assets/styling/styles';
import { validate } from '../components/Validation';
const {height, width} = Dimensions.get('window');

class TransactionSummary extends React.Component{
  static navigationOptions = {
    header:null,
  };

  constructor() {
    super();
  }  
  
  state = {
    email:"",
    password:"",
    redeemContainerButton: true,
    typeQR: false,
    datas:[
      'a', 'b', 'c', 'd',
    ],
    itemsTab: ['Date', 'Voucher\nCode', 'Bliimo\nShare', 'Points\nEarn'],
  }

  HandleTypeQR = () => {
    this.setState({redeemContainerButton: false, typeQR: true});
  }

  HandleEventScanner = () => {
    this.props.actionScanner();
  }

  HandleRow = (row) => {
    const column = ['',''];
    const data = ['08/14/2018', '12QWER', '400', '80'];
    console.log(row)
    return (
      <View>
        <View style={[{flexDirection: "row", marginLeft: 10, marginRight: 10, marginTop: 20}]}>
          { row.map( (value, index) => {
              return (
                <View key={index} style={{flex: 1, paddingVertical: 5, alignSelf: 'center'}} >
                  <Text style={customStyles.txtHeader}>{this.state.itemsTab[index]}</Text>
                </View> 
              )   
            })
          }  
        </View>
        <View style={[{flexDirection: "column"}]}> 
          { column.map( (value, index) => {
              return (
                <View key={index} style={[{flexDirection: "row", marginLeft: 10, marginRight: 10}]}>
                  { data.map( (value, index) => {
                      return (
                        <ScrollView key={index} width= {width}>
                          <View key={index} style={{flex: 1, paddingVertical: 5, alignSelf: 'center'}} >
                            <Text style={customStyles.txtHeader}>{data[index]}</Text>
                          </View> 
                        </ScrollView>
                      )   
                    })
                  }  
                </View> 
              )   
            })
          }
        </View>
      </View>
    )
  }

  render(){
    
    return(
      <View style={[customStyles.containerFlex]}>
        <View style={customStyles.btnBack}>
          <TouchableOpacity
            onPress={() => {this.props.navigation.navigate('RewardScreen')}}>
            <View>
                <Image source={require('../assets/images/back.png')} style={customStyles.backIcon} resizeMode='contain'/>
            </View>
          </TouchableOpacity>
        </View> 
        <View style={{paddingTop: 30}}>
          <Text style={customStyles.lineContainer}></Text>
          <View style={customStyles.imageContainerProfile}>
            <Image source={require('../assets/images/sample1.png')} style={customStyles.imageProfile} resizeMode='cover'/>
          </View>
        </View>
        <View style={{paddingTop: 40}}>
          <Text style={customStyles.txtRedeem}>{"Horse Riders PH"}</Text>
        </View>
        <View style={customStyles.pointsContainer}>
          <Text style={customStyles.numberPoints}>{"200"}</Text>
          <Text style={customStyles.txtPoints}>{"points"}</Text>
        </View>
        <View>
          {this.HandleRow(Object.entries(this.state.itemsTab))}
        </View>
      </View> 
    )
  }
}

export default TransactionSummary;