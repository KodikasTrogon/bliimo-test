import React from 'react';
import { 
  View, 
  Text, 
  TextInput, 
  TouchableOpacity,
  Dimensions,
  FlatList,
  Image,
  ScrollView,
  AsyncStorage,
  RefreshControl
} from 'react-native';

import { connect } from 'react-redux';
import customStyles from '../assets/styling/styles';
import Search from '../components/Search';
import Message from '../components/Message';
import GroupMessage from '../components/GroupMessage';
import { getChatList, getMessages } from '../reducers/reducer';
import Loading from './../components/Loading';
import * as emoticons from 'react-native-emoticons';

const { height, width } = Dimensions.get('window');
const token = '';

class Messages extends React.Component {
  static navigationOptions = {
    header : null
  };

  constructor() {
    super();
  }
  state = {
    messages          : [],
    result            : [],
    idChatList        : [],
    showGroupMessage  : false,
    showMessage       : true,
    clickMessage      : true,
    clickGroupMessage : false,
    refreshing        : false,
    search            : true,
    searchResult      : undefined,
    merchantMessages  : '',
    searchInput       : '',
    chatListResult    : '',
    timer             : false
  };

  componentDidMount = async () => {
    this.HandleGetChatList();
  }

  HandleGetChatList = async () => {
    token = await AsyncStorage.getItem('token');
    await this.props.getChatList(token);
    let getChatList = await this.props.chatsState || 0, result = [], idChatList = [];
    if(this.state.timer){
      let {timer } = this.props.navigation.state.params
      this.setState({ timer })
      this.clearTimer();
      this.timer = setTimeout(() => this.HandleGetChatList(), 500);
    }else{
      if(getChatList.chatList.length > 0){
        getChatList.chatList.map( (value) => {
        result = [...result, value]
        idChatList = [...idChatList, value.id]
      })
        this.setState({result, idChatList});
      }else{
        this.setState({chatListResult: 'empty'});
      }
      this.timer = setTimeout(() => this.HandleGetChatList(), 500);
      console.log("timer start chatlist")
    }
  }

  clearTimer = () => {
    console.log("stop timer chatlist")
    clearTimeout(this.timer)
  }

  HandleChatList = ({item}) => {
    let image  = (!item.creator.avatar) ? require('../assets/images/profile.png') : {uri:item.creator.avatar};  
    return(
    <Message
        openChats = {() => {
          this.props.navigation.navigate('Chats',{chatId: item.id, name: item.creator.firstName + ' ' + item.creator.lastName, avatar: item.creator.avatar, hash: item.hash}),
          this.setState({timer: true})
        }}
        avatar={image}
        id={item.id}
        name={item.creator.firstName + ' ' + item.creator.lastName}
        username={item.creator.username}
        message={emoticons.parse(item.message[item.message.length-1].mess)}
        time={item.lastUpdated}
      />
    )
  }
  
  _refreshChatList = () => {
    this.setState({
      refreshing: true,
      timer: false
    }, () => {
        this.HandleGetChatList();
        this.setState({refreshing: false});
    });
  }
  
  _checkChatList = () => {
    if(this.state.chatListResult == 'empty'){
      return(
        <Text style={{color: '#666', fontSize: 20, alignSelf: 'center', paddingTop: height * .4, position: 'absolute'}}>No Message</Text>
      )
    }else if(!this.state.search){
      return(
        <Text style={{color: '#666', fontSize: 20, alignSelf: 'center', paddingTop: height * .4, position: 'absolute'}}>No Search</Text>
      )
    }else{
      return(
        (Object.entries(this.state.result).length <= 0) && ( <Loading /> )
      )
    }
  }

  handleSearch = (searchText) => {
    let text = searchText.toLowerCase(), filteredList;
    this.clearTimer();
    filteredList = this.state.result.filter((item) => {
      let name = item.creator.firstName + ' ' + item.creator.lastName + '@'+item.creator.username;
      return name.toLowerCase().match(text) ;
    })
    if ( filteredList.length > 0 ) {
      if ( text == '' ) {
        this.setState({ result: filteredList, search: true, timer: false });
        this.HandleGetChatList();
      } else {
        this.setState({ result: filteredList, search: true, timer: true})
      }
    } else {
      this.setState({ search: false, timer: true});
    }
  };

  HandleGroupTab = () => {
    this.setState({ showGroupMessage: true, showMessage: false, clickGroupMessage: true, clickMessage: false });
  };

  HandleMessageTab = () => {
    this.setState({ showGroupMessage: false, showMessage: true, clickGroupMessage: false, clickMessage: true });
  };

  render() {
    return (
      <View style={customStyles.containerFlex}>
        {/* FOR GROUP MESSAGE UPDATE */}
        {/* <View style={customStyles.padding10}>
          <View
            style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            backgroundColor: '#191919'
            }}
          >
            <View>
              <TouchableOpacity onPress={this.HandleMessageTab}>
                {this.state.clickMessage ? (
                  <View
                    style={{
                      paddingTop: 10,
                      paddingBottom: 10,
                      width: width / 2.3,
                      backgroundColor: '#606060',
                      borderRadius: 10,
                      justifyContent: 'center'
                    }}
                  >
                    <Text style={{ textAlign: 'center', fontSize: 12, color: '#cccccc' }}>Messages</Text>
                  </View>
                ) : (
                  <View style={{ paddingTop: 10, paddingBottom: 10, width: width / 2.3, justifyContent: 'center' }}>
                    <Text style={{ textAlign: 'center', fontSize: 12, color: '#606060' }}>Messages</Text>
                  </View>
                  )}
              </TouchableOpacity>
            </View>
            <View>
              <TouchableOpacity onPress={this.HandleGroupTab}>
                {this.state.clickGroupMessage ? (
                  <View
                    style={{
                      paddingTop: 10,
                      paddingBottom: 10,
                      width: width / 2.3,
                      backgroundColor: '#606060',
                      borderRadius: 10,
                      justifyContent: 'center'
                    }}
                    >
                    <Text style={{ textAlign: 'center', fontSize: 12, color: '#cccccc' }}>Group</Text>
                    </View>
                  ) : (
                  <View style={{ paddingTop: 10, paddingBottom: 10, width: width / 2.3, justifyContent: 'center' }}>
                    <Text style={{ textAlign: 'center', fontSize: 12, color: '#606060' }}>Group</Text>
                    </View>
                  )}
              </TouchableOpacity>
            </View>
          </View>
        </View> */}
        <Search 
          handleSearch={(e) => {
            this.handleSearch(e)
          }}
        />
        {/* <View style={{ paddingBottom: 50, display: this.state.showGroupMessage ? 'flex' : 'none' }}>
          <ScrollView>
            <FlatList
              data={this.state.result}
              renderItem={({ item }) => (
                <GroupMessage
                  navigate={this.props.navigate}
                  id={item.id}
                  name={item.name}
                  username={item.username}
                  message={item.message}
                  time={item.time}
                />
              )}
              keyExtractor={(data, index) => index.toString()}
            />
          </ScrollView>
        </View> */}
        <View style={{ paddingBottom: 50, display: this.state.showMessage ? 'flex' : 'none' }}>
          {this._checkChatList()}
          <FlatList
            style={{height: height/12*10}}
            data={ this.state.search? this.state.result : ""}
            renderItem={this.HandleChatList}
            keyExtractor={(data, index) => index.toString()}
            refreshControl={
              <RefreshControl
                onRefresh={this._refreshChatList}
                refreshing={this.state.refreshing}
              />
            }
          />
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    chatsState : state.chatsState,
    messagesState: state.messagesState
  };
};

const mapDispatchToProps = {
  getChatList,
  getMessages
};

export default connect(mapStateToProps, mapDispatchToProps)(Messages);